-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 21 Jul 2021 pada 06.26
-- Versi server: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `asesmen_awal_medis`
--

CREATE TABLE `asesmen_awal_medis` (
  `no_rawat` varchar(17) NOT NULL,
  `tanggal` datetime NOT NULL,
  `keluhan_utama` varchar(150) NOT NULL,
  `rps` varchar(100) NOT NULL,
  `rpk` varchar(100) NOT NULL,
  `rpd` varchar(100) NOT NULL DEFAULT '',
  `keadaanumum` enum('Tampak tidak sakit','Tampak sakit ringan','Tampak sakit sedang','Tampak sakit berat') NOT NULL,
  `kesadaran` enum('Kompos mentis','Apatis','Somnolen','Sopor','Koma') NOT NULL,
  `gcs` varchar(5) NOT NULL,
  `td` varchar(8) NOT NULL DEFAULT '',
  `nadi` varchar(5) NOT NULL DEFAULT '',
  `suhu` varchar(5) NOT NULL DEFAULT '',
  `rr` varchar(5) NOT NULL,
  `pemeriksaanfisik` varchar(100) NOT NULL,
  `pemeriksaanpenunjang` varchar(100) NOT NULL,
  `obat` varchar(100) NOT NULL,
  `diagnosakerja` varchar(100) NOT NULL,
  `diagnosabanding` varchar(100) NOT NULL,
  `rencanapenunjang` varchar(100) NOT NULL,
  `rencanaterapi` varchar(100) NOT NULL,
  `nip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `asesmen_awal_medis`
--

INSERT INTO `asesmen_awal_medis` (`no_rawat`, `tanggal`, `keluhan_utama`, `rps`, `rpk`, `rpd`, `keadaanumum`, `kesadaran`, `gcs`, `td`, `nadi`, `suhu`, `rr`, `pemeriksaanfisik`, `pemeriksaanpenunjang`, `obat`, `diagnosakerja`, `diagnosabanding`, `rencanapenunjang`, `rencanaterapi`, `nip`) VALUES
('2020/12/20/000001', '2021-07-11 07:15:14', 'Keluhan utama', 'rps', 'rpk', 'rpd', 'Tampak tidak sakit', 'Kompos mentis', 'gcs', 'tensi', 'nadi', 'suhu', 'rr', 'pemeriksaan fisik', 'pemeriksaan penunjang', 'obat', 'diagnosa kerja', 'diagnosa banding', 'rencana penunjang', 'rencana terapi', 'D0000045'),
('2021/07/04/000001', '2021-07-21 04:28:53', 'Keluhan utama', 'rps', 'rpk', 'rpd', 'Tampak tidak sakit', 'Kompos mentis', 'gcs', '120/80', '80', '36', 'rr', 'pemeriksaan fisik', 'pemeriksaan penunjang', 'obat', 'diagnosa kerja', 'diagnosa banding', 'rencana pemeriksaan penunjang', 'rencana diet', 'D0000063');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `asesmen_awal_medis`
--
ALTER TABLE `asesmen_awal_medis`
  ADD PRIMARY KEY (`no_rawat`),
  ADD KEY `nip` (`nip`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `asesmen_awal_medis`
--
ALTER TABLE `asesmen_awal_medis`
  ADD CONSTRAINT `asesmen_awal_medis_ibfk1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asesmen_awal_medis_ibfk2` FOREIGN KEY (`nip`) REFERENCES `dokter` (`kd_dokter`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
