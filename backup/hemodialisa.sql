-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 09 Agu 2021 pada 06.13
-- Versi server: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hemodialisa`
--

CREATE TABLE `hemodialisa` (
  `no_rawat` varchar(17) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kd_dokter` varchar(20) DEFAULT NULL,
  `lama` varchar(5) DEFAULT NULL,
  `akses` varchar(30) DEFAULT NULL,
  `dialist` varchar(30) DEFAULT NULL,
  `transfusi` varchar(5) DEFAULT NULL,
  `penarikan` varchar(5) DEFAULT NULL,
  `qb` varchar(5) DEFAULT NULL,
  `qd` varchar(5) DEFAULT NULL,
  `ureum` varchar(10) DEFAULT NULL,
  `hb` varchar(10) DEFAULT NULL,
  `hbsag` varchar(10) DEFAULT NULL,
  `creatinin` varchar(10) DEFAULT NULL,
  `hiv` varchar(10) DEFAULT NULL,
  `hcv` varchar(10) DEFAULT NULL,
  `lain` varchar(200) DEFAULT NULL,
  `kd_penyakit` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hemodialisa`
--

INSERT INTO `hemodialisa` (`no_rawat`, `tanggal`, `kd_dokter`, `lama`, `akses`, `dialist`, `transfusi`, `penarikan`, `qb`, `qd`, `ureum`, `hb`, `hbsag`, `creatinin`, `hiv`, `hcv`, `lain`, `kd_penyakit`) VALUES
('2021/05/14/000001', '2021-05-14 14:18:07', 'D0000009', '0', 'Femoral / Cimino', 'Bicarbonat', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '-');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `hemodialisa`
--
ALTER TABLE `hemodialisa`
  ADD PRIMARY KEY (`no_rawat`,`tanggal`),
  ADD KEY `kd_penyakit` (`kd_penyakit`),
  ADD KEY `kd_dokter` (`kd_dokter`),
  ADD KEY `no_rawat` (`no_rawat`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hemodialisa`
--
ALTER TABLE `hemodialisa`
  ADD CONSTRAINT `hemodialisa_ibfk_1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hemodialisa_ibfk_2` FOREIGN KEY (`kd_penyakit`) REFERENCES `penyakit` (`kd_penyakit`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hemodialisa_ibfk_3` FOREIGN KEY (`kd_dokter`) REFERENCES `dokter` (`kd_dokter`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
