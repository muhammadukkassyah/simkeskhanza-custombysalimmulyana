-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 16 Des 2021 pada 20.34
-- Versi server: 10.3.32-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mcu`
--

CREATE TABLE `mcu` (
  `no_rawat` varchar(17) NOT NULL,
  `bb` varchar(3) NOT NULL,
  `tb` varchar(3) NOT NULL,
  `tensi` varchar(7) NOT NULL,
  `nadi` varchar(3) NOT NULL,
  `refleksfisiologis` enum('Positif','Negatif') NOT NULL,
  `reflekspatologis` enum('Positif','Negatif') NOT NULL,
  `visuskanan` varchar(3) NOT NULL,
  `visuskiri` varchar(3) NOT NULL,
  `kacamata` varchar(100) NOT NULL,
  `ketkacamata` varchar(100) NOT NULL,
  `butawarna` enum('Positif','Negatif','Parsial') NOT NULL,
  `telingakanan` varchar(100) NOT NULL,
  `telingakiri` varchar(100) NOT NULL,
  `tesberbisik` varchar(100) NOT NULL,
  `hidung` varchar(100) NOT NULL,
  `tenggorokan` varchar(100) NOT NULL,
  `paru` varchar(100) NOT NULL,
  `jantung` varchar(100) NOT NULL,
  `abdomen` varchar(100) NOT NULL,
  `ekstremitas` varchar(100) NOT NULL,
  `darahrutin` varchar(100) NOT NULL,
  `urin` varchar(100) NOT NULL,
  `rontgen` varchar(100) NOT NULL,
  `kesimpulan` varchar(100) NOT NULL,
  `saran` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kd_dokter` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mcu`
--

INSERT INTO `mcu` (`no_rawat`, `bb`, `tb`, `tensi`, `nadi`, `refleksfisiologis`, `reflekspatologis`, `visuskanan`, `visuskiri`, `kacamata`, `ketkacamata`, `butawarna`, `telingakanan`, `telingakiri`, `tesberbisik`, `hidung`, `tenggorokan`, `paru`, `jantung`, `abdomen`, `ekstremitas`, `darahrutin`, `urin`, `rontgen`, `kesimpulan`, `saran`, `tanggal`, `kd_dokter`) VALUES
('2021/11/23/000001', '50', '165', '120/80', '80', 'Positif', 'Negatif', '6/6', '66', 'Negatif', 'Normal (tidak pakai kacamata)', 'Negatif', 'Normal', 'Normal', 'Normal', 'Normal', 'Nor', 'no', '', '', '', 'darah rutin', 'urin rutin', 'rontgen', 'kesimpulan', 'saran', '2021-11-25 07:06:18', 'D0000063'),
('2021/11/25/000001', '50', '165', '120/80', '80', 'Positif', 'Negatif', '6/6', '6/6', 'Negatif', 'Normal (tidak pakai kacamata)', 'Negatif', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'darah rutin', 'urint rutin', 'rontgen', 'kesimpulan', 'saran', '2021-11-25 17:12:08', '-'),
('2021/12/03/000001', '50', '165', '120/80', '80', 'Positif', 'Negatif', '6/6', '6/6', 'Negatif', 'Normal (tidak pakai kacamata)', 'Negatif', 'Normal', 'Normaltetetgasw', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'Normal', 'darah rutin', 'urin rutin', 'gdasgagd', 'sehat', '-', '2021-12-03 07:57:11', 'D0000004');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mcu`
--
ALTER TABLE `mcu`
  ADD PRIMARY KEY (`no_rawat`,`tanggal`),
  ADD KEY `kd_dokter` (`kd_dokter`),
  ADD KEY `no_rawat` (`no_rawat`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mcu`
--
ALTER TABLE `mcu`
  ADD CONSTRAINT `mcu_ibfk_1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
