-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 18 Jul 2021 pada 19.29
-- Versi server: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `rujuk`
--

CREATE TABLE `rujuk` (
  `no_rujuk` varchar(40) NOT NULL,
  `no_rawat` varchar(17) DEFAULT NULL,
  `tgl_rujuk` date NOT NULL,
  `rujuk_ke` varchar(150) DEFAULT NULL,
  `bagian` varchar(50) NOT NULL,
  `kat_rujuk` enum('-','Bedah','Non Bedah','Kebidanan','Anak') DEFAULT NULL,
  `ambulance` enum('-','AGD','SENDIRI','SWASTA') DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `nokendaraan` text NOT NULL,
  `alasanrujuk` enum('Perawatan Khusus','Permintaan Khusus') NOT NULL,
  `pendamping` enum('-','Dokter','Perawat','Bidan','Keluarga') NOT NULL,
  `namapendamping` varchar(50) NOT NULL,
  `ku` varchar(50) NOT NULL,
  `gcs` varchar(15) NOT NULL,
  `respirasi` varchar(5) NOT NULL,
  `suhu` varchar(5) NOT NULL,
  `tensi` varchar(10) NOT NULL,
  `nadi` varchar(5) NOT NULL,
  `spo2` varchar(5) NOT NULL,
  `alergi` varchar(50) NOT NULL,
  `pemeriksaan` varchar(500) NOT NULL,
  `pemeriksaanpj` varchar(100) NOT NULL,
  `pemeriksaanlab` varchar(100) NOT NULL,
  `terapi` varchar(500) NOT NULL,
  `keterangan_diagnosa` text DEFAULT NULL,
  `kd_dokter` varchar(20) DEFAULT NULL,
  `jam` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rujuk`
--

INSERT INTO `rujuk` (`no_rujuk`, `no_rawat`, `tgl_rujuk`, `rujuk_ke`, `bagian`, `kat_rujuk`, `ambulance`, `keterangan`, `nokendaraan`, `alasanrujuk`, `pendamping`, `namapendamping`, `ku`, `gcs`, `respirasi`, `suhu`, `tensi`, `nadi`, `spo2`, `alergi`, `pemeriksaan`, `pemeriksaanpj`, `pemeriksaanlab`, `terapi`, `keterangan_diagnosa`, `kd_dokter`, `jam`) VALUES
('R000000001', '2021/03/29/000001', '2021-07-05', 'RSUD', 'IGD', '-', '-', '-', '', 'Perawatan Khusus', '-', '', 'Sedang', '4,5,6', '20', '20', '120/80', '80', '90', '-', 'pemeriksaan fisik', 'pemeriksaan radiolgi', 'pemeriksaan lab', 'terapi', 'sdb', 'D0000032', '03:11:09'),
('R000000002', '2021/07/14/000001', '2021-07-14', 'RS Setempat', 'IGD', '-', '-', '-', '', 'Perawatan Khusus', '-', '', 'Baik', '5', '20', '36', '120/80', '80', '90', '-', 'pemeriksaan fisik', 'pemeriksan radiologi', 'pemeriksaan lab', 'terapi', 'ckd', 'D0000006', '12:01:19'),
('R000000003', '2021/07/18/000002', '2021-07-18', 'RSUD Karawang', 'IGD', '-', 'SENDIRI', '-', 'T1335H', 'Perawatan Khusus', 'Dokter', 'mr x', 'Baik', 'gcs', 'rrr', 'dsgs', 'dg', 'd', 'dga', 'gda', 'dgsa', 'gdas', 'gdasg', 'gdas', 'gdasgs', '12345678901', '17:29:16');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `rujuk`
--
ALTER TABLE `rujuk`
  ADD KEY `kd_dokter` (`kd_dokter`),
  ADD KEY `no_rawat` (`no_rawat`),
  ADD KEY `rujuk_ke` (`rujuk_ke`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `rujuk`
--
ALTER TABLE `rujuk`
  ADD CONSTRAINT `rujuk_ibfk_1` FOREIGN KEY (`kd_dokter`) REFERENCES `dokter` (`kd_dokter`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rujuk_ibfk_2` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
