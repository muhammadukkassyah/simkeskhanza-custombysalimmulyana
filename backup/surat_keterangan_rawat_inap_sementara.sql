-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 20 Jul 2021 pada 11.37
-- Versi server: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_keterangan_rawat_inap_sementara`
--

CREATE TABLE `surat_keterangan_rawat_inap_sementara` (
  `no_surat` varchar(17) NOT NULL,
  `no_rawat` varchar(17) DEFAULT NULL,
  `tanggalawal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat_keterangan_rawat_inap_sementara`
--

INSERT INTO `surat_keterangan_rawat_inap_sementara` (`no_surat`, `no_rawat`, `tanggalawal`) VALUES
('SKRS20210601001', '2021/03/29/000001', '2021-06-01'),
('SKRS20210601002', '2021/01/07/000003', '2021-06-01'),
('SKRS20210602001', '2020/12/20/000001', '2021-06-02'),
('SKRS20210703001', '2021/01/07/000003', '2021-07-03'),
('SKRS20210703002', '2021/01/07/000003', '2021-07-03'),
('SKRS20210715001', '2021/01/07/000003', '2021-07-15'),
('SKRS20210715002', '2020/12/20/000001', '2021-07-15'),
('SKRS20210715003', '2021/07/04/000001', '2021-07-15');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `surat_keterangan_rawat_inap_sementara`
--
ALTER TABLE `surat_keterangan_rawat_inap_sementara`
  ADD PRIMARY KEY (`no_surat`),
  ADD KEY `no_rawat` (`no_rawat`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `surat_keterangan_rawat_inap_sementara`
--
ALTER TABLE `surat_keterangan_rawat_inap_sementara`
  ADD CONSTRAINT `surat_keterangan_rawat_inap_sementara_ibfk_1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
