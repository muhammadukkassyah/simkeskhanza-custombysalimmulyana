-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 17 Jul 2021 pada 14.48
-- Versi server: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `usg_kehamilan1`
--

CREATE TABLE `usg_kehamilan1` (
  `no_rawat` varchar(17) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `umur` varchar(20) NOT NULL,
  `hpht` date NOT NULL,
  `usiakehamilanhpht` varchar(10) NOT NULL,
  `indikasi` varchar(100) NOT NULL,
  `keluhan` varchar(100) NOT NULL,
  `kantonggestasi` enum('Ada','Tidak ada') NOT NULL,
  `lokasi` enum('Intrauterine','Ekstrauterine','-') NOT NULL,
  `ukuran` varchar(50) NOT NULL,
  `embrio` enum('Ada','Tidak ada') NOT NULL,
  `crl` varchar(3) NOT NULL,
  `fetalpulse` varchar(3) NOT NULL,
  `frekpulse` varchar(3) NOT NULL,
  `jumlahjanin` varchar(2) NOT NULL,
  `umurkehamilan` varchar(3) NOT NULL,
  `hpl` date NOT NULL,
  `tbj` varchar(4) NOT NULL,
  `diagnosa` varchar(100) NOT NULL,
  `rencana` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kd_dokter` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usg_kehamilan1`
--

INSERT INTO `usg_kehamilan1` (`no_rawat`, `tgl_lahir`, `umur`, `hpht`, `usiakehamilanhpht`, `indikasi`, `keluhan`, `kantonggestasi`, `lokasi`, `ukuran`, `embrio`, `crl`, `fetalpulse`, `frekpulse`, `jumlahjanin`, `umurkehamilan`, `hpl`, `tbj`, `diagnosa`, `rencana`, `tanggal`, `kd_dokter`) VALUES
('2021/05/01/000001', '2021-05-11', '29 Th 2 Bl 23 Hr', '2021-05-11', '', '-', '--', 'Ada', 'Intrauterine', '0', 'Ada', '0', 'Ada', '0', '0', '0', '2021-05-11', '0', '0', '0', '2021-05-11 08:19:23', 'D0000014'),
('2021/05/10/000001', '2021-05-11', '1 Th 1 Bl 14 Hr', '2021-05-11', '', '-', '-', 'Ada', 'Intrauterine', '0', 'Ada', '0', 'Ada', '0', '0', '0', '2021-05-11', '0', '-', '-', '2021-05-11 08:16:33', 'D0000063'),
('2021/05/11/000001', '2021-05-11', '30 Th 3 Bl 10 Hr', '2021-05-11', '', 'indikasi', 'keluhan', 'Ada', 'Intrauterine', '1', 'Ada', '2', 'Ada', '100', '1', '20', '2021-05-11', '500', 'diagnosa', 'rencana', '2021-05-11 08:24:53', 'D0000006'),
('2021/05/11/000002', '2021-05-11', '25 Th 9 Bl 23 Hr', '2021-01-01', '12', 'ANC rutin', 'Mual muntah', 'Ada', 'Intrauterine', '1', 'Ada', '2', 'Ada', '120', '1', '13', '2021-09-11', '100', 'G1P1A0 hamil 13 minggu', 'Kontrol 1 bulan berikutnya', '2021-05-11 14:23:26', '12345678901'),
('2021/05/14/000001', '2021-05-14', '22 Th 9 Bl 21 Hr', '2021-05-14', '000', 'gdagas', 'dgsagdas00000gdsagsa', 'Ada', 'Intrauterine', '', 'Ada', '0', 'Ada', '0', '0', '00', '2021-05-14', '0', '0', '0', '2021-05-14 21:57:27', 'D0000041'),
('2021/07/15/000001', '2021-07-15', '31 Th 6 Bl 14 Hr', '2021-07-15', '40', 'indikasi pemeriksaan', 'keluhan', 'Ada', 'Intrauterine', '50', 'Ada', '5', 'Ada', '80', '5', '40', '2021-07-15', '5000', 'diagnosa', 'rencana', '2021-07-15 11:00:39', 'D0000041'),
('2021/07/17/000001', '2021-07-17', '33 Th 5 Bl 16 Hr', '2021-07-17', '30', 'ANC', 'Mual muntah', 'Ada', 'Intrauterine', '5', 'Ada', '20', 'Ada', '50', '1', '12', '2021-07-17', '5000', 'G1A1P1', 'Kontrol ulang', '2021-07-17 12:13:54', 'D0000052');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `usg_kehamilan1`
--
ALTER TABLE `usg_kehamilan1`
  ADD PRIMARY KEY (`no_rawat`,`tanggal`),
  ADD KEY `kd_dokter` (`kd_dokter`),
  ADD KEY `no_rawat` (`no_rawat`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `usg_kehamilan1`
--
ALTER TABLE `usg_kehamilan1`
  ADD CONSTRAINT `usg_kehamilan1_ibfk_1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usg_kehamilan1_ibfk_2` FOREIGN KEY (`kd_dokter`) REFERENCES `dokter` (`kd_dokter`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
