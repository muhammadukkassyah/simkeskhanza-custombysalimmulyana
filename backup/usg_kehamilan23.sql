-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 18 Jul 2021 pada 09.47
-- Versi server: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikcustomsalim`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `usg_kehamilan23`
--

CREATE TABLE `usg_kehamilan23` (
  `no_rawat` varchar(17) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `umur` varchar(20) NOT NULL,
  `hpht` date NOT NULL,
  `usiakehamilanhpht` varchar(10) NOT NULL,
  `indikasi` varchar(100) NOT NULL,
  `keluhan` varchar(100) NOT NULL,
  `janin` enum('Hidup','Tidak hidup') NOT NULL,
  `jumlahjanin` enum('Tunggal','Gemeli') NOT NULL,
  `lokasi` enum('Intrauterine','Ekstrauterine','-') NOT NULL,
  `letakjanin` enum('Memanjang') NOT NULL,
  `presentasi` enum('Kepala','Sungsang','Melintang') NOT NULL,
  `bpd` varchar(5) NOT NULL,
  `hc` varchar(5) NOT NULL,
  `ac` varchar(5) NOT NULL,
  `fl` varchar(5) NOT NULL,
  `frekpulse` varchar(3) NOT NULL,
  `jk` enum('Belum Dapat Di Tentukan','Laki-laki','Perempuan') NOT NULL,
  `plasenta` enum('Normal','Tidak') NOT NULL,
  `implantasi` enum('Fundus','Corpus','Segmen bawah uterus') NOT NULL,
  `derajat` enum('0','1','2','3') NOT NULL,
  `afi1` varchar(5) NOT NULL,
  `afi2` varchar(5) NOT NULL,
  `afi3` varchar(5) NOT NULL,
  `afi4` varchar(5) NOT NULL,
  `totalafi` varchar(5) NOT NULL,
  `ketuban` enum('Cukup','Kurang','Lebih') NOT NULL,
  `umurkehamilan` varchar(3) NOT NULL,
  `hpl` date NOT NULL,
  `tbj` varchar(4) NOT NULL,
  `diagnosa` varchar(100) NOT NULL,
  `rencana` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kd_dokter` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usg_kehamilan23`
--

INSERT INTO `usg_kehamilan23` (`no_rawat`, `tgl_lahir`, `umur`, `hpht`, `usiakehamilanhpht`, `indikasi`, `keluhan`, `janin`, `jumlahjanin`, `lokasi`, `letakjanin`, `presentasi`, `bpd`, `hc`, `ac`, `fl`, `frekpulse`, `jk`, `plasenta`, `implantasi`, `derajat`, `afi1`, `afi2`, `afi3`, `afi4`, `totalafi`, `ketuban`, `umurkehamilan`, `hpl`, `tbj`, `diagnosa`, `rencana`, `tanggal`, `kd_dokter`) VALUES
('2021/05/12/000001', '2021-05-12', '0 Th 10 Bl 18 Hr', '2021-05-12', '10', 'Indikasi', 'Keluhan', 'Hidup', 'Tunggal', 'Intrauterine', 'Memanjang', 'Kepala', 'bpd', 'hc', 'ac', 'fl', 'djj', 'Belum Dapat Di Tentukan', 'Normal', 'Fundus', '0', 'afi1', 'afi2', 'afi3', 'afi4', 'ttlaf', 'Cukup', '40', '2021-05-12', '500', 'diagnosa', 'rencana', '2021-05-12 07:11:12', '12345678901'),
('2021/05/14/000001', '2021-05-14', '22 Th 9 Bl 21 Hr', '2021-05-14', '-', '-', '-dggdasg', 'Hidup', 'Tunggal', 'Intrauterine', 'Memanjang', 'Kepala', '-', '-', '-', '-', '-', 'Belum Dapat Di Tentukan', 'Normal', 'Fundus', '0', '-', '-', '-', '-', '-', 'Cukup', '0', '2021-05-14', '0', '-', '-', '2021-05-14 21:59:47', 'D0000045'),
('2021/05/15/000001', '2021-05-15', '25 Th 9 Bl 27 Hr', '2021-05-15', '20', 'ANC rutin', '-', 'Hidup', 'Tunggal', 'Intrauterine', 'Memanjang', 'Kepala', '5', '5', '52', '20', '120', 'Belum Dapat Di Tentukan', 'Normal', 'Fundus', '0', '2', '2', '2', '2', '2', 'Cukup', '40', '2021-05-15', '2500', 'G1P1 A0 40 minggu', 'Kontrol 1 minggu lagi', '2021-05-15 14:57:32', '12345678901'),
('2021/07/17/000001', '2021-07-17', '33 Th 5 Bl 16 Hr', '2021-07-17', '30', 'indikasi', 'keluhan', 'Hidup', 'Tunggal', 'Intrauterine', 'Memanjang', 'Kepala', '1', '2', '3', '4', '5', 'Belum Dapat Di Tentukan', 'Normal', 'Fundus', '0', '6', '7', '8', '9', '24', 'Cukup', '30', '2021-07-17', '5000', 'diagnosa', 'rencana', '2021-07-17 18:49:07', 'D0000006'),
('2021/07/18/000001', '2021-07-18', '31 Th 6 Bl 17 Hr', '2021-07-18', '50', 'Kontrol rutin', 'Tidak ada keluhan', 'Hidup', 'Tunggal', 'Intrauterine', 'Memanjang', 'Kepala', '1', '2', '3', '4', '5', 'Belum Dapat Di Tentukan', 'Normal', 'Fundus', '0', '1', '2', '3', '4', '15', 'Cukup', '20', '2021-07-18', '8000', 'G1P1A0 38 minggu', 'Kontrol tiap minggu', '2021-07-18 08:50:14', 'D0000045');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `usg_kehamilan23`
--
ALTER TABLE `usg_kehamilan23`
  ADD PRIMARY KEY (`no_rawat`,`tanggal`),
  ADD KEY `kd_dokter` (`kd_dokter`),
  ADD KEY `no_rawat` (`no_rawat`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `usg_kehamilan23`
--
ALTER TABLE `usg_kehamilan23`
  ADD CONSTRAINT `usg_kehamilan23_ibfk_1` FOREIGN KEY (`no_rawat`) REFERENCES `reg_periksa` (`no_rawat`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usg_kehamilan23_ibfk_2` FOREIGN KEY (`kd_dokter`) REFERENCES `dokter` (`kd_dokter`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
