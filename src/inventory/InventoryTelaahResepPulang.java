/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgRujuk.java
 *
 * Created on 31 Mei 10, 20:19:56
 */

package inventory;

import rekammedis.*;
import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class InventoryTelaahResepPulang extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;    
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private String status,KejelasanTulisan,BenarNamaPasien,BenarNamaObat,BenarDosis,BenarWaktudanFrekuensiPemberian,BenarCaraPemberian,AdaTidaknyaPolifarmasi,AdaTidaknyaDuplikasi,InteraksiObat,BenarObat,BenarWaktu,BenarFrekuensi,BenarDosis2,BenarRute,BenarIdentitas;
    private SimpleDateFormat tanggalNow = new SimpleDateFormat("yyyy-MM-dd");
//    private SimpleDateFormat tanggalNow = new SimpleDateFormat("dd-MM-yyyy");
    private SimpleDateFormat jamNow = new SimpleDateFormat("HH:mm:ss");
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public InventoryTelaahResepPulang(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Resep","Tanggal Resep","Jam Resep","No.Rawat","No.RM","Nama Pasien","Kode Dokter","Dokter","NIP","Petugas","Status","Kejelasan Tulisan","Benar nama pasien","Benar nama obat",
            "Benar dosis","Benar waktu dan frekuensi pemberian","Benar cara pemberian","Ada tidaknya polifarmasi","Ada tidaknya duplikasi","Interaksi obat yang mungkin terjadi","Benar obat",
            "Benar waktu","Benar frekuensi","Benar dosis","Benar rute","Benar identitas pasien","Tanggal Telaah","Jam Telaah"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 28; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(180);
            }else if(i==1){
                column.setPreferredWidth(180);
            }else if(i==2){
                column.setPreferredWidth(180);
            }else if(i==3){
                column.setPreferredWidth(180);
            }else if(i==4){
                column.setPreferredWidth(180);
            }else if(i==5){
                column.setPreferredWidth(180);
            }else if(i==6){
                column.setPreferredWidth(180);
            }else if(i==7){
                column.setPreferredWidth(180);
            }else if(i==8){
                column.setPreferredWidth(180);
            }else if(i==9){
                column.setPreferredWidth(180);
            }else if(i==10){
                column.setPreferredWidth(180);
            }else if(i==11){
                column.setPreferredWidth(180);
            }else if(i==12){
                column.setPreferredWidth(180);
            }else if(i==13){
                column.setPreferredWidth(180);
            }else if(i==14){
                column.setPreferredWidth(180);
            }else if(i==15){
                column.setPreferredWidth(180);
            }else if(i==16){
                column.setPreferredWidth(180);
            }else if(i==17){
                column.setPreferredWidth(180);
            }else if(i==18){
                column.setPreferredWidth(180);
            }else if(i==19){
                column.setPreferredWidth(180);
            }else if(i==20){
                column.setPreferredWidth(180);
            }else if(i==21){
                column.setPreferredWidth(180);
            }else if(i==22){
                column.setPreferredWidth(180);
            }else if(i==23){
                column.setPreferredWidth(180);
            }else if(i==24){
                column.setPreferredWidth(180);
            }else if(i==25){
                column.setPreferredWidth(180);
            }else if(i==26){
                column.setPreferredWidth(180);
            }else if(i==27){
                column.setPreferredWidth(180);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());

        TNoResep.setDocument(new batasInput((byte)14).getKata(TNoResep));
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        KodeDokter.setDocument(new batasInput((byte)20).getKata(KodeDokter));
        KdPetugas.setDocument(new batasInput((byte)20).getKata(KdPetugas));
        
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        petugas.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(petugas.getTable().getSelectedRow()!= -1){ 
                    KdPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),0).toString());
                    NmPetugas.setText(petugas.getTable().getValueAt(petugas.getTable().getSelectedRow(),1).toString());   
                }              
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        ChkInput.setSelected(false);
        isForm();
      
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        buttonGroup8 = new javax.swing.ButtonGroup();
        buttonGroup9 = new javax.swing.ButtonGroup();
        buttonGroup10 = new javax.swing.ButtonGroup();
        buttonGroup11 = new javax.swing.ButtonGroup();
        buttonGroup12 = new javax.swing.ButtonGroup();
        buttonGroup13 = new javax.swing.ButtonGroup();
        buttonGroup14 = new javax.swing.ButtonGroup();
        buttonGroup15 = new javax.swing.ButtonGroup();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        KdPetugas = new widget.TextBox();
        NmPetugas = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel10 = new widget.Label();
        jLabel5 = new widget.Label();
        TNoResep = new widget.TextBox();
        label15 = new widget.Label();
        KodeDokter = new widget.TextBox();
        NamaDokter = new widget.TextBox();
        label16 = new widget.Label();
        label17 = new widget.Label();
        KejelasanTulisanYa = new widget.RadioButton();
        KejelasanTulisanTidak = new widget.RadioButton();
        label19 = new widget.Label();
        BenarNamaPasienYa = new widget.RadioButton();
        BenarNamaPasienTidak = new widget.RadioButton();
        label21 = new widget.Label();
        label22 = new widget.Label();
        label23 = new widget.Label();
        label25 = new widget.Label();
        label26 = new widget.Label();
        label27 = new widget.Label();
        label28 = new widget.Label();
        label29 = new widget.Label();
        label30 = new widget.Label();
        label31 = new widget.Label();
        label32 = new widget.Label();
        label33 = new widget.Label();
        label34 = new widget.Label();
        label35 = new widget.Label();
        BenarNamaObatYa = new widget.RadioButton();
        BenarNamaObatTidak = new widget.RadioButton();
        BenarDosisYa = new widget.RadioButton();
        BenarDosisTidak = new widget.RadioButton();
        BenarWaktudanFrekuensiPemberianYa = new widget.RadioButton();
        BenarWaktudanFrekuensiPemberianTidak = new widget.RadioButton();
        BenarCaraPemberianYa = new widget.RadioButton();
        BenarCaraPemberianTidak = new widget.RadioButton();
        AdaTidaknyaPolifarmasiYa = new widget.RadioButton();
        AdaTidaknyaPolifarmasiTidak = new widget.RadioButton();
        AdaTidaknyaDuplikasiYa = new widget.RadioButton();
        AdaTidaknyaDuplikasiTidak = new widget.RadioButton();
        InteraksiObatYa = new widget.RadioButton();
        InteraksiObatTidak = new widget.RadioButton();
        BenarIdentitasYa = new widget.RadioButton();
        BenarIdentitasTidak = new widget.RadioButton();
        BenarObatYa = new widget.RadioButton();
        BenarObatTidak = new widget.RadioButton();
        BenarWaktuYa = new widget.RadioButton();
        BenarWaktuTidak = new widget.RadioButton();
        BenarFrekuensiYa = new widget.RadioButton();
        BenarFrekuensiTidak = new widget.RadioButton();
        BenarDosis2Ya = new widget.RadioButton();
        BenarDosis2Tidak = new widget.RadioButton();
        BenarRuteYa = new widget.RadioButton();
        BenarRuteTidak = new widget.RadioButton();
        jLabel8 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        TglRw = new widget.TextBox();
        jLabel9 = new widget.Label();
        JamRw = new widget.TextBox();
        jLabel11 = new widget.Label();
        Status = new widget.TextBox();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Telaah Resep Pulang ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass8.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass8.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Rawat :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(67, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10-04-2022" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10-04-2022" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(310, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnAll);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 448));
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        scrollInput.setName("scrollInput"); // NOI18N

        FormInput.setBackground(new java.awt.Color(250, 255, 245));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 555));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 141, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(331, 10, 280, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(217, 10, 112, 23);

        label14.setText("Petugas :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 70, 60, 23);

        KdPetugas.setEditable(false);
        KdPetugas.setName("KdPetugas"); // NOI18N
        KdPetugas.setPreferredSize(new java.awt.Dimension(80, 23));
        KdPetugas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdPetugasKeyPressed(evt);
            }
        });
        FormInput.add(KdPetugas);
        KdPetugas.setBounds(70, 70, 140, 23);

        NmPetugas.setEditable(false);
        NmPetugas.setName("NmPetugas"); // NOI18N
        NmPetugas.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmPetugas);
        NmPetugas.setBounds(210, 70, 270, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(480, 70, 28, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        jLabel5.setText("No.Resep :");
        jLabel5.setName("jLabel5"); // NOI18N
        FormInput.add(jLabel5);
        jLabel5.setBounds(490, 40, 60, 23);

        TNoResep.setHighlighter(null);
        TNoResep.setName("TNoResep"); // NOI18N
        TNoResep.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoResepKeyPressed(evt);
            }
        });
        FormInput.add(TNoResep);
        TNoResep.setBounds(560, 40, 141, 23);

        label15.setText("VERIFIKASI OBAT");
        label15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label15.setName("label15"); // NOI18N
        label15.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label15);
        label15.setBounds(410, 130, 110, 23);

        KodeDokter.setEditable(false);
        KodeDokter.setName("KodeDokter"); // NOI18N
        KodeDokter.setPreferredSize(new java.awt.Dimension(80, 23));
        KodeDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KodeDokterKeyPressed(evt);
            }
        });
        FormInput.add(KodeDokter);
        KodeDokter.setBounds(70, 40, 141, 23);

        NamaDokter.setEditable(false);
        NamaDokter.setName("NamaDokter"); // NOI18N
        NamaDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NamaDokter);
        NamaDokter.setBounds(210, 40, 270, 23);

        label16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label16.setText("1.Kejelasan Tulisan :");
        label16.setName("label16"); // NOI18N
        label16.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label16);
        label16.setBounds(20, 150, 120, 23);

        label17.setText("Dokter  :");
        label17.setName("label17"); // NOI18N
        label17.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label17);
        label17.setBounds(0, 40, 60, 23);

        buttonGroup1.add(KejelasanTulisanYa);
        KejelasanTulisanYa.setText("Ya");
        KejelasanTulisanYa.setName("KejelasanTulisanYa"); // NOI18N
        KejelasanTulisanYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(KejelasanTulisanYa);
        KejelasanTulisanYa.setBounds(270, 150, 45, 23);

        buttonGroup1.add(KejelasanTulisanTidak);
        KejelasanTulisanTidak.setText("Tidak");
        KejelasanTulisanTidak.setName("KejelasanTulisanTidak"); // NOI18N
        FormInput.add(KejelasanTulisanTidak);
        KejelasanTulisanTidak.setBounds(310, 150, 60, 23);

        label19.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label19.setText("2. Benar Nama Pasien :");
        label19.setName("label19"); // NOI18N
        label19.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label19);
        label19.setBounds(20, 170, 130, 23);

        buttonGroup2.add(BenarNamaPasienYa);
        BenarNamaPasienYa.setText("Ya");
        BenarNamaPasienYa.setName("BenarNamaPasienYa"); // NOI18N
        BenarNamaPasienYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarNamaPasienYa);
        BenarNamaPasienYa.setBounds(270, 170, 45, 23);

        buttonGroup2.add(BenarNamaPasienTidak);
        BenarNamaPasienTidak.setText("Tidak");
        BenarNamaPasienTidak.setName("BenarNamaPasienTidak"); // NOI18N
        FormInput.add(BenarNamaPasienTidak);
        BenarNamaPasienTidak.setBounds(310, 170, 60, 23);

        label21.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label21.setText("3. Benar nama obat:");
        label21.setName("label21"); // NOI18N
        label21.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label21);
        label21.setBounds(20, 190, 120, 23);

        label22.setText("5. Benar waktu dan frekuensi pemberian:");
        label22.setName("label22"); // NOI18N
        label22.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label22);
        label22.setBounds(20, 230, 200, 20);

        label23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label23.setText("4. Benar dosis");
        label23.setName("label23"); // NOI18N
        label23.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label23);
        label23.setBounds(20, 210, 150, 23);

        label25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label25.setText("6. Benar cara pemberian");
        label25.setName("label25"); // NOI18N
        label25.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label25);
        label25.setBounds(20, 250, 170, 23);

        label26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label26.setText("7. Ada tidaknya polifarmasi");
        label26.setName("label26"); // NOI18N
        label26.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label26);
        label26.setBounds(20, 270, 140, 23);

        label27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label27.setText("8. Ada tidaknya duplikasi");
        label27.setName("label27"); // NOI18N
        label27.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label27);
        label27.setBounds(20, 290, 160, 23);

        label28.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label28.setText("9. Interakti obat yang mungkin terjadii :");
        label28.setName("label28"); // NOI18N
        label28.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label28);
        label28.setBounds(20, 310, 230, 23);

        label29.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label29.setText("6. Benar identitas pasien:");
        label29.setName("label29"); // NOI18N
        label29.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label29);
        label29.setBounds(410, 250, 130, 23);

        label30.setText("Telaah Resep");
        label30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        label30.setName("label30"); // NOI18N
        label30.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label30);
        label30.setBounds(0, 130, 80, 23);

        label31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label31.setText("1. Benar Obat:");
        label31.setName("label31"); // NOI18N
        label31.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label31);
        label31.setBounds(410, 150, 150, 23);

        label32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label32.setText("2. Benar Waktu :");
        label32.setName("label32"); // NOI18N
        label32.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label32);
        label32.setBounds(410, 170, 100, 23);

        label33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label33.setText("3. Benar Frekuensi :");
        label33.setName("label33"); // NOI18N
        label33.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label33);
        label33.setBounds(410, 190, 100, 23);

        label34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label34.setText("4. Benar Dosis :");
        label34.setName("label34"); // NOI18N
        label34.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label34);
        label34.setBounds(410, 210, 150, 23);

        label35.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label35.setText("5. Benar Rute :");
        label35.setName("label35"); // NOI18N
        label35.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label35);
        label35.setBounds(410, 230, 160, 23);

        buttonGroup3.add(BenarNamaObatYa);
        BenarNamaObatYa.setText("Ya");
        BenarNamaObatYa.setName("BenarNamaObatYa"); // NOI18N
        BenarNamaObatYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarNamaObatYa);
        BenarNamaObatYa.setBounds(270, 190, 45, 23);

        buttonGroup3.add(BenarNamaObatTidak);
        BenarNamaObatTidak.setText("Tidak");
        BenarNamaObatTidak.setName("BenarNamaObatTidak"); // NOI18N
        FormInput.add(BenarNamaObatTidak);
        BenarNamaObatTidak.setBounds(310, 190, 60, 23);

        buttonGroup4.add(BenarDosisYa);
        BenarDosisYa.setText("Ya");
        BenarDosisYa.setName("BenarDosisYa"); // NOI18N
        BenarDosisYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarDosisYa);
        BenarDosisYa.setBounds(270, 210, 45, 23);

        buttonGroup4.add(BenarDosisTidak);
        BenarDosisTidak.setText("Tidak");
        BenarDosisTidak.setName("BenarDosisTidak"); // NOI18N
        FormInput.add(BenarDosisTidak);
        BenarDosisTidak.setBounds(310, 210, 60, 23);

        buttonGroup5.add(BenarWaktudanFrekuensiPemberianYa);
        BenarWaktudanFrekuensiPemberianYa.setText("Ya");
        BenarWaktudanFrekuensiPemberianYa.setName("BenarWaktudanFrekuensiPemberianYa"); // NOI18N
        BenarWaktudanFrekuensiPemberianYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarWaktudanFrekuensiPemberianYa);
        BenarWaktudanFrekuensiPemberianYa.setBounds(270, 230, 45, 23);

        buttonGroup5.add(BenarWaktudanFrekuensiPemberianTidak);
        BenarWaktudanFrekuensiPemberianTidak.setText("Tidak");
        BenarWaktudanFrekuensiPemberianTidak.setName("BenarWaktudanFrekuensiPemberianTidak"); // NOI18N
        FormInput.add(BenarWaktudanFrekuensiPemberianTidak);
        BenarWaktudanFrekuensiPemberianTidak.setBounds(310, 230, 60, 23);

        buttonGroup6.add(BenarCaraPemberianYa);
        BenarCaraPemberianYa.setText("Ya");
        BenarCaraPemberianYa.setName("BenarCaraPemberianYa"); // NOI18N
        BenarCaraPemberianYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarCaraPemberianYa);
        BenarCaraPemberianYa.setBounds(270, 250, 45, 23);

        buttonGroup6.add(BenarCaraPemberianTidak);
        BenarCaraPemberianTidak.setText("Tidak");
        BenarCaraPemberianTidak.setName("BenarCaraPemberianTidak"); // NOI18N
        FormInput.add(BenarCaraPemberianTidak);
        BenarCaraPemberianTidak.setBounds(310, 250, 60, 20);

        buttonGroup7.add(AdaTidaknyaPolifarmasiYa);
        AdaTidaknyaPolifarmasiYa.setText("Ya");
        AdaTidaknyaPolifarmasiYa.setName("AdaTidaknyaPolifarmasiYa"); // NOI18N
        AdaTidaknyaPolifarmasiYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(AdaTidaknyaPolifarmasiYa);
        AdaTidaknyaPolifarmasiYa.setBounds(270, 270, 45, 23);

        buttonGroup7.add(AdaTidaknyaPolifarmasiTidak);
        AdaTidaknyaPolifarmasiTidak.setText("Tidak");
        AdaTidaknyaPolifarmasiTidak.setName("AdaTidaknyaPolifarmasiTidak"); // NOI18N
        FormInput.add(AdaTidaknyaPolifarmasiTidak);
        AdaTidaknyaPolifarmasiTidak.setBounds(310, 270, 60, 23);

        buttonGroup8.add(AdaTidaknyaDuplikasiYa);
        AdaTidaknyaDuplikasiYa.setText("Ya");
        AdaTidaknyaDuplikasiYa.setName("AdaTidaknyaDuplikasiYa"); // NOI18N
        AdaTidaknyaDuplikasiYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(AdaTidaknyaDuplikasiYa);
        AdaTidaknyaDuplikasiYa.setBounds(270, 290, 45, 23);

        buttonGroup8.add(AdaTidaknyaDuplikasiTidak);
        AdaTidaknyaDuplikasiTidak.setText("Tidak");
        AdaTidaknyaDuplikasiTidak.setName("AdaTidaknyaDuplikasiTidak"); // NOI18N
        FormInput.add(AdaTidaknyaDuplikasiTidak);
        AdaTidaknyaDuplikasiTidak.setBounds(310, 290, 60, 23);

        buttonGroup9.add(InteraksiObatYa);
        InteraksiObatYa.setText("Ya");
        InteraksiObatYa.setName("InteraksiObatYa"); // NOI18N
        InteraksiObatYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(InteraksiObatYa);
        InteraksiObatYa.setBounds(270, 310, 45, 23);

        buttonGroup9.add(InteraksiObatTidak);
        InteraksiObatTidak.setText("Tidak");
        InteraksiObatTidak.setName("InteraksiObatTidak"); // NOI18N
        FormInput.add(InteraksiObatTidak);
        InteraksiObatTidak.setBounds(310, 310, 60, 23);

        buttonGroup10.add(BenarIdentitasYa);
        BenarIdentitasYa.setText("Ya");
        BenarIdentitasYa.setName("BenarIdentitasYa"); // NOI18N
        BenarIdentitasYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarIdentitasYa);
        BenarIdentitasYa.setBounds(570, 250, 45, 23);

        buttonGroup10.add(BenarIdentitasTidak);
        BenarIdentitasTidak.setText("Tidak");
        BenarIdentitasTidak.setName("BenarIdentitasTidak"); // NOI18N
        FormInput.add(BenarIdentitasTidak);
        BenarIdentitasTidak.setBounds(610, 250, 60, 23);

        buttonGroup11.add(BenarObatYa);
        BenarObatYa.setText("Ya");
        BenarObatYa.setName("BenarObatYa"); // NOI18N
        BenarObatYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarObatYa);
        BenarObatYa.setBounds(570, 150, 45, 23);

        buttonGroup11.add(BenarObatTidak);
        BenarObatTidak.setText("Tidak");
        BenarObatTidak.setName("BenarObatTidak"); // NOI18N
        FormInput.add(BenarObatTidak);
        BenarObatTidak.setBounds(610, 150, 60, 23);

        buttonGroup12.add(BenarWaktuYa);
        BenarWaktuYa.setText("Ya");
        BenarWaktuYa.setName("BenarWaktuYa"); // NOI18N
        BenarWaktuYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarWaktuYa);
        BenarWaktuYa.setBounds(570, 170, 45, 23);

        buttonGroup12.add(BenarWaktuTidak);
        BenarWaktuTidak.setText("Tidak");
        BenarWaktuTidak.setName("BenarWaktuTidak"); // NOI18N
        BenarWaktuTidak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BenarWaktuTidakActionPerformed(evt);
            }
        });
        FormInput.add(BenarWaktuTidak);
        BenarWaktuTidak.setBounds(610, 170, 60, 23);

        buttonGroup13.add(BenarFrekuensiYa);
        BenarFrekuensiYa.setText("Ya");
        BenarFrekuensiYa.setName("BenarFrekuensiYa"); // NOI18N
        BenarFrekuensiYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarFrekuensiYa);
        BenarFrekuensiYa.setBounds(570, 190, 45, 23);

        buttonGroup13.add(BenarFrekuensiTidak);
        BenarFrekuensiTidak.setText("Tidak");
        BenarFrekuensiTidak.setName("BenarFrekuensiTidak"); // NOI18N
        FormInput.add(BenarFrekuensiTidak);
        BenarFrekuensiTidak.setBounds(610, 190, 60, 23);

        buttonGroup14.add(BenarDosis2Ya);
        BenarDosis2Ya.setText("Ya");
        BenarDosis2Ya.setName("BenarDosis2Ya"); // NOI18N
        BenarDosis2Ya.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarDosis2Ya);
        BenarDosis2Ya.setBounds(570, 210, 45, 23);

        buttonGroup14.add(BenarDosis2Tidak);
        BenarDosis2Tidak.setText("Tidak");
        BenarDosis2Tidak.setName("BenarDosis2Tidak"); // NOI18N
        FormInput.add(BenarDosis2Tidak);
        BenarDosis2Tidak.setBounds(610, 210, 60, 23);

        buttonGroup15.add(BenarRuteYa);
        BenarRuteYa.setText("Ya");
        BenarRuteYa.setName("BenarRuteYa"); // NOI18N
        BenarRuteYa.setPreferredSize(new java.awt.Dimension(40, 20));
        FormInput.add(BenarRuteYa);
        BenarRuteYa.setBounds(570, 230, 45, 23);

        buttonGroup15.add(BenarRuteTidak);
        BenarRuteTidak.setText("Tidak");
        BenarRuteTidak.setName("BenarRuteTidak"); // NOI18N
        FormInput.add(BenarRuteTidak);
        BenarRuteTidak.setBounds(610, 230, 60, 23);

        jLabel8.setText("Tgl. Resep :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(10, 100, 60, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 130, 780, 1);

        jSeparator2.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator2.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator2.setName("jSeparator2"); // NOI18N
        FormInput.add(jSeparator2);
        jSeparator2.setBounds(0, 150, 780, 1);

        jSeparator3.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator3.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator3.setName("jSeparator3"); // NOI18N
        FormInput.add(jSeparator3);
        jSeparator3.setBounds(0, 170, 300, 1);

        jSeparator4.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator4.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator4.setName("jSeparator4"); // NOI18N
        FormInput.add(jSeparator4);
        jSeparator4.setBounds(0, 230, 300, 1);

        jSeparator5.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator5.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator5.setName("jSeparator5"); // NOI18N
        FormInput.add(jSeparator5);
        jSeparator5.setBounds(0, 310, 300, 1);

        TglRw.setEditable(false);
        TglRw.setHighlighter(null);
        TglRw.setName("TglRw"); // NOI18N
        TglRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglRwKeyPressed(evt);
            }
        });
        FormInput.add(TglRw);
        TglRw.setBounds(80, 100, 141, 23);

        jLabel9.setText("Jam. Resep :");
        jLabel9.setName("jLabel9"); // NOI18N
        FormInput.add(jLabel9);
        jLabel9.setBounds(210, 100, 80, 23);

        JamRw.setEditable(false);
        JamRw.setHighlighter(null);
        JamRw.setName("JamRw"); // NOI18N
        JamRw.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JamRwActionPerformed(evt);
            }
        });
        JamRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamRwKeyPressed(evt);
            }
        });
        FormInput.add(JamRw);
        JamRw.setBounds(300, 100, 141, 23);

        jLabel11.setText("Status Resep:");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(450, 100, 80, 23);

        Status.setEditable(false);
        Status.setHighlighter(null);
        Status.setName("Status"); // NOI18N
        Status.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                StatusKeyPressed(evt);
            }
        });
        FormInput.add(Status);
        Status.setBounds(540, 100, 141, 23);

        scrollInput.setViewportView(FormInput);

        PanelInput.add(scrollInput, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            //Valid.pindah(evt,TCari,Kejadian);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Pasien");
        }else if(TNoResep.getText().trim().equals("")){
            Valid.textKosong(TNoResep,"No.Resep");
        }else if(TNoResep.getText().trim().equals("")){
            Valid.textKosong(TNoResep,"No.Resep");
        }else if(KodeDokter.getText().trim().equals("")){
            Valid.textKosong(KodeDokter,"Dokter");
        }else if(KdPetugas.getText().trim().equals("")){
            Valid.textKosong(KdPetugas,"Petugas");
        }else{
        KejelasanTulisan="Tidak";
        BenarNamaPasien="Tidak";
        BenarNamaObat="Tidak";
        BenarDosis="Tidak";
        BenarWaktudanFrekuensiPemberian="Tidak";
        BenarCaraPemberian="Tidak";
        AdaTidaknyaPolifarmasi="Tidak";
        AdaTidaknyaDuplikasi="Tidak";
        InteraksiObat="Tidak";
        BenarObat="Tidak";
        BenarWaktu="Tidak";
        BenarFrekuensi="Tidak";
        BenarDosis2="Tidak";
        BenarRute="Tidak";
        BenarIdentitas="Tidak";
            if(KejelasanTulisanYa.isSelected()==true){
               KejelasanTulisan="Ya";
            }
            if(BenarNamaPasienYa.isSelected()==true){
                BenarNamaPasien="Ya";
            }
            if(BenarNamaObatYa.isSelected()==true){
                BenarNamaObat="Ya";
            }
            if(BenarDosisYa.isSelected()==true){
                BenarDosis="Ya";
            }
            if(BenarWaktudanFrekuensiPemberianYa.isSelected()==true){
                BenarWaktudanFrekuensiPemberian="Ya";
            }
            if(BenarCaraPemberianYa.isSelected()==true){
                BenarCaraPemberian="Ya";
            }
            if(AdaTidaknyaPolifarmasiYa.isSelected()==true){
                AdaTidaknyaPolifarmasi="Ya";
            }
            if(AdaTidaknyaDuplikasiYa.isSelected()==true){
                AdaTidaknyaDuplikasi="Ya";
            }
            if(InteraksiObatYa.isSelected()==true){
                InteraksiObat="Ya";
            }
            if(BenarObatYa.isSelected()==true){
                BenarObat="Ya";
            }
            
            if(BenarWaktuYa.isSelected()==true){
                BenarWaktu="Ya";
            }
            
            if(BenarFrekuensiYa.isSelected()==true){
                BenarFrekuensi="Ya";
            }
            if(BenarDosis2Ya.isSelected()==true){
                BenarDosis2="Ya";
            }
            if(BenarRuteYa.isSelected()==true){
                BenarRute="Ya";
            }
            if(BenarIdentitasYa.isSelected()==true){
                BenarIdentitas="Ya";
            }
            
            if(Sequel.menyimpantf("telaah_farmasi","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Resep",24,new String[]{
               TNoResep.getText(),TglRw.getText(),JamRw.getText(),
               TNoRw.getText(),KodeDokter.getText(),KdPetugas.getText(),Status.getText(),KejelasanTulisan,BenarNamaPasien,BenarNamaObat,
               BenarDosis,BenarWaktudanFrekuensiPemberian,BenarCaraPemberian,AdaTidaknyaPolifarmasi,AdaTidaknyaDuplikasi,InteraksiObat,
               BenarObat,BenarWaktu,BenarFrekuensi,BenarDosis2,BenarRute,BenarIdentitas,tanggalNow.format(new Date()),jamNow.format(new Date())
                })==true){
                    tampil();
                    emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            //Valid.pindah(evt,Obat2an,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
        ChkInput.setSelected(true);
        isForm(); 
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(Sequel.queryu2tf("delete from telaah_farmasi where no_resep=? and tanggal=?",2,new String[]{
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),1).toString()
            })==true){
                tampil();
                emptTeks();
            }else{
                JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Pasien");
        }else if(TNoResep.getText().trim().equals("")){
            Valid.textKosong(TNoResep,"No.Resep");
        }else if(TNoResep.getText().trim().equals("")){
            Valid.textKosong(TNoResep,"No.Resep");
        }else if(KodeDokter.getText().trim().equals("")){
            Valid.textKosong(KodeDokter,"Dokter");
        }else if(KdPetugas.getText().trim().equals("")){
            Valid.textKosong(KdPetugas,"Petugas");
        }else{
            if(tbObat.getSelectedRow()>-1){
            KejelasanTulisan="Tidak";
            BenarNamaPasien="Tidak";
            BenarNamaObat="Tidak";
            BenarDosis="Tidak";
            BenarWaktudanFrekuensiPemberian="Tidak";
            BenarCaraPemberian="Tidak";
            AdaTidaknyaPolifarmasi="Tidak";
            AdaTidaknyaDuplikasi="Tidak";
            InteraksiObat="Tidak";
            BenarObat="Tidak";
            BenarWaktu="Tidak";
            BenarFrekuensi="Tidak";
            BenarDosis2="Tidak";
            BenarRute="Tidak";
            BenarIdentitas="Tidak";
            if(KejelasanTulisanYa.isSelected()==true){
               KejelasanTulisan="Ya";
            }
            if(BenarNamaPasienYa.isSelected()==true){
                BenarNamaPasien="Ya";
            }
            if(BenarNamaObatYa.isSelected()==true){
                BenarNamaObat="Ya";
            }
            if(BenarDosisYa.isSelected()==true){
                BenarDosis="Ya";
            }
            if(BenarWaktudanFrekuensiPemberianYa.isSelected()==true){
                BenarWaktudanFrekuensiPemberian="Ya";
            }
            if(BenarCaraPemberianYa.isSelected()==true){
                BenarCaraPemberian="Ya";
            }
            if(AdaTidaknyaPolifarmasiYa.isSelected()==true){
                AdaTidaknyaPolifarmasi="Ya";
            }
            if(AdaTidaknyaDuplikasiYa.isSelected()==true){
                AdaTidaknyaDuplikasi="Ya";
            }
            if(InteraksiObatYa.isSelected()==true){
                InteraksiObat="Ya";
            }
            if(BenarObatYa.isSelected()==true){
                BenarObat="Ya";
            }
            
            if(BenarWaktuYa.isSelected()==true){
                BenarWaktu="Ya";
            }
            
            if(BenarFrekuensiYa.isSelected()==true){
                BenarFrekuensi="Ya";
            }
            if(BenarDosis2Ya.isSelected()==true){
                BenarDosis2="Ya";
            }
            if(BenarRuteYa.isSelected()==true){
                BenarRute="Ya";
            }
            if(BenarIdentitasYa.isSelected()==true){
                BenarIdentitas="Ya";
            }
                
                if(Sequel.mengedittf("telaah_farmasi","no_resep=? and tanggal=?","no_resep=?,tanggal=?,jam=?,no_rawat=?,kd_dokter=?,nip=?,status=?,kejelasantulisan=?,namapasien=?,namaobat=?,benardosis=?,waktupemberian=?,carapemberian=?,polifarmasi=?,duplikasi=?,interaksi=?,benarobat2=?,benarwaktu=?,benarfrekuensi=?,benardosis2=?,benarrute=?,benaridentitas=?,tgl=?,jam_telaah=?",26,new String[]{
                TNoResep.getText(),TglRw.getText(),JamRw.getText(),
                TNoRw.getText(),KodeDokter.getText(),KdPetugas.getText(),Status.getText(),KejelasanTulisan,BenarNamaPasien,BenarNamaObat,
                BenarDosis,BenarWaktudanFrekuensiPemberian,BenarCaraPemberian,AdaTidaknyaPolifarmasi,AdaTidaknyaDuplikasi,InteraksiObat,
                BenarObat,BenarWaktu,BenarFrekuensi,BenarDosis2,BenarRute,BenarIdentitas,tanggalNow.format(new Date()),jamNow.format(new Date()),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),1).toString()
                    })==true){
                        tampil();
                        emptTeks();
                }
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            Map<String, Object> param = new HashMap<>(); 
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());   
                param.put("logo",Sequel.cariGambar("select logo from setting")); 
                if(TCari.getText().equals("")){
                    Valid.MyReportqry("rptDataAsuhanGiziPasien.jasper","report","::[ Data Asuhan Gizi Pasien ]::",
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.tgl_lahir,asuhan_gizi.tanggal,"+
                        "asuhan_gizi.antropometri_bb,asuhan_gizi.antropometri_tb,asuhan_gizi.antropometri_imt,asuhan_gizi.antropometri_lla,"+
                        "asuhan_gizi.antropometri_tl,asuhan_gizi.antropometri_ulna,asuhan_gizi.antropometri_bbideal,asuhan_gizi.antropometri_bbperu,"+
                        "asuhan_gizi.antropometri_tbperu,asuhan_gizi.antropometri_bbpertb,asuhan_gizi.antropometri_llaperu,asuhan_gizi.biokimia,"+
                        "asuhan_gizi.fisik_klinis,asuhan_gizi.alergi_telur,asuhan_gizi.alergi_susu_sapi,asuhan_gizi.alergi_kacang,asuhan_gizi.alergi_gluten,"+
                        "asuhan_gizi.alergi_udang,asuhan_gizi.alergi_ikan,asuhan_gizi.alergi_hazelnut,asuhan_gizi.pola_makan,asuhan_gizi.riwayat_personal,"+
                        "asuhan_gizi.diagnosis,asuhan_gizi.intervensi_gizi,asuhan_gizi.monitoring_evaluasi,asuhan_gizi.nip,petugas.nama "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join asuhan_gizi on reg_periksa.no_rawat=asuhan_gizi.no_rawat "+
                        "inner join petugas on asuhan_gizi.nip=petugas.nip where "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' order by asuhan_gizi.tanggal",param);
                }else{
                    Valid.MyReportqry("rptDataAsuhanGiziPasien.jasper","report","::[ Data Asuhan Gizi Pasien ]::",
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.tgl_lahir,asuhan_gizi.tanggal,"+
                        "asuhan_gizi.antropometri_bb,asuhan_gizi.antropometri_tb,asuhan_gizi.antropometri_imt,asuhan_gizi.antropometri_lla,"+
                        "asuhan_gizi.antropometri_tl,asuhan_gizi.antropometri_ulna,asuhan_gizi.antropometri_bbideal,asuhan_gizi.antropometri_bbperu,"+
                        "asuhan_gizi.antropometri_tbperu,asuhan_gizi.antropometri_bbpertb,asuhan_gizi.antropometri_llaperu,asuhan_gizi.biokimia,"+
                        "asuhan_gizi.fisik_klinis,asuhan_gizi.alergi_telur,asuhan_gizi.alergi_susu_sapi,asuhan_gizi.alergi_kacang,asuhan_gizi.alergi_gluten,"+
                        "asuhan_gizi.alergi_udang,asuhan_gizi.alergi_ikan,asuhan_gizi.alergi_hazelnut,asuhan_gizi.pola_makan,asuhan_gizi.riwayat_personal,"+
                        "asuhan_gizi.diagnosis,asuhan_gizi.intervensi_gizi,asuhan_gizi.monitoring_evaluasi,asuhan_gizi.nip,petugas.nama "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join asuhan_gizi on reg_periksa.no_rawat=asuhan_gizi.no_rawat "+
                        "inner join petugas on asuhan_gizi.nip=petugas.nip where "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' and reg_periksa.no_rawat like '%"+TCari.getText().trim()+"%' or "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' and pasien.no_rkm_medis like '%"+TCari.getText().trim()+"%' or "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' and pasien.nm_pasien like '%"+TCari.getText().trim()+"%' or "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' and asuhan_gizi.nip like '%"+TCari.getText().trim()+"%' or "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' and asuhan_gizi.diagnosis like '%"+TCari.getText().trim()+"%' or "+
                        "asuhan_gizi.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' and petugas.nama like '%"+TCari.getText().trim()+"%' order by asuhan_gizi.tanggal",param);
                }   
        }
        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    ChkInput.setSelected(true);
                    isForm(); 
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void KdPetugasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdPetugasKeyPressed
        
    }//GEN-LAST:event_KdPetugasKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        petugas.isCek();
        petugas.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        petugas.setLocationRelativeTo(internalFrame1);
        petugas.setAlwaysOnTop(false);
        petugas.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        Valid.pindah(evt,KdPetugas,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void TNoResepKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoResepKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TNoResepKeyPressed

    private void KodeDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KodeDokterKeyPressed
        Valid.pindah(evt,TCari,KdPetugas);
    }//GEN-LAST:event_KodeDokterKeyPressed

    private void BenarWaktuTidakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BenarWaktuTidakActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BenarWaktuTidakActionPerformed

    private void TglRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglRwKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglRwKeyPressed

    private void JamRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamRwKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamRwKeyPressed

    private void JamRwActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JamRwActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JamRwActionPerformed

    private void StatusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_StatusKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_StatusKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            InventoryTelaahResepPulang dialog = new InventoryTelaahResepPulang(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.RadioButton AdaTidaknyaDuplikasiTidak;
    private widget.RadioButton AdaTidaknyaDuplikasiYa;
    private widget.RadioButton AdaTidaknyaPolifarmasiTidak;
    private widget.RadioButton AdaTidaknyaPolifarmasiYa;
    private widget.RadioButton BenarCaraPemberianTidak;
    private widget.RadioButton BenarCaraPemberianYa;
    private widget.RadioButton BenarDosis2Tidak;
    private widget.RadioButton BenarDosis2Ya;
    private widget.RadioButton BenarDosisTidak;
    private widget.RadioButton BenarDosisYa;
    private widget.RadioButton BenarFrekuensiTidak;
    private widget.RadioButton BenarFrekuensiYa;
    private widget.RadioButton BenarIdentitasTidak;
    private widget.RadioButton BenarIdentitasYa;
    private widget.RadioButton BenarNamaObatTidak;
    private widget.RadioButton BenarNamaObatYa;
    private widget.RadioButton BenarNamaPasienTidak;
    private widget.RadioButton BenarNamaPasienYa;
    private widget.RadioButton BenarObatTidak;
    private widget.RadioButton BenarObatYa;
    private widget.RadioButton BenarRuteTidak;
    private widget.RadioButton BenarRuteYa;
    private widget.RadioButton BenarWaktuTidak;
    private widget.RadioButton BenarWaktuYa;
    private widget.RadioButton BenarWaktudanFrekuensiPemberianTidak;
    private widget.RadioButton BenarWaktudanFrekuensiPemberianYa;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkInput;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.RadioButton InteraksiObatTidak;
    private widget.RadioButton InteraksiObatYa;
    private widget.TextBox JamRw;
    private widget.TextBox KdPetugas;
    private widget.RadioButton KejelasanTulisanTidak;
    private widget.RadioButton KejelasanTulisanYa;
    private widget.TextBox KodeDokter;
    private widget.Label LCount;
    private widget.TextBox NamaDokter;
    private widget.TextBox NmPetugas;
    private javax.swing.JPanel PanelInput;
    private widget.ScrollPane Scroll;
    private widget.TextBox Status;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoResep;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.TextBox TglRw;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup10;
    private javax.swing.ButtonGroup buttonGroup11;
    private javax.swing.ButtonGroup buttonGroup12;
    private javax.swing.ButtonGroup buttonGroup13;
    private javax.swing.ButtonGroup buttonGroup14;
    private javax.swing.ButtonGroup buttonGroup15;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.ButtonGroup buttonGroup8;
    private javax.swing.ButtonGroup buttonGroup9;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel5;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private widget.Label label14;
    private widget.Label label15;
    private widget.Label label16;
    private widget.Label label17;
    private widget.Label label19;
    private widget.Label label21;
    private widget.Label label22;
    private widget.Label label23;
    private widget.Label label25;
    private widget.Label label26;
    private widget.Label label27;
    private widget.Label label28;
    private widget.Label label29;
    private widget.Label label30;
    private widget.Label label31;
    private widget.Label label32;
    private widget.Label label33;
    private widget.Label label34;
    private widget.Label label35;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
            Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,"+
                        "telaah_farmasi.no_resep,telaah_farmasi.tanggal,telaah_farmasi.jam,telaah_farmasi.no_rawat,"+
                        "telaah_farmasi.kd_dokter,telaah_farmasi.nip,telaah_farmasi.status,telaah_farmasi.kejelasantulisan,telaah_farmasi.namapasien,telaah_farmasi.namaobat,telaah_farmasi.benardosis,telaah_farmasi.waktupemberian, telaah_farmasi.carapemberian,telaah_farmasi.polifarmasi,telaah_farmasi.duplikasi,telaah_farmasi.interaksi, " +
                        "telaah_farmasi.benarobat2, telaah_farmasi.benarwaktu, telaah_farmasi.benarfrekuensi,telaah_farmasi.benardosis2, telaah_farmasi.benarrute, telaah_farmasi.benaridentitas, telaah_farmasi.tgl,telaah_farmasi.jam_telaah,dokter.nm_dokter,petugas.nama "+
                        "from resep_pulang inner join telaah_farmasi on resep_pulang.no_rawat=telaah_farmasi.no_rawat "+
                        "inner join dpjp_ranap on dpjp_ranap.no_rawat=telaah_farmasi.no_rawat inner join dokter on dokter.kd_dokter=dpjp_ranap.kd_dokter "+
                        "inner join petugas on telaah_farmasi.nip=petugas.nip "+
                        "inner join reg_periksa on resep_pulang.no_rawat=reg_periksa.no_rawat "+
                        "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis where "+
                        "telaah_farmasi.tanggal between ? and ? order by telaah_farmasi.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,"+
                        "telaah_farmasi.no_resep,telaah_farmasi.tanggal,telaah_farmasi.jam,telaah_farmasi.no_rawat,"+
                        "telaah_farmasi.kd_dokter,telaah_farmasi.nip,telaah_farmasi.status,telaah_farmasi.kejelasantulisan,telaah_farmasi.namapasien,telaah_farmasi.namaobat,telaah_farmasi.benardosis,telaah_farmasi.waktupemberian, telaah_farmasi.carapemberian,telaah_farmasi.polifarmasi,telaah_farmasi.duplikasi,telaah_farmasi.interaksi, " +
                        "telaah_farmasi.benarobat2, telaah_farmasi.benarwaktu, telaah_farmasi.benarfrekuensi,telaah_farmasi.benardosis2, telaah_farmasi.benarrute, telaah_farmasi.benaridentitas, telaah_farmasi.tgl,telaah_farmasi.jam_telaah,dokter.nm_dokter,petugas.nama "+
                        "from resep_pulang inner join telaah_farmasi on resep_pulang.no_rawat=telaah_farmasi.no_rawat "+
                        "inner join dpjp_ranap on dpjp_ranap.no_rawat=telaah_farmasi.no_rawat inner join dokter on dokter.kd_dokter=dpjp_ranap.kd_dokter "+
                        "inner join petugas on telaah_farmasi.nip=petugas.nip "+
                        "inner join reg_periksa on resep_pulang.no_rawat=reg_periksa.no_rawat "+
                        "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis where "+
                        "telaah_farmasi.tanggal between ? and ? and telaah_farmasi.no_resep like ? or "+
                        "telaah_farmasi.tanggal between ? and ? and resep_pulang.no_rawat like ? or "+
                        "telaah_farmasi.tanggal between ? and ? and telaah_farmasi.jam like ? or "+
                        "telaah_farmasi.tanggal between ? and ? and telaah_farmasi.nip like ? or "+
                        "telaah_farmasi.tanggal between ? and ? and telaah_farmasi.kd_dokter like ? or "+
                        "telaah_farmasi.tanggal between ? and ? and petugas.nama like ? order by telaah_farmasi.tanggal");

            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(12,"%"+TCari.getText()+"%");
                    ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(15,"%"+TCari.getText()+"%");
                    ps.setString(16,Valid.SetTgl(DTPCari1.getSelectedItem()+""));
                    ps.setString(17,Valid.SetTgl(DTPCari2.getSelectedItem()+""));
                    ps.setString(18,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_resep"),rs.getString("tanggal"),rs.getString("jam"),rs.getString("no_rawat"),rs.getString("no_rkm_medis"),
                        rs.getString("nm_pasien"),rs.getString("kd_dokter"),rs.getString("nm_dokter"),
                        rs.getString("nip"),rs.getString("nama"),rs.getString("status"),rs.getString("kejelasantulisan"),rs.getString("namapasien"),
                        rs.getString("namaobat"),rs.getString("benardosis"),rs.getString("waktupemberian"),rs.getString("carapemberian"),
                        rs.getString("polifarmasi"),rs.getString("duplikasi"),rs.getString("interaksi"),rs.getString("benarobat2"),
                        rs.getString("benarwaktu"),rs.getString("benarfrekuensi"),rs.getString("benardosis2"),rs.getString("benarrute"),
                        rs.getString("benaridentitas"),rs.getString("tgl"),rs.getString("jam_telaah")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        int b=tabMode.getRowCount();
        LCount.setText(""+b);
    }

    public void emptTeks() {
        KejelasanTulisanYa.setSelected(true);
        BenarNamaPasienYa.setSelected(true);
        BenarNamaObatYa.setSelected(true);
        BenarDosisYa.setSelected(true);
        BenarWaktudanFrekuensiPemberianYa.setSelected(true);
        BenarCaraPemberianYa.setSelected(true);
        AdaTidaknyaPolifarmasiTidak.setSelected(true);
        AdaTidaknyaDuplikasiTidak.setSelected(true);
        InteraksiObatTidak.setSelected(true);
        BenarIdentitasYa.setSelected(true);
        BenarObatYa.setSelected(true);
        BenarWaktuYa.setSelected(true);
        BenarFrekuensiYa.setSelected(true);
        BenarDosis2Ya.setSelected(true);
        BenarRuteYa.setSelected(true);
        TNoResep.requestFocus();
        Status.setText("resep pulang");
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoResep.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TglRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            JamRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            KodeDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            NamaDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()); 
            KdPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            NmPetugas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()); 
            Status.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());          
            if(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString().equals("Ya")){
                KejelasanTulisanYa.setSelected(true);
            }else{
                KejelasanTulisanTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString().equals("Ya")){
                BenarNamaPasienYa.setSelected(true);
            }else{
                BenarNamaPasienTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString().equals("Ya")){
                BenarNamaObatYa.setSelected(true);
            }else{
                BenarNamaObatTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString().equals("Ya")){
                BenarDosisYa.setSelected(true);
            }else{
                BenarDosisTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString().equals("Ya")){
                BenarWaktudanFrekuensiPemberianYa.setSelected(true);
            }else{
                BenarWaktudanFrekuensiPemberianTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString().equals("Ya")){
                BenarCaraPemberianYa.setSelected(true);
            }else{
                BenarCaraPemberianTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString().equals("Ya")){
                AdaTidaknyaPolifarmasiYa.setSelected(true);
            }else{
                AdaTidaknyaPolifarmasiTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString().equals("Ya")){
                AdaTidaknyaDuplikasiYa.setSelected(true);
            }else{
                AdaTidaknyaDuplikasiTidak.setSelected(true);
            } 
            if(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString().equals("Ya")){
                InteraksiObatYa.setSelected(true);
            }else{
                InteraksiObatTidak.setSelected(true);
            } 
            if(tbObat.getValueAt(tbObat.getSelectedRow(),25).toString().equals("Ya")){
                BenarIdentitasYa.setSelected(true);
            }else{
                BenarIdentitasTidak.setSelected(true);
            } 
            if(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString().equals("Ya")){
                BenarObatYa.setSelected(true);
            }else{
                BenarObatTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString().equals("Ya")){
                BenarWaktuYa.setSelected(true);
            }else{
                BenarWaktuTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString().equals("Ya")){
                BenarFrekuensiYa.setSelected(true);
            }else{
                BenarFrekuensiTidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString().equals("Ya")){
                BenarDosis2Ya.setSelected(true);
            }else{
                BenarDosis2Tidak.setSelected(true);
            }
            if(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString().equals("Ya")){
                BenarRuteYa.setSelected(true);
            }else{
                BenarRuteTidak.setSelected(true);
            }
        }
    }

    private void isRawat() {
         Sequel.cariIsi("select no_rawat from resep_pulang where no_rawat='"+TNoRw.getText()+"' ",TNoRw);
         Sequel.cariIsi("select kd_dokter from dpjp_ranap where no_rawat='"+TNoRw.getText()+"' ",KodeDokter);
         Sequel.cariIsi("select tanggal from resep_pulang where no_rawat='"+TNoRw.getText()+"' order by tanggal desc",TglRw);
         Sequel.cariIsi("select jam from resep_pulang where no_rawat='"+TNoRw.getText()+"' order by jam desc",JamRw);
    //   Sequel.cariIsi("select status from permintaan_resep_pulang where no_permintaa=? ",Status,TNoResep.getText());
    }

    private void isPsien() {
        Sequel.cariIsi("select no_rkm_medis from reg_periksa where no_rawat=? ",TNoRM,TNoRw.getText());
        Sequel.cariIsi("select nm_pasien from pasien where no_rkm_medis=? ",TPasien,TNoRM.getText());
        Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=? ",NamaDokter,KodeDokter.getText());
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TNoResep.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);  
        Status.setText("resep pulang");
        isRawat();
        isPsien();              
        ChkInput.setSelected(true);
        isForm();
        }
    
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            ChkInput.setVisible(false);
            PanelInput.setPreferredSize(new Dimension(WIDTH,400));
            scrollInput.setVisible(true);      
            ChkInput.setVisible(true);
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            scrollInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getresep_obat());
        BtnHapus.setEnabled(akses.getresep_obat());
        BtnEdit.setEnabled(akses.getresep_obat());
        BtnPrint.setEnabled(akses.getresep_obat());   
        if(akses.getjml2()>=1){
            KdPetugas.setEditable(false);
            BtnDokter.setEnabled(false);
            KdPetugas.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", NmPetugas,KdPetugas.getText());
            if(NmPetugas.getText().equals("")){
                KdPetugas.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    
}
