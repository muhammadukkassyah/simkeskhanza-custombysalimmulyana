/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * DlgRujuk.java
 *
 * Created on 31 Mei 10, 20:19:56
 */

package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import laporan.DlgCariPenyakit;


/**
 *
 * @author perpustakaan
 */
public final class RMKunjunganHamil extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;    
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPenyakit penyakit=new DlgCariPenyakit(null,false);
    private String usg="";
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMKunjunganHamil(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);

        tabMode=new DefaultTableModel(null,new Object[]{
        "No.Rawat","No.R.M.","Nama Pasien","Tanggal Lahir","Umur","HPHT","Usia Kehamilan Mnrt HPHT","Indikasi Pemeriksaan","Keluhan","Kantong Gestasi",
        "Lokasi Kantong Gestasi","Ukuran","Embrio","CRL","Fetal Pulse","Frekuensi Fetal Pulse","Jumlah Janin", "Usia Kehamilan",
        "HPL","TBJ","Diagnosa","Rencana","Tanggal Periksa","Kode Dokter","Nama Dokter"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 25; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(100);
            }else if(i==4){
                column.setPreferredWidth(100);
            }else if(i==5){
                column.setPreferredWidth(120);
            }else if(i==6){
                column.setPreferredWidth(120);
            }else if(i==7){
                column.setPreferredWidth(150);
            }else if(i==8){
                column.setPreferredWidth(110);
            }else if(i==9){
                column.setPreferredWidth(100);
            }else if(i==10){
                column.setPreferredWidth(150);
            }else if(i==11){
                column.setPreferredWidth(100);
            }else if(i==12){
                column.setPreferredWidth(70);
            }else if(i==13){
                column.setPreferredWidth(70);
            }else if(i==14){
                column.setPreferredWidth(70);
            }else if(i==15){
                column.setPreferredWidth(150);
            }else if(i==16){
                column.setPreferredWidth(70);
            }else if(i==17){
                column.setPreferredWidth(150);
            }else if(i==18){
                column.setPreferredWidth(70);
            }else if(i==19){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==20){
                column.setPreferredWidth(150);
            }else if(i==21){
                column.setMinWidth(0);
                column.setMaxWidth(0);
            }else if(i==22){
                column.setPreferredWidth(150);
            }else if(i==50){
                column.setPreferredWidth(45);
            }else if(i==23){
                column.setPreferredWidth(180);
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());

        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        Indikasi.setDocument(new batasInput((byte)100).getKata(Indikasi));
        Keluhan.setDocument(new batasInput((byte)100).getKata(Keluhan));
        Ukuran.setDocument(new batasInput((byte)5).getOnlyAngka(Ukuran));
        UsiaKehamilanHpht.setDocument(new batasInput((byte)5).getOnlyAngka(UsiaKehamilanHpht));
        Crl.setDocument(new batasInput((byte)3).getKata(Crl));
        FrekuensiFetal.setDocument(new batasInput((byte)3).getOnlyAngka(FrekuensiFetal));
        JumlahJanin.setDocument(new batasInput((byte)3).getOnlyAngka(JumlahJanin));
        UsiaKehamilan.setDocument(new batasInput((byte)2).getKata(UsiaKehamilan));
        Tbj.setDocument(new batasInput((byte)4).getOnlyAngka(Tbj));
        Diagnosa.setDocument(new batasInput((byte)100).getKata(Diagnosa));
        Rencana.setDocument(new batasInput((byte)100).getKata(Rencana));
        kddok.setDocument(new batasInput((byte)20).getKata(kddok));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){                   
                    kddok.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    namadokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                }  
                kddok.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        }); 
        
                
        ChkInput.setSelected(false);
        isForm();
        
        kddok.setText(Sequel.cariIsi("select kd_dokterhemodialisa from set_pjlab"));
        namadokter.setText(Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?",kddok.getText()));
        
        jam();
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakUsg = new javax.swing.JMenuItem();
        MnCetakUsg1 = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        BtnAll = new widget.Button();
        PanelInput = new javax.swing.JPanel();
        FormInput = new widget.PanelBiasa();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        Tanggal = new widget.Tanggal();
        TNoRM = new widget.TextBox();
        jLabel16 = new widget.Label();
        Jam = new widget.ComboBox();
        Menit = new widget.ComboBox();
        Detik = new widget.ComboBox();
        ChkKejadian = new widget.CekBox();
        jLabel18 = new widget.Label();
        kddok = new widget.TextBox();
        namadokter = new widget.TextBox();
        btnDokter = new widget.Button();
        jLabel17 = new widget.Label();
        TglLahir = new widget.Tanggal();
        jLabel5 = new widget.Label();
        Umur = new widget.TextBox();
        Hpht = new widget.Tanggal();
        jLabel20 = new widget.Label();
        jLabel9 = new widget.Label();
        Diagnosa = new widget.TextBox();
        jLabel10 = new widget.Label();
        Indikasi = new widget.TextBox();
        jLabel11 = new widget.Label();
        Keluhan = new widget.TextBox();
        cmbGestasi = new widget.ComboBox();
        jLabel13 = new widget.Label();
        jLabel14 = new widget.Label();
        JumlahJanin = new widget.TextBox();
        jLabel15 = new widget.Label();
        cmbEmbrio = new widget.ComboBox();
        Crl = new widget.TextBox();
        jLabel22 = new widget.Label();
        cmbFetal = new widget.ComboBox();
        jLabel23 = new widget.Label();
        jLabel24 = new widget.Label();
        Ukuran = new widget.TextBox();
        jLabel25 = new widget.Label();
        UsiaKehamilan = new widget.TextBox();
        jLabel26 = new widget.Label();
        Hpl = new widget.Tanggal();
        jLabel27 = new widget.Label();
        Tbj = new widget.TextBox();
        jLabel28 = new widget.Label();
        jLabel29 = new widget.Label();
        jLabel31 = new widget.Label();
        Rencana = new widget.TextBox();
        jLabel30 = new widget.Label();
        FrekuensiFetal = new widget.TextBox();
        cmbLokasi = new widget.ComboBox();
        jLabel32 = new widget.Label();
        jLabel8 = new widget.Label();
        UsiaKehamilanHpht = new widget.TextBox();
        jLabel12 = new widget.Label();
        jLabel33 = new widget.Label();
        UsiaKehamilanHpht1 = new widget.TextBox();
        jLabel34 = new widget.Label();
        UsiaKehamilanHpht2 = new widget.TextBox();
        jLabel35 = new widget.Label();
        jLabel36 = new widget.Label();
        UsiaKehamilanHpht3 = new widget.TextBox();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        UsiaKehamilanHpht4 = new widget.TextBox();
        jLabel39 = new widget.Label();
        jLabel40 = new widget.Label();
        jLabel41 = new widget.Label();
        UsiaKehamilanHpht5 = new widget.TextBox();
        jLabel42 = new widget.Label();
        ChkInput = new widget.CekBox();

        jPopupMenu1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakUsg.setBackground(new java.awt.Color(250, 250, 250));
        MnCetakUsg.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakUsg.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakUsg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakUsg.setText("Cetak Hasil dan Gambar USG");
        MnCetakUsg.setName("MnCetakUsg"); // NOI18N
        MnCetakUsg.setPreferredSize(new java.awt.Dimension(200, 26));
        MnCetakUsg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakUsgActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakUsg);

        MnCetakUsg1.setBackground(new java.awt.Color(250, 250, 250));
        MnCetakUsg1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakUsg1.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakUsg1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakUsg1.setText("Cetak Hasil USG");
        MnCetakUsg1.setName("MnCetakUsg1"); // NOI18N
        MnCetakUsg1.setPreferredSize(new java.awt.Dimension(200, 26));
        MnCetakUsg1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakUsg1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakUsg1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Kunjungan Hamil ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass8.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass8.add(LCount);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tanggal :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2022" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2022" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(95, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(310, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnAll);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 245));
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        FormInput.setBackground(new java.awt.Color(250, 255, 245));
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 225));
        FormInput.setLayout(null);

        jLabel4.setText("No.Rawat :");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(0, 10, 75, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(79, 10, 141, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        TPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TPasienKeyPressed(evt);
            }
        });
        FormInput.add(TPasien);
        TPasien.setBounds(336, 10, 480, 23);

        Tanggal.setForeground(new java.awt.Color(50, 70, 50));
        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2022" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalKeyPressed(evt);
            }
        });
        FormInput.add(Tanggal);
        Tanggal.setBounds(110, 330, 90, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        TNoRM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRMKeyPressed(evt);
            }
        });
        FormInput.add(TNoRM);
        TNoRM.setBounds(222, 10, 112, 23);

        jLabel16.setText("Tanggal Periksa :");
        jLabel16.setName("jLabel16"); // NOI18N
        jLabel16.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel16);
        jLabel16.setBounds(10, 330, 90, 23);

        Jam.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
        Jam.setName("Jam"); // NOI18N
        Jam.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JamKeyPressed(evt);
            }
        });
        FormInput.add(Jam);
        Jam.setBounds(210, 330, 62, 23);

        Menit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Menit.setName("Menit"); // NOI18N
        Menit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MenitKeyPressed(evt);
            }
        });
        FormInput.add(Menit);
        Menit.setBounds(270, 330, 62, 23);

        Detik.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        Detik.setName("Detik"); // NOI18N
        Detik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DetikKeyPressed(evt);
            }
        });
        FormInput.add(Detik);
        Detik.setBounds(340, 330, 62, 23);

        ChkKejadian.setBorder(null);
        ChkKejadian.setSelected(true);
        ChkKejadian.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ChkKejadian.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkKejadian.setName("ChkKejadian"); // NOI18N
        FormInput.add(ChkKejadian);
        ChkKejadian.setBounds(400, 330, 23, 23);

        jLabel18.setText("Dokter P.J. :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(430, 330, 70, 23);

        kddok.setEditable(false);
        kddok.setHighlighter(null);
        kddok.setName("kddok"); // NOI18N
        kddok.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kddokKeyPressed(evt);
            }
        });
        FormInput.add(kddok);
        kddok.setBounds(510, 330, 94, 23);

        namadokter.setEditable(false);
        namadokter.setName("namadokter"); // NOI18N
        FormInput.add(namadokter);
        namadokter.setBounds(610, 330, 185, 23);

        btnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnDokter.setMnemonic('2');
        btnDokter.setToolTipText("ALt+2");
        btnDokter.setName("btnDokter"); // NOI18N
        btnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDokterActionPerformed(evt);
            }
        });
        btnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDokterKeyPressed(evt);
            }
        });
        FormInput.add(btnDokter);
        btnDokter.setBounds(800, 330, 28, 23);

        jLabel17.setText("Tanggal Lahir :");
        jLabel17.setName("jLabel17"); // NOI18N
        jLabel17.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel17);
        jLabel17.setBounds(0, 40, 73, 23);

        TglLahir.setForeground(new java.awt.Color(50, 70, 50));
        TglLahir.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2022" }));
        TglLahir.setDisplayFormat("dd-MM-yyyy");
        TglLahir.setName("TglLahir"); // NOI18N
        TglLahir.setOpaque(false);
        TglLahir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglLahirKeyPressed(evt);
            }
        });
        FormInput.add(TglLahir);
        TglLahir.setBounds(80, 40, 90, 23);

        jLabel5.setText("Umur:");
        jLabel5.setName("jLabel5"); // NOI18N
        FormInput.add(jLabel5);
        jLabel5.setBounds(180, 40, 30, 23);

        Umur.setHighlighter(null);
        Umur.setName("Umur"); // NOI18N
        Umur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UmurKeyPressed(evt);
            }
        });
        FormInput.add(Umur);
        Umur.setBounds(220, 40, 141, 23);

        Hpht.setForeground(new java.awt.Color(50, 70, 50));
        Hpht.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2022" }));
        Hpht.setDisplayFormat("dd-MM-yyyy");
        Hpht.setName("Hpht"); // NOI18N
        Hpht.setOpaque(false);
        Hpht.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HphtKeyPressed(evt);
            }
        });
        FormInput.add(Hpht);
        Hpht.setBounds(410, 40, 90, 23);

        jLabel20.setText("HPHT:");
        jLabel20.setName("jLabel20"); // NOI18N
        jLabel20.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel20);
        jLabel20.setBounds(370, 40, 30, 23);

        jLabel9.setText("Diagnosa:");
        jLabel9.setName("jLabel9"); // NOI18N
        FormInput.add(jLabel9);
        jLabel9.setBounds(10, 300, 60, 23);

        Diagnosa.setHighlighter(null);
        Diagnosa.setName("Diagnosa"); // NOI18N
        Diagnosa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                DiagnosaKeyPressed(evt);
            }
        });
        FormInput.add(Diagnosa);
        Diagnosa.setBounds(80, 300, 310, 23);

        jLabel10.setText("Kantong Gestasi:");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(10, 200, 90, 23);

        Indikasi.setHighlighter(null);
        Indikasi.setName("Indikasi"); // NOI18N
        Indikasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IndikasiKeyPressed(evt);
            }
        });
        FormInput.add(Indikasi);
        Indikasi.setBounds(120, 170, 240, 23);

        jLabel11.setText("minggu");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(780, 40, 40, 23);

        Keluhan.setHighlighter(null);
        Keluhan.setName("Keluhan"); // NOI18N
        Keluhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeluhanKeyPressed(evt);
            }
        });
        FormInput.add(Keluhan);
        Keluhan.setBounds(420, 170, 410, 23);

        cmbGestasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ada", "Tidak ada" }));
        cmbGestasi.setName("cmbGestasi"); // NOI18N
        FormInput.add(cmbGestasi);
        cmbGestasi.setBounds(110, 200, 80, 20);

        jLabel13.setText("Lokasi:");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(200, 200, 40, 23);

        jLabel14.setText("Jumlah Janin:");
        jLabel14.setName("jLabel14"); // NOI18N
        FormInput.add(jLabel14);
        jLabel14.setBounds(680, 230, 80, 23);

        JumlahJanin.setHighlighter(null);
        JumlahJanin.setName("JumlahJanin"); // NOI18N
        JumlahJanin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                JumlahJaninKeyPressed(evt);
            }
        });
        FormInput.add(JumlahJanin);
        JumlahJanin.setBounds(770, 230, 70, 23);

        jLabel15.setText("Embrio:");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(10, 230, 90, 23);

        cmbEmbrio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ada", "Tidak ada" }));
        cmbEmbrio.setName("cmbEmbrio"); // NOI18N
        FormInput.add(cmbEmbrio);
        cmbEmbrio.setBounds(110, 230, 80, 20);

        Crl.setHighlighter(null);
        Crl.setName("Crl"); // NOI18N
        Crl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                CrlKeyPressed(evt);
            }
        });
        FormInput.add(Crl);
        Crl.setBounds(240, 230, 60, 23);

        jLabel22.setText("CRL:");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(200, 230, 30, 23);

        cmbFetal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ada", "Tidak ada" }));
        cmbFetal.setName("cmbFetal"); // NOI18N
        FormInput.add(cmbFetal);
        cmbFetal.setBounds(400, 230, 90, 20);

        jLabel23.setText("DJJ:");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(330, 230, 70, 23);

        jLabel24.setText("Ukuran:");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(420, 200, 50, 23);

        Ukuran.setHighlighter(null);
        Ukuran.setName("Ukuran"); // NOI18N
        Ukuran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UkuranKeyPressed(evt);
            }
        });
        FormInput.add(Ukuran);
        Ukuran.setBounds(480, 200, 70, 23);

        jLabel25.setText("Usia Kehamilan Menurut USG:");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(10, 270, 150, 23);

        UsiaKehamilan.setHighlighter(null);
        UsiaKehamilan.setName("UsiaKehamilan"); // NOI18N
        UsiaKehamilan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanKeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilan);
        UsiaKehamilan.setBounds(170, 270, 60, 23);

        jLabel26.setText("minggu");
        jLabel26.setName("jLabel26"); // NOI18N
        jLabel26.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel26);
        jLabel26.setBounds(230, 270, 40, 23);

        Hpl.setForeground(new java.awt.Color(50, 70, 50));
        Hpl.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "02-10-2022" }));
        Hpl.setDisplayFormat("dd-MM-yyyy");
        Hpl.setName("Hpl"); // NOI18N
        Hpl.setOpaque(false);
        Hpl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                HplKeyPressed(evt);
            }
        });
        FormInput.add(Hpl);
        Hpl.setBounds(350, 270, 90, 23);

        jLabel27.setText("gram");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(590, 270, 30, 23);

        Tbj.setHighlighter(null);
        Tbj.setName("Tbj"); // NOI18N
        Tbj.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TbjKeyPressed(evt);
            }
        });
        FormInput.add(Tbj);
        Tbj.setBounds(520, 270, 70, 23);

        jLabel28.setText("TBJ:");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(450, 270, 70, 23);

        jLabel29.setText("Indikasi Pemeriksaan:");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(10, 170, 107, 23);

        jLabel31.setText("Rencana:");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(460, 300, 60, 23);

        Rencana.setHighlighter(null);
        Rencana.setName("Rencana"); // NOI18N
        Rencana.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RencanaKeyPressed(evt);
            }
        });
        FormInput.add(Rencana);
        Rencana.setBounds(530, 300, 300, 23);

        jLabel30.setText("Frekuensi DJJ:");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(500, 230, 110, 23);

        FrekuensiFetal.setHighlighter(null);
        FrekuensiFetal.setName("FrekuensiFetal"); // NOI18N
        FrekuensiFetal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                FrekuensiFetalKeyPressed(evt);
            }
        });
        FormInput.add(FrekuensiFetal);
        FrekuensiFetal.setBounds(610, 230, 70, 23);

        cmbLokasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Intrauterine", "Ekstrauterine", "-" }));
        cmbLokasi.setName("cmbLokasi"); // NOI18N
        FormInput.add(cmbLokasi);
        cmbLokasi.setBounds(250, 200, 160, 20);

        jLabel32.setText("HPL:");
        jLabel32.setName("jLabel32"); // NOI18N
        jLabel32.setVerifyInputWhenFocusTarget(false);
        FormInput.add(jLabel32);
        jLabel32.setBounds(310, 270, 30, 23);

        jLabel8.setText("Usia Kehamilan Berdasarkan HPHT:");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(510, 40, 190, 23);

        UsiaKehamilanHpht.setHighlighter(null);
        UsiaKehamilanHpht.setName("UsiaKehamilanHpht"); // NOI18N
        UsiaKehamilanHpht.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanHphtKeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilanHpht);
        UsiaKehamilanHpht.setBounds(710, 40, 70, 23);

        jLabel12.setText("Keluhan:");
        jLabel12.setName("jLabel12"); // NOI18N
        FormInput.add(jLabel12);
        jLabel12.setBounds(370, 170, 50, 23);

        jLabel33.setText("Hamil ke:");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(0, 70, 60, 23);

        UsiaKehamilanHpht1.setHighlighter(null);
        UsiaKehamilanHpht1.setName("UsiaKehamilanHpht1"); // NOI18N
        UsiaKehamilanHpht1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanHpht1KeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilanHpht1);
        UsiaKehamilanHpht1.setBounds(70, 70, 70, 23);

        jLabel34.setText("kg");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(250, 70, 20, 23);

        UsiaKehamilanHpht2.setHighlighter(null);
        UsiaKehamilanHpht2.setName("UsiaKehamilanHpht2"); // NOI18N
        UsiaKehamilanHpht2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanHpht2KeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilanHpht2);
        UsiaKehamilanHpht2.setBounds(180, 70, 70, 23);

        jLabel35.setText("BB:");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(150, 70, 20, 23);

        jLabel36.setText("cm");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(390, 70, 20, 23);

        UsiaKehamilanHpht3.setHighlighter(null);
        UsiaKehamilanHpht3.setName("UsiaKehamilanHpht3"); // NOI18N
        UsiaKehamilanHpht3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanHpht3KeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilanHpht3);
        UsiaKehamilanHpht3.setBounds(320, 70, 70, 23);

        jLabel37.setText("TB:");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(290, 70, 20, 23);

        jLabel38.setText("IMT:");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(430, 70, 40, 23);

        UsiaKehamilanHpht4.setHighlighter(null);
        UsiaKehamilanHpht4.setName("UsiaKehamilanHpht4"); // NOI18N
        UsiaKehamilanHpht4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanHpht4KeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilanHpht4);
        UsiaKehamilanHpht4.setBounds(480, 70, 70, 23);

        jLabel39.setText("cm");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(550, 70, 20, 23);

        jLabel40.setText("Faktor Resiko Kehamilah:");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(0, 100, 130, 23);

        jLabel41.setText("LILA:");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(580, 70, 40, 23);

        UsiaKehamilanHpht5.setHighlighter(null);
        UsiaKehamilanHpht5.setName("UsiaKehamilanHpht5"); // NOI18N
        UsiaKehamilanHpht5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                UsiaKehamilanHpht5KeyPressed(evt);
            }
        });
        FormInput.add(UsiaKehamilanHpht5);
        UsiaKehamilanHpht5.setBounds(630, 70, 70, 23);

        jLabel42.setText("cm");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(700, 70, 20, 23);

        PanelInput.add(FormInput, java.awt.BorderLayout.CENTER);

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            Valid.pindah(evt,TCari,Tanggal);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void TPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TPasienKeyPressed
        Valid.pindah(evt,TCari,BtnSimpan);
}//GEN-LAST:event_TPasienKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(kddok.getText().trim().equals("")||namadokter.getText().trim().equals("")){
            Valid.textKosong(kddok,"Dokter P.J");
        }else if(Indikasi.getText().trim().equals("")){
            Valid.textKosong(Indikasi,"Indikasi");
        }else if(Keluhan.getText().trim().equals("")){
            Valid.textKosong(Keluhan,"Keluhan");
        }else if(UsiaKehamilanHpht.getText().trim().equals("")){
            Valid.textKosong(UsiaKehamilanHpht,"Usia Kehamilan Menurut HPHT");
        }else if(Crl.getText().trim().equals("")){
            Valid.textKosong(Crl,"CRL");
        }else if(FrekuensiFetal.getText().trim().equals("")){
            Valid.textKosong(FrekuensiFetal,"Frekuensi Fetal Pulse");
        }else if(JumlahJanin.getText().trim().equals("")){
            Valid.textKosong(JumlahJanin,"Jumlah Janin");
        }else if(UsiaKehamilan.getText().trim().equals("")){
            Valid.textKosong(UsiaKehamilan,"Usia Kehamilan");
        }else if(Tbj.getText().trim().equals("")){
            Valid.textKosong(Tbj,"Taksiran Berat Janin");
        }else if(Diagnosa.getText().trim().equals("")){
            Valid.textKosong(Diagnosa,"Diagnosa");
        }else if(Rencana.getText().trim().equals("")){
            Valid.textKosong(Rencana,"Rencana");
        
        }else{
            if(Sequel.menyimpantf("usg_kehamilan1","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","Data",22,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglLahir.getSelectedItem()+""),Umur.getText(),Valid.SetTgl(Hpht.getSelectedItem()+""),UsiaKehamilanHpht.getText(),
                Indikasi.getText(),Keluhan.getText(),cmbGestasi.getSelectedItem().toString(),cmbLokasi.getSelectedItem().toString(),
                Ukuran.getText(),cmbEmbrio.getSelectedItem().toString(),Crl.getText(),cmbFetal.getSelectedItem().toString(),
                FrekuensiFetal.getText(),JumlahJanin.getText(),UsiaKehamilan.getText(),Valid.SetTgl(Hpl.getSelectedItem()+""),
                Tbj.getText(),Diagnosa.getText(),
                Rencana.getText(),Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                kddok.getText()
            })==true){
                tampil();
                emptTeks();
            }   
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,BtnAll,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
        ChkInput.setSelected(true);
        isForm(); 
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()!= -1){
            if(Sequel.queryu2tf("delete from usg_kehamilan1 where tanggal=? and no_rawat=?",2,new String[]{
                tbObat.getValueAt(tbObat.getSelectedRow(),22).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
            })==true){
                tampil();
                emptTeks();
            }else{
                JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
            }
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(kddok.getText().trim().equals("")||namadokter.getText().trim().equals("")){
            Valid.textKosong(kddok,"Dokter P.J");
        }else if(Indikasi.getText().trim().equals("")){
            Valid.textKosong(Indikasi,"Indikasi");
        }else if(Keluhan.getText().trim().equals("")){
            Valid.textKosong(Keluhan,"Keluhan");
        }else if(UsiaKehamilanHpht.getText().trim().equals("")){
            Valid.textKosong(UsiaKehamilanHpht,"Ukuran");
        }else if(Crl.getText().trim().equals("")){
            Valid.textKosong(Crl,"CRL");
        }else if(FrekuensiFetal.getText().trim().equals("")){
            Valid.textKosong(FrekuensiFetal,"Frekuensi Fetal Pulse");
        }else if(JumlahJanin.getText().trim().equals("")){
            Valid.textKosong(JumlahJanin,"Jumlah Janin");
        }else if(UsiaKehamilan.getText().trim().equals("")){
            Valid.textKosong(UsiaKehamilan,"Usia Kehamilan");
        }else if(Tbj.getText().trim().equals("")){
            Valid.textKosong(Tbj,"Taksiran Berat Janin");
        }else if(Diagnosa.getText().trim().equals("")){
            Valid.textKosong(Diagnosa,"Diagnosa");
        }else if(Rencana.getText().trim().equals("")){
            Valid.textKosong(Rencana,"Rencana");
        }else{
            Sequel.mengedit("usg_kehamilan1","tanggal=? and no_rawat=?","no_rawat=?,tgl_lahir=?,umur=?,hpht=?,usiakehamilanhpht=?,indikasi=?,keluhan=?,"+
                "kantonggestasi=?,lokasi=?,ukuran=?,embrio=?,crl=?,fetalpulse=?,frekpulse=?,jumlahjanin=?,umurkehamilan=?,hpl=?,tbj=?,diagnosa=?,rencana=?,tanggal=?,kd_dokter=?",24,new String[]{
                TNoRw.getText(),Valid.SetTgl(TglLahir.getSelectedItem()+""),Umur.getText(),Valid.SetTgl(Hpht.getSelectedItem()+""),UsiaKehamilanHpht.getText(),
                Indikasi.getText(),Keluhan.getText(),cmbGestasi.getSelectedItem().toString(),cmbLokasi.getSelectedItem().toString(),
                Ukuran.getText(),cmbEmbrio.getSelectedItem().toString(),Crl.getText(),cmbFetal.getSelectedItem().toString(),
                FrekuensiFetal.getText(),JumlahJanin.getText(),UsiaKehamilan.getText(),Valid.SetTgl(Hpl.getSelectedItem()+""),
                Tbj.getText(),Diagnosa.getText(),Rencana.getText(),Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Jam.getSelectedItem()+":"+Menit.getSelectedItem()+":"+Detik.getSelectedItem(),
                kddok.getText(),tbObat.getValueAt(tbObat.getSelectedRow(),22).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
            });
            if(tabMode.getRowCount()!=0){tampil();}
            emptTeks();
        }
       
             
           
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dokter.dispose();
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
    /*  this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            Map<String, Object> param = new HashMap<>(); 
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());   
            param.put("logo",Sequel.cariGambar("select logo from setting")); 
            if(TCari.getText().equals("")){ 
                Valid.MyReportqry("rptDataHemodialisa.jasper","report","::[ Data Hemodialis ]::",
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,"+
                    "pasien.jk,usg_kehamilan1.tanggal,usg_kehamilan1.lama,usg_kehamilan1.akses,usg_kehamilan1.dialist,usg_kehamilan1.transfusi,usg_kehamilan1.penarikan, "+
                    "usg_kehamilan1.qb,usg_kehamilan1.qd,usg_kehamilan1.ureum,usg_kehamilan1.hb,usg_kehamilan1.hbsag,creatinin,usg_kehamilan1.hiv,usg_kehamilan1.hcv,usg_kehamilan1.lain, "+
                    "usg_kehamilan1.kd_dokter,dokter.nm_dokter, "+
                    "from usg_kehamilan1 inner join reg_periksa on usg_kehamilan1.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter on usg_kehamilan1.kd_dokter=dokter.kd_dokter "+
                    " where "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' order by usg_kehamilan1.tanggal ",param);
            }else{
                Valid.MyReportqry("rptDataHemodialisa.jasper","report","::[ Data Hemodialis ]::",
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,"+
                    "pasien.jk,usg_kehamilan1.tanggal,usg_kehamilan1.lama,usg_kehamilan1.akses,usg_kehamilan1.dialist,usg_kehamilan1.transfusi,usg_kehamilan1.penarikan, "+
                    "usg_kehamilan1.qb,usg_kehamilan1.qd,usg_kehamilan1.ureum,usg_kehamilan1.hb,usg_kehamilan1.hbsag,creatinin,usg_kehamilan1.hiv,usg_kehamilan1.hcv,usg_kehamilan1.lain, "+
                    "usg_kehamilan1.kd_dokter,dokter.nm_dokter "+
                    "from usg_kehamilan1 inner join reg_periksa on usg_kehamilan1.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter on usg_kehamilan1.kd_dokter=dokter.kd_dokter "+
                    " where "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and reg_periksa.no_rawat like '%"+TCari.getText().trim()+"%' or "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and pasien.no_rkm_medis like '%"+TCari.getText().trim()+"%' or "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and pasien.nm_pasien like '%"+TCari.getText().trim()+"%' or "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and usg_kehamilan1.akses like '%"+TCari.getText().trim()+"%' or "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and usg_kehamilan1.dialist like '%"+TCari.getText().trim()+"%' or "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and usg_kehamilan1.lain like '%"+TCari.getText().trim()+"%' or "+
                    "usg_kehamilan1.tanggal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59' and dokter.nm_dokter like '%"+TCari.getText().trim()+"%' "+
                    "order by usg_kehamilan1.tanggal ",param);
            }  
        }
        this.setCursor(Cursor.getDefaultCursor()); */
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void TanggalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalKeyPressed
        Valid.pindah(evt,TCari,Jam);
}//GEN-LAST:event_TanggalKeyPressed

    private void TNoRMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRMKeyPressed
        // Valid.pindah(evt, TNm, BtnSimpan);
}//GEN-LAST:event_TNoRMKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
        isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void JamKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JamKeyPressed
        Valid.pindah(evt,Tanggal,Menit);
    }//GEN-LAST:event_JamKeyPressed

    private void MenitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MenitKeyPressed
        Valid.pindah(evt,Jam,Detik);
    }//GEN-LAST:event_MenitKeyPressed

    private void DetikKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DetikKeyPressed
        Valid.pindah(evt,Menit,btnDokter);
    }//GEN-LAST:event_DetikKeyPressed

    private void kddokKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kddokKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?",namadokter,kddok.getText());
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            Detik.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnAll.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            btnDokterActionPerformed(null);
        }
    }//GEN-LAST:event_kddokKeyPressed

    private void btnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDokterActionPerformed
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_btnDokterActionPerformed

    private void btnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDokterKeyPressed
        Valid.pindah(evt,Detik,BtnAll);
    }//GEN-LAST:event_btnDokterKeyPressed

    private void TglLahirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglLahirKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TglLahirKeyPressed

    private void UmurKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UmurKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UmurKeyPressed

    private void HphtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HphtKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HphtKeyPressed

    private void DiagnosaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DiagnosaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiagnosaKeyPressed

    private void IndikasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IndikasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_IndikasiKeyPressed

    private void KeluhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeluhanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeluhanKeyPressed

    private void JumlahJaninKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_JumlahJaninKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_JumlahJaninKeyPressed

    private void CrlKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CrlKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_CrlKeyPressed

    private void UkuranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UkuranKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UkuranKeyPressed

    private void UsiaKehamilanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanKeyPressed

    private void HplKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_HplKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_HplKeyPressed

    private void TbjKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TbjKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_TbjKeyPressed

    private void RencanaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RencanaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RencanaKeyPressed

    private void FrekuensiFetalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FrekuensiFetalKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_FrekuensiFetalKeyPressed

    private void UsiaKehamilanHphtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanHphtKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanHphtKeyPressed

    private void MnCetakUsgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakUsgActionPerformed
        usg = "http://"+koneksiDB.HOSTHYBRIDWEB()+":"+koneksiDB.PORTWEB()+"/"+koneksiDB.HYBRIDWEB()+"/radiologi/"+Sequel.cariIsi("select lokasi_gambar from gambar_radiologi where no_rawat='"+TNoRw.getText()+"'")+"";
        if(TPasien.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan anda pilih dulu pasien...!!!");
        }else{
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo from setting"));
            param.put("photousg",usg);
        Valid.MyReportqry("rptUsgTrimester1.jasper","report","::[ Data Pemeriksaan USG Trimester 1 ]::",
            "select DATE_FORMAT(usg_kehamilan1.hpht,'%d-%m-%Y')as hpht,usg_kehamilan1.usiakehamilanhpht,usg_kehamilan1.indikasi,usg_kehamilan1.keluhan,"+
            "usg_kehamilan1.kantonggestasi,usg_kehamilan1.lokasi,usg_kehamilan1.ukuran,usg_kehamilan1.embrio,usg_kehamilan1.crl,"+
            "usg_kehamilan1.fetalpulse,usg_kehamilan1.frekpulse,usg_kehamilan1.jumlahjanin,usg_kehamilan1.umurkehamilan,usg_kehamilan1.hpl,usg_kehamilan1.tbj,"+
            "usg_kehamilan1.diagnosa,usg_kehamilan1.rencana,usg_kehamilan1.tanggal,usg_kehamilan1.kd_dokter,DATE_FORMAT(reg_periksa.tgl_registrasi,'%d-%m-%Y')as tgl_registrasi,reg_periksa.jam_reg,poliklinik.nm_poli,dokter.nm_dokter,pasien.tgl_lahir,"+
            "pasien.no_rkm_medis,pasien.no_ktp,(pasien.jk)as jkpasien,pasien.tmp_lahir,pasien.tgl_lahir,reg_periksa.kd_pj,pasien.nm_pasien,concat(reg_periksa.umurdaftar,' ',reg_periksa.sttsumur)as umur,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat "+ 
            "from usg_kehamilan1 inner join reg_periksa inner join pasien inner join dokter inner join kelurahan inner join kecamatan inner join kabupaten inner join poliklinik "+
            "on reg_periksa.no_rawat=usg_kehamilan1.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis and usg_kehamilan1.kd_dokter=dokter.kd_dokter and pasien.kd_kel=kelurahan.kd_kel "+
            "and reg_periksa.kd_poli=poliklinik.kd_poli where usg_kehamilan1.no_rawat='"+TNoRw.getText()+"' ",param);
        this.setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_MnCetakUsgActionPerformed

    private void MnCetakUsg1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakUsg1ActionPerformed
        if(TPasien.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan anda pilih dulu pasien...!!!");
        }else{
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo from setting"));
        Valid.MyReportqry("rptUsgTrimester1NoPhoto.jasper","report","::[ Data Pemeriksaan USG Trimester 1 ]::",
            "select DATE_FORMAT(usg_kehamilan1.hpht,'%d-%m-%Y')as hpht,usg_kehamilan1.usiakehamilanhpht,usg_kehamilan1.indikasi,usg_kehamilan1.keluhan,"+
            "usg_kehamilan1.kantonggestasi,usg_kehamilan1.lokasi,usg_kehamilan1.ukuran,usg_kehamilan1.embrio,usg_kehamilan1.crl,"+
            "usg_kehamilan1.fetalpulse,usg_kehamilan1.frekpulse,usg_kehamilan1.jumlahjanin,usg_kehamilan1.umurkehamilan,usg_kehamilan1.hpl,usg_kehamilan1.tbj,"+
            "usg_kehamilan1.diagnosa,usg_kehamilan1.rencana,usg_kehamilan1.tanggal,usg_kehamilan1.kd_dokter,DATE_FORMAT(reg_periksa.tgl_registrasi,'%d-%m-%Y')as tgl_registrasi,reg_periksa.jam_reg,poliklinik.nm_poli,dokter.nm_dokter,pasien.tgl_lahir,"+
            "pasien.no_rkm_medis,pasien.no_ktp,(pasien.jk)as jkpasien,pasien.tmp_lahir,pasien.tgl_lahir,reg_periksa.kd_pj,pasien.nm_pasien,concat(reg_periksa.umurdaftar,' ',reg_periksa.sttsumur)as umur,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat "+ 
            "from usg_kehamilan1 inner join reg_periksa inner join pasien inner join dokter inner join kelurahan inner join kecamatan inner join kabupaten inner join poliklinik "+
            "on reg_periksa.no_rawat=usg_kehamilan1.no_rawat and reg_periksa.no_rkm_medis=pasien.no_rkm_medis and usg_kehamilan1.kd_dokter=dokter.kd_dokter and pasien.kd_kel=kelurahan.kd_kel "+
            "and reg_periksa.kd_poli=poliklinik.kd_poli where usg_kehamilan1.no_rawat='"+TNoRw.getText()+"' ",param);
        this.setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_MnCetakUsg1ActionPerformed

    private void UsiaKehamilanHpht1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanHpht1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanHpht1KeyPressed

    private void UsiaKehamilanHpht2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanHpht2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanHpht2KeyPressed

    private void UsiaKehamilanHpht3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanHpht3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanHpht3KeyPressed

    private void UsiaKehamilanHpht4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanHpht4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanHpht4KeyPressed

    private void UsiaKehamilanHpht5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_UsiaKehamilanHpht5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsiaKehamilanHpht5KeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMKunjunganHamil dialog = new RMKunjunganHamil(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkInput;
    private widget.CekBox ChkKejadian;
    private widget.TextBox Crl;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox Detik;
    private widget.TextBox Diagnosa;
    private widget.PanelBiasa FormInput;
    private widget.TextBox FrekuensiFetal;
    private widget.Tanggal Hpht;
    private widget.Tanggal Hpl;
    private widget.TextBox Indikasi;
    private widget.ComboBox Jam;
    private widget.TextBox JumlahJanin;
    private widget.TextBox Keluhan;
    private widget.Label LCount;
    private widget.ComboBox Menit;
    private javax.swing.JMenuItem MnCetakUsg;
    private javax.swing.JMenuItem MnCetakUsg1;
    private javax.swing.JPanel PanelInput;
    private widget.TextBox Rencana;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.Tanggal Tanggal;
    private widget.TextBox Tbj;
    private widget.Tanggal TglLahir;
    private widget.TextBox Ukuran;
    private widget.TextBox Umur;
    private widget.TextBox UsiaKehamilan;
    private widget.TextBox UsiaKehamilanHpht;
    private widget.TextBox UsiaKehamilanHpht1;
    private widget.TextBox UsiaKehamilanHpht2;
    private widget.TextBox UsiaKehamilanHpht3;
    private widget.TextBox UsiaKehamilanHpht4;
    private widget.TextBox UsiaKehamilanHpht5;
    private widget.Button btnDokter;
    private widget.ComboBox cmbEmbrio;
    private widget.ComboBox cmbFetal;
    private widget.ComboBox cmbGestasi;
    private widget.ComboBox cmbLokasi;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel10;
    private widget.Label jLabel11;
    private widget.Label jLabel12;
    private widget.Label jLabel13;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel4;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel5;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private widget.TextBox kddok;
    private widget.TextBox namadokter;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().toString().trim().equals("")){
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,"+
                    "pasien.jk,  usg_kehamilan1.no_rawat,usg_kehamilan1.tgl_lahir,usg_kehamilan1.umur,usg_kehamilan1.hpht,usg_kehamilan1.indikasi, "+
                    "usg_kehamilan1.usiakehamilanhpht,usg_kehamilan1.keluhan,usg_kehamilan1.kantonggestasi,usg_kehamilan1.lokasi,usg_kehamilan1.ukuran,usg_kehamilan1.embrio, " +
                    "usg_kehamilan1.crl,usg_kehamilan1.fetalpulse,usg_kehamilan1.frekpulse,usg_kehamilan1.jumlahjanin,usg_kehamilan1.umurkehamilan, " +
                    "usg_kehamilan1.hpl,usg_kehamilan1.tbj,usg_kehamilan1.diagnosa,usg_kehamilan1.rencana,usg_kehamilan1.tanggal,usg_kehamilan1.kd_dokter,dokter.nm_dokter "+
                    "from usg_kehamilan1 inner join reg_periksa on usg_kehamilan1.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter on usg_kehamilan1.kd_dokter=dokter.kd_dokter "+
                    " where "+
                    "usg_kehamilan1.tanggal between ? and ? order by usg_kehamilan1.tanggal ");
            }else{
                ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,reg_periksa.umurdaftar,reg_periksa.sttsumur,"+
                    "pasien.jk,  usg_kehamilan1.no_rawat,usg_kehamilan1.tgl_lahir,usg_kehamilan1.umur,usg_kehamilan1.hpht,usg_kehamilan1.indikasi, "+
                    "usg_kehamilan1.usiakehamilanhpht,usg_kehamilan1.keluhan,usg_kehamilan1.kantonggestasi,usg_kehamilan1.lokasi,usg_kehamilan1.ukuran,usg_kehamilan1.embrio, " +
                    "usg_kehamilan1.crl,usg_kehamilan1.fetalpulse,usg_kehamilan1.frekpulse,usg_kehamilan1.jumlahjanin,usg_kehamilan1.umurkehamilan, " +
                    "usg_kehamilan1.hpl,usg_kehamilan1.tbj,usg_kehamilan1.diagnosa,usg_kehamilan1.rencana,usg_kehamilan1.tanggal,usg_kehamilan1.kd_dokter,dokter.nm_dokter "+
                    "from usg_kehamilan1 inner join reg_periksa on usg_kehamilan1.no_rawat=reg_periksa.no_rawat "+
                    "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                    "inner join dokter on usg_kehamilan1.kd_dokter=dokter.kd_dokter "+
                    " where "+
                    "usg_kehamilan1.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
                    "usg_kehamilan1.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
                    "usg_kehamilan1.tanggal between ? and ? and pasien.nm_pasien like ? or "+
                    "usg_kehamilan1.tanggal between ? and ? and dokter.nm_dokter like ? "+
                    "order by usg_kehamilan1.tanggal ");
            }
                
            try {
                if(TCari.getText().toString().trim().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(12,"%"+TCari.getText()+"%");
               }
                    
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),
                        rs.getString("tgl_lahir"),rs.getString("umur"),
                        rs.getString("hpht"),rs.getString("usiakehamilanhpht"),rs.getString("indikasi"),rs.getString("keluhan"),
                        rs.getString("kantonggestasi"),rs.getString("lokasi"),rs.getString("ukuran"),rs.getString("embrio"),rs.getString("crl"),
                        rs.getString("fetalpulse"),rs.getString("frekpulse"),rs.getString("jumlahjanin"),rs.getString("umurkehamilan"),
                        rs.getString("hpl"),rs.getString("tbj"),rs.getString("diagnosa"),rs.getString("rencana"),
                        rs.getString("tanggal"),rs.getString("kd_dokter"),rs.getString("nm_dokter")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(SQLException e){
            System.out.println("Notifikasi : "+e);
        }
        int b=tabMode.getRowCount();
        LCount.setText(""+b);
    }

    public void emptTeks() {
        TNoRw.setText("");
        TNoRM.setText("");
        TPasien.setText("");
        Umur.setText("");
        Indikasi.setText("");
        Keluhan.setText("");
        Crl.setText("");
        Ukuran.setText("");
        FrekuensiFetal.setText("");
        JumlahJanin.setText("");
        UsiaKehamilan.setText("");
        UsiaKehamilanHpht.setText("");
        Tbj.setText("");
        Diagnosa.setText("");
        Rencana.setText("");
        kddok.setText("");
        namadokter.setText("");
        Tanggal.setDate(new Date());
        Tanggal.requestFocus();
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            Valid.SetTgl(TglLahir,tbObat.getValueAt(tbObat.getSelectedRow(),3).toString()); 
            Umur.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            Valid.SetTgl(Hpht,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString()); 
            UsiaKehamilanHpht.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString());
            Indikasi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString());
            Keluhan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            cmbGestasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            cmbLokasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            Ukuran.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString());
            cmbEmbrio.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString());
            Crl.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString());
            cmbFetal.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString());
            FrekuensiFetal.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString());
            JumlahJanin.setText(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString());
            UsiaKehamilan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString());
            Valid.SetTgl(Hpl,tbObat.getValueAt(tbObat.getSelectedRow(),18).toString()); 
            Tbj.setText(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());
            Diagnosa.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());
            Rencana.setText(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());
            Valid.SetTgl(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());  
            Jam.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString().substring(11,13));
            Menit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString().substring(14,15));
            Detik.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString().substring(17,19));
            kddok.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            namadokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString());
            
        }
    }

    private void isRawat() {
         Sequel.cariIsi("select no_rkm_medis from reg_periksa where no_rawat='"+TNoRw.getText()+"' ",TNoRM);
    }

    private void isPsien() {
        Sequel.cariIsi("select nm_pasien from pasien where no_rkm_medis='"+TNoRM.getText()+"' ",TPasien);
    }
    
    private void isUmur() {
        Sequel.cariIsi("select umur from pasien where no_rkm_medis='"+TNoRM.getText()+"' ",Umur);
    
    }
    
    public void setNoRm(String norwt) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        isRawat();
        isPsien();    
        isUmur();
        ChkInput.setSelected(true);
        isForm();
    }
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            ChkInput.setVisible(false);
            PanelInput.setPreferredSize(new Dimension(WIDTH,300));
            FormInput.setVisible(true);      
            ChkInput.setVisible(true);
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.gettindakan_ralan());
        BtnHapus.setEnabled(akses.gettindakan_ralan());
        BtnEdit.setEnabled(akses.gettindakan_ralan());
        BtnPrint.setEnabled(akses.gettindakan_ralan()); 
    }

    private void jam(){
        ActionListener taskPerformer = new ActionListener(){
            private int nilai_jam;
            private int nilai_menit;
            private int nilai_detik;
            public void actionPerformed(ActionEvent e) {
                String nol_jam = "";
                String nol_menit = "";
                String nol_detik = "";
                
                Date now = Calendar.getInstance().getTime();

                // Mengambil nilaj JAM, MENIT, dan DETIK Sekarang
                if(ChkKejadian.isSelected()==true){
                    nilai_jam = now.getHours();
                    nilai_menit = now.getMinutes();
                    nilai_detik = now.getSeconds();
                }else if(ChkKejadian.isSelected()==false){
                    nilai_jam =Jam.getSelectedIndex();
                    nilai_menit =Menit.getSelectedIndex();
                    nilai_detik =Detik.getSelectedIndex();
                }

                // Jika nilai JAM lebih kecil dari 10 (hanya 1 digit)
                if (nilai_jam <= 9) {
                    // Tambahkan "0" didepannya
                    nol_jam = "0";
                }
                // Jika nilai MENIT lebih kecil dari 10 (hanya 1 digit)
                if (nilai_menit <= 9) {
                    // Tambahkan "0" didepannya
                    nol_menit = "0";
                }
                // Jika nilai DETIK lebih kecil dari 10 (hanya 1 digit)
                if (nilai_detik <= 9) {
                    // Tambahkan "0" didepannya
                    nol_detik = "0";
                }
                // Membuat String JAM, MENIT, DETIK
                String jam = nol_jam + Integer.toString(nilai_jam);
                String menit = nol_menit + Integer.toString(nilai_menit);
                String detik = nol_detik + Integer.toString(nilai_detik);
                // Menampilkan pada Layar
                //tampil_jam.setText("  " + jam + " : " + menit + " : " + detik + "  ");
                Jam.setSelectedItem(jam);
                Menit.setSelectedItem(menit);
                Detik.setSelectedItem(detik);
            }
        };
        // Timer
        new Timer(1000, taskPerformer).start();
    }
    
}
