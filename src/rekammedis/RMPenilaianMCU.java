package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariPetugas;


/**
 *
 * @author perpustakaan
 */
public final class RMPenilaianMCU extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private RMCariHasilRadiologi cariradiologi=new RMCariHasilRadiologi(null,false);
    private RMCariHasilLaborat carilaborat=new RMCariHasilLaborat(null,false);
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps5;
    private ResultSet rs;
    private int i=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private DlgCariPetugas petugas=new DlgCariPetugas(null,false);
    private String finger=""; 
    private StringBuilder htmlContent;
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMPenilaianMCU(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tanggal","Tanggal Lahir", "Jenis Kelamin","Riwayat Penyakit Darah Tinggi",
            "Riwayat Penyakit Kencing Manis","Riwayat Penyakit Jantung","Riwayat Penyakit Asma/Paru-paru","Riwayat Penyakit Hati",
            "Benjolan Pada tubuh/Tumor","Riwayat Penyakit Maag","Riwayat Penyakit Tertentu","Merokok","Olahraga","Haid","Hamil",
            "HPHT","KB","Pakai Kacamata/lensa","Ukuran Kacamata","Keadaan Umum","Kesadaran","Tensi","Nadi","Respirasi",
            "Tinggi Badan","Berat Badan","IMT","Suhu","Submandibula","Axilla","Supraklavikula","Leher","Inguinal","Oedema",
            "Nyeri tekan sinus frontalis","Nyeri tekan sinus maxillaris","Palpebra","Sklera","Buta warna","Konjungtiva",
            "Mata Kanan","Mata kiri","Cornea","Pupil","Lubang telinga","Daun Telinga","Selaput Pendengaran","Proc Mastoideus",
            "Septum Nasi","Lubang Hidung","Bibir","Caries","Lidah","Faring","Tonsil","Kelenjar limfe","Kelenjar Gondok",
            "Gerakan dada","Vocal Fremitus","Perkusi","Bunyi Nafas","Bunyi Tambahan","Ictus Cordis","Bunyi Jantung",
            "Batas jantung","Inspeksi Abdomen","Palpasi","Perkusi","Auskultasi","Hepar","Limpa","Nyeri ketok CVA",
            "Ekstremitas Atas","Ket Ekstremitas Atas","Eksterimitas Bawah","Ket Ekstremitas Bawah","Kulit",
            "Pemeriksan Lab","Rontgen","EKG","Audiometri","Kesimpulan",
            "Kode Dokter","Nama Dokter"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 87; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(60);
            }else if(i==5){
                column.setPreferredWidth(120);
            }else if(i==6){
                column.setPreferredWidth(85);
            }else if(i==7){
                column.setPreferredWidth(85);
            }else if(i==8){
                column.setPreferredWidth(85);
            }else if(i==9){
                column.setPreferredWidth(85);
            }else if(i==10){
                column.setPreferredWidth(50);
            }else if(i==11){
                column.setPreferredWidth(30);
            }else if(i==12){
                column.setPreferredWidth(30);
            }else if(i==13){
                column.setPreferredWidth(35);
            }else if(i==14){
                column.setPreferredWidth(70);
            }else if(i==15){
                column.setPreferredWidth(70);
            }else if(i==16){
                column.setPreferredWidth(70);
            }else if(i==17){
                column.setPreferredWidth(70);
            }else if(i==18){
                column.setPreferredWidth(70);
            }else if(i==19){
                column.setPreferredWidth(70);
            }else if(i==20){
                column.setPreferredWidth(70);
            }else if(i==21){
                column.setPreferredWidth(70);
            }else if(i==22){
                column.setPreferredWidth(70);
            }else if(i==23){
                column.setPreferredWidth(70);
            }else if(i==24){
                column.setPreferredWidth(60);
            }else if(i==25){
                column.setPreferredWidth(70);
            }else if(i==26){
                column.setPreferredWidth(70);
            }else if(i==27){
                column.setPreferredWidth(70);
            }else if(i==28){
                column.setPreferredWidth(62);
            }else if(i==29){
                column.setPreferredWidth(70);
            }else if(i==30){
                column.setPreferredWidth(70);
            }else if(i==31){
                column.setPreferredWidth(70);
            }else if(i==32){
                column.setPreferredWidth(55);
            }else if(i==33){
                column.setPreferredWidth(70);
            }else if(i==34){
                column.setPreferredWidth(70);
            }else if(i==35){
                column.setPreferredWidth(70);
            }else if(i==36){
                column.setPreferredWidth(70);
            }else if(i==37){
                column.setPreferredWidth(70);
            }else if(i==38){
                column.setPreferredWidth(70);
            }else if(i==39){
                column.setPreferredWidth(70);
            }else if(i==40){
                column.setPreferredWidth(70);
            }else if(i==41){
                column.setPreferredWidth(70);
            }else if(i==42){
                column.setPreferredWidth(70);
            }else if(i==43){
                column.setPreferredWidth(70);
            }else if(i==44){
                column.setPreferredWidth(70);
            }else if(i==45){
                column.setPreferredWidth(70);
            }else if(i==46){
                column.setPreferredWidth(70);
            }else if(i==47){
                column.setPreferredWidth(70);
            }else if(i==48){
                column.setPreferredWidth(70);
            }else if(i==49){
                column.setPreferredWidth(70);
            }else if(i==50){
                column.setPreferredWidth(70);
            }else if(i==51){
                column.setPreferredWidth(70);
            }else if(i==52){
                column.setPreferredWidth(70);
            }else if(i==53){
                column.setPreferredWidth(70);
            }else if(i==54){
                column.setPreferredWidth(70);
            }else if(i==55){
                column.setPreferredWidth(70);
            }else if(i==56){
                column.setPreferredWidth(70);
            }else if(i==57){
                column.setPreferredWidth(70);
            }else if(i==58){
                column.setPreferredWidth(70);
            }else if(i==59){
                column.setPreferredWidth(70);
            }else if(i==60){
                column.setPreferredWidth(70);
            }else if(i==61){
                column.setPreferredWidth(70);
            }else if(i==62){
                column.setPreferredWidth(70);
            }else if(i==63){
                column.setPreferredWidth(70);
            }else if(i==64){
                column.setPreferredWidth(70);
            }else if(i==65){
                column.setPreferredWidth(70);
            }else if(i==66){
                column.setPreferredWidth(70);
            }else if(i==67){
                column.setPreferredWidth(70);
            }else if(i==68){
                column.setPreferredWidth(70);
            }else if(i==69){
                column.setPreferredWidth(70);
            }else if(i==70){
                column.setPreferredWidth(70);
            }else if(i==71){
                column.setPreferredWidth(70);
            }else if(i==72){
                column.setPreferredWidth(70);
            }else if(i==73){
                column.setPreferredWidth(70);
            }else if(i==74){
                column.setPreferredWidth(70);
            }else if(i==75){
                column.setPreferredWidth(70);
            }else if(i==76){
                column.setPreferredWidth(70);                
            }else if(i==77){
                column.setPreferredWidth(70);                
            }else if(i==78){
                column.setPreferredWidth(70);                
            }else if(i==79){
                column.setPreferredWidth(70);                
            }else if(i==80){
                column.setPreferredWidth(70);
            }else if(i==81){
                column.setPreferredWidth(70);
            }else if(i==82){
                column.setPreferredWidth(70);
            }else if(i==83){
                column.setPreferredWidth(70);                
            }else if(i==84){
                column.setPreferredWidth(70);                
            }else if(i==85){
                column.setPreferredWidth(70);                
            }else if(i==86){
                column.setPreferredWidth(70);   
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
        Tensi.setDocument(new batasInput((byte)8).getKata(Tensi));
        Nadi.setDocument(new batasInput((byte)5).getKata(Nadi));
        Rr.setDocument(new batasInput((byte)5).getKata(Rr));
        Suhu.setDocument(new batasInput((byte)5).getKata(Suhu));
        Tb.setDocument(new batasInput((byte)5).getKata(Tb));
        Bb.setDocument(new batasInput((byte)5).getKata(Bb));
        Audiometri.setDocument(new batasInput((int)2000).getKata(Audiometri));
        PemeriksaanLaboratorium.setDocument(new batasInput((int)200).getKata(PemeriksaanLaboratorium));
        EKG.setDocument(new batasInput((int)200).getKata(EKG));
        RongsenThorax.setDocument(new batasInput((int)200).getKata(RongsenThorax));
        Kesimpulan.setDocument(new batasInput((int)100).getKata(Kesimpulan));
        KetExtremitasAtas.setDocument(new batasInput((int)200).getKata(KetExtremitasAtas));
        KetExtremitasBawah.setDocument(new batasInput((int)200).getKata(KetExtremitasBawah));
        TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){                   
                    kddok.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    namadokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                }  
                kddok.requestFocus();
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
            cariradiologi.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(cariradiologi.getTable().getSelectedRow()!= -1){
                    RongsenThorax.append(cariradiologi.getTable().getValueAt(cariradiologi.getTable().getSelectedRow(),2).toString()+", ");
                    RongsenThorax.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        carilaborat.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(carilaborat.getTable().getSelectedRow()!= -1){
                    PemeriksaanLaboratorium.append(carilaborat.getTable().getValueAt(carilaborat.getTable().getSelectedRow(),2).toString()+", ");
                    PemeriksaanLaboratorium.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        Bb.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isBMI();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isBMI();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isBMI();
            }
        });
        
        Tb.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e) {
                isBMI();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                isBMI();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                isBMI();
            }
        });
        
        
        HTMLEditorKit kit = new HTMLEditorKit();
        LoadHTML.setEditable(true);
        LoadHTML.setEditorKit(kit);
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule(
                ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
        );
        Document doc = kit.createDefaultDocument();
        LoadHTML.setDocument(doc);
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnPenilaianMCU = new javax.swing.JMenuItem();
        MnPenunjangKesimpulan = new javax.swing.JMenuItem();
        WindowPenunjangKesimpulan = new javax.swing.JDialog();
        internalFrame7 = new widget.InternalFrame();
        panelGlass7 = new widget.panelisi();
        BtnSimpan1 = new widget.Button();
        BtnCloseIn6 = new widget.Button();
        jPanel4 = new javax.swing.JPanel();
        Scroll5 = new widget.ScrollPane();
        Kesan1 = new widget.TextArea();
        Scroll6 = new widget.ScrollPane();
        Saran1 = new widget.TextArea();
        Scroll7 = new widget.ScrollPane();
        Thorax = new widget.TextArea();
        Scroll8 = new widget.ScrollPane();
        Lab = new widget.TextArea();
        Scroll9 = new widget.ScrollPane();
        Ekg = new widget.TextArea();
        Kd2 = new widget.TextBox();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        kddok = new widget.TextBox();
        namadokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        Bb = new widget.TextBox();
        jLabel15 = new widget.Label();
        jLabel16 = new widget.Label();
        Nadi = new widget.TextBox();
        jLabel17 = new widget.Label();
        jLabel18 = new widget.Label();
        Suhu = new widget.TextBox();
        jLabel22 = new widget.Label();
        Tensi = new widget.TextBox();
        jLabel20 = new widget.Label();
        jLabel23 = new widget.Label();
        jLabel25 = new widget.Label();
        Rr = new widget.TextBox();
        jLabel26 = new widget.Label();
        jLabel53 = new widget.Label();
        Tanggal = new widget.Tanggal();
        jLabel28 = new widget.Label();
        Tb = new widget.TextBox();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel9 = new widget.Label();
        jLabel30 = new widget.Label();
        jLabel31 = new widget.Label();
        jLabel54 = new widget.Label();
        jLabel29 = new widget.Label();
        jLabel32 = new widget.Label();
        jLabel33 = new widget.Label();
        jLabel34 = new widget.Label();
        jLabel35 = new widget.Label();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        jLabel39 = new widget.Label();
        jLabel55 = new widget.Label();
        RiwayatDT = new widget.ComboBox();
        jLabel50 = new widget.Label();
        RiwayatDM = new widget.ComboBox();
        jLabel56 = new widget.Label();
        RiwayatParu = new widget.ComboBox();
        jLabel51 = new widget.Label();
        RiwayatJantung = new widget.ComboBox();
        jLabel57 = new widget.Label();
        Ku = new widget.ComboBox();
        jLabel52 = new widget.Label();
        jLabel41 = new widget.Label();
        scrollPane8 = new widget.ScrollPane();
        Audiometri = new widget.TextArea();
        jLabel47 = new widget.Label();
        jLabel49 = new widget.Label();
        jLabel58 = new widget.Label();
        jLabel43 = new widget.Label();
        jLabel59 = new widget.Label();
        jLabel60 = new widget.Label();
        scrollPane10 = new widget.ScrollPane();
        PemeriksaanLaboratorium = new widget.TextArea();
        jLabel61 = new widget.Label();
        jLabel62 = new widget.Label();
        scrollPane11 = new widget.ScrollPane();
        RongsenThorax = new widget.TextArea();
        scrollPane12 = new widget.ScrollPane();
        EKG = new widget.TextArea();
        jSeparator14 = new javax.swing.JSeparator();
        scrollPane14 = new widget.ScrollPane();
        Kesimpulan = new widget.TextArea();
        jLabel102 = new widget.Label();
        jSeparator16 = new javax.swing.JSeparator();
        jLabel104 = new widget.Label();
        jLabel45 = new widget.Label();
        Submandibula = new widget.ComboBox();
        Kesadaran = new widget.ComboBox();
        Leher = new widget.ComboBox();
        Axila = new widget.ComboBox();
        Inguinal = new widget.ComboBox();
        Supraklavikula = new widget.ComboBox();
        Oedema = new widget.ComboBox();
        NyeriTekanSinusFrontalis = new widget.ComboBox();
        NyeriTekananSinusMaxilaris = new widget.ComboBox();
        Palpebra = new widget.ComboBox();
        Sklera = new widget.ComboBox();
        TestButaWarna = new widget.ComboBox();
        Konjungtiva = new widget.ComboBox();
        Cornea = new widget.ComboBox();
        Pupil = new widget.ComboBox();
        LubangTelinga = new widget.ComboBox();
        jLabel63 = new widget.Label();
        LubangTelinga1 = new widget.ComboBox();
        DaunTelinga = new widget.ComboBox();
        jLabel64 = new widget.Label();
        SelaputPendengaran = new widget.ComboBox();
        jLabel65 = new widget.Label();
        NyeriMastoideus = new widget.ComboBox();
        jLabel46 = new widget.Label();
        jLabel66 = new widget.Label();
        SeptumNasi = new widget.ComboBox();
        jLabel67 = new widget.Label();
        LubangHidung = new widget.ComboBox();
        jLabel68 = new widget.Label();
        jLabel69 = new widget.Label();
        jLabel70 = new widget.Label();
        jLabel71 = new widget.Label();
        jLabel72 = new widget.Label();
        jLabel73 = new widget.Label();
        Bibir = new widget.ComboBox();
        Caries = new widget.ComboBox();
        Lidah = new widget.ComboBox();
        Faring = new widget.ComboBox();
        Tonsil = new widget.ComboBox();
        jLabel74 = new widget.Label();
        jLabel75 = new widget.Label();
        jLabel76 = new widget.Label();
        KelenjarLimfe = new widget.ComboBox();
        KelenjarGondok = new widget.ComboBox();
        jLabel77 = new widget.Label();
        jLabel78 = new widget.Label();
        jLabel79 = new widget.Label();
        jLabel80 = new widget.Label();
        jLabel81 = new widget.Label();
        jLabel82 = new widget.Label();
        jLabel83 = new widget.Label();
        jLabel84 = new widget.Label();
        GerakanDada = new widget.ComboBox();
        BunyiNapas = new widget.ComboBox();
        VocalFremitus = new widget.ComboBox();
        BunyiTambahan = new widget.ComboBox();
        Perkusi = new widget.ComboBox();
        jLabel44 = new widget.Label();
        jLabel85 = new widget.Label();
        jLabel86 = new widget.Label();
        jLabel87 = new widget.Label();
        jLabel88 = new widget.Label();
        jLabel89 = new widget.Label();
        jLabel90 = new widget.Label();
        jLabel91 = new widget.Label();
        jLabel92 = new widget.Label();
        jLabel93 = new widget.Label();
        jLabel94 = new widget.Label();
        jLabel95 = new widget.Label();
        jLabel96 = new widget.Label();
        jLabel97 = new widget.Label();
        jLabel98 = new widget.Label();
        jLabel99 = new widget.Label();
        jLabel100 = new widget.Label();
        IctusCordis = new widget.ComboBox();
        BunyiJantung = new widget.ComboBox();
        BatasJantung = new widget.ComboBox();
        Inspeksi = new widget.ComboBox();
        Palpasi = new widget.ComboBox();
        PerkusiAbdomen = new widget.ComboBox();
        Auskultasi = new widget.ComboBox();
        Hepar = new widget.ComboBox();
        Limpa = new widget.ComboBox();
        NyeriKetok = new widget.ComboBox();
        ExtremitasAtas = new widget.ComboBox();
        ExtremitasBawah = new widget.ComboBox();
        KetExtremitasAtas = new widget.TextBox();
        KetExtremitasBawah = new widget.TextBox();
        Kulit = new widget.ComboBox();
        jLabel13 = new widget.Label();
        Imt = new widget.TextBox();
        jLabel24 = new widget.Label();
        MataKanan = new widget.TextBox();
        jLabel27 = new widget.Label();
        jLabel40 = new widget.Label();
        MataKiri = new widget.TextBox();
        jLabel101 = new widget.Label();
        RiwayatHati = new widget.ComboBox();
        jLabel105 = new widget.Label();
        RiwayatBenjolan = new widget.ComboBox();
        jLabel103 = new widget.Label();
        RiwayatMaag = new widget.ComboBox();
        jLabel106 = new widget.Label();
        RiwayatTertentu = new widget.ComboBox();
        jLabel14 = new widget.Label();
        Merokok = new widget.TextBox();
        jLabel36 = new widget.Label();
        Olahraga = new widget.TextBox();
        Haid = new widget.TextBox();
        jLabel42 = new widget.Label();
        Hamil = new widget.TextBox();
        jLabel107 = new widget.Label();
        Hpht = new widget.TextBox();
        jLabel108 = new widget.Label();
        Kb = new widget.TextBox();
        jLabel109 = new widget.Label();
        jLabel110 = new widget.Label();
        PakaiKacamata = new widget.ComboBox();
        jLabel111 = new widget.Label();
        UkuranKacamata = new widget.TextBox();
        btnPemeriksaanLab = new widget.Button();
        btnPemeriksaanPj = new widget.Button();
        jLabel48 = new widget.Label();
        jLabel112 = new widget.Label();
        jLabel113 = new widget.Label();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnPenilaianMCU.setBackground(new java.awt.Color(255, 255, 254));
        MnPenilaianMCU.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenilaianMCU.setForeground(new java.awt.Color(50, 50, 50));
        MnPenilaianMCU.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenilaianMCU.setText("Laporan Penilaian MCU");
        MnPenilaianMCU.setName("MnPenilaianMCU"); // NOI18N
        MnPenilaianMCU.setPreferredSize(new java.awt.Dimension(220, 26));
        MnPenilaianMCU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenilaianMCUActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenilaianMCU);

        MnPenunjangKesimpulan.setBackground(new java.awt.Color(255, 255, 254));
        MnPenunjangKesimpulan.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnPenunjangKesimpulan.setForeground(new java.awt.Color(50, 50, 50));
        MnPenunjangKesimpulan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnPenunjangKesimpulan.setText("Input Penunjang dan Kesimpulan");
        MnPenunjangKesimpulan.setName("MnPenunjangKesimpulan"); // NOI18N
        MnPenunjangKesimpulan.setPreferredSize(new java.awt.Dimension(190, 26));
        MnPenunjangKesimpulan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnPenunjangKesimpulanActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnPenunjangKesimpulan);

        WindowPenunjangKesimpulan.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        WindowPenunjangKesimpulan.setName("WindowPenunjangKesimpulan"); // NOI18N
        WindowPenunjangKesimpulan.setUndecorated(true);
        WindowPenunjangKesimpulan.setResizable(false);

        internalFrame7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Penunjang dan Kesimpulan ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame7.setName("internalFrame7"); // NOI18N

        panelGlass7.setName("panelGlass7"); // NOI18N
        panelGlass7.setPreferredSize(new java.awt.Dimension(55, 55));
        panelGlass7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan1.setMnemonic('U');
        BtnSimpan1.setText("Update");
        BtnSimpan1.setToolTipText("Alt+U");
        BtnSimpan1.setName("BtnSimpan1"); // NOI18N
        BtnSimpan1.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpan1ActionPerformed(evt);
            }
        });
        BtnSimpan1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpan1KeyPressed(evt);
            }
        });
        panelGlass7.add(BtnSimpan1);

        BtnCloseIn6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/cross.png"))); // NOI18N
        BtnCloseIn6.setMnemonic('U');
        BtnCloseIn6.setText("Tutup");
        BtnCloseIn6.setToolTipText("Alt+U");
        BtnCloseIn6.setName("BtnCloseIn6"); // NOI18N
        BtnCloseIn6.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnCloseIn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCloseIn6ActionPerformed(evt);
            }
        });
        panelGlass7.add(BtnCloseIn6);

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(241, 246, 236)));
        jPanel4.setName("jPanel4"); // NOI18N
        jPanel4.setOpaque(false);
        jPanel4.setPreferredSize(new java.awt.Dimension(300, 102));

        Scroll5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)), "Audiometri", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        Scroll5.setName("Scroll5"); // NOI18N
        Scroll5.setOpaque(true);
        Scroll5.setPreferredSize(new java.awt.Dimension(182, 183));

        Kesan1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(201, 206, 196)));
        Kesan1.setColumns(20);
        Kesan1.setRows(5);
        Kesan1.setName("Kesan1"); // NOI18N
        Scroll5.setViewportView(Kesan1);

        Scroll6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)), "Kesimpulan:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        Scroll6.setName("Scroll6"); // NOI18N
        Scroll6.setOpaque(true);

        Saran1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(201, 206, 196)));
        Saran1.setColumns(20);
        Saran1.setRows(5);
        Saran1.setName("Saran1"); // NOI18N
        Scroll6.setViewportView(Saran1);

        Scroll7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)), "Rontgen Thorax:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        Scroll7.setName("Scroll7"); // NOI18N
        Scroll7.setOpaque(true);
        Scroll7.setPreferredSize(new java.awt.Dimension(182, 183));

        Thorax.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(201, 206, 196)));
        Thorax.setColumns(20);
        Thorax.setRows(5);
        Thorax.setName("Thorax"); // NOI18N
        Scroll7.setViewportView(Thorax);

        Scroll8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)), "Pemeriksaan Lab:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        Scroll8.setName("Scroll8"); // NOI18N
        Scroll8.setOpaque(true);
        Scroll8.setPreferredSize(new java.awt.Dimension(182, 183));

        Lab.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(201, 206, 196)));
        Lab.setColumns(20);
        Lab.setRows(5);
        Lab.setName("Lab"); // NOI18N
        Scroll8.setViewportView(Lab);

        Scroll9.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 253)), "EKG:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        Scroll9.setName("Scroll9"); // NOI18N
        Scroll9.setOpaque(true);
        Scroll9.setPreferredSize(new java.awt.Dimension(182, 183));

        Ekg.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(201, 206, 196)));
        Ekg.setColumns(20);
        Ekg.setRows(5);
        Ekg.setName("Ekg"); // NOI18N
        Scroll9.setViewportView(Ekg);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Scroll9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(Scroll8, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Scroll7, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                    .addComponent(Scroll5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(Scroll6, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Scroll7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Scroll8, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Scroll5, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                    .addComponent(Scroll9, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Scroll6, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout internalFrame7Layout = new javax.swing.GroupLayout(internalFrame7);
        internalFrame7.setLayout(internalFrame7Layout);
        internalFrame7Layout.setHorizontalGroup(
            internalFrame7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(internalFrame7Layout.createSequentialGroup()
                .addComponent(panelGlass7, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 645, Short.MAX_VALUE)
        );
        internalFrame7Layout.setVerticalGroup(
            internalFrame7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(internalFrame7Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panelGlass7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout WindowPenunjangKesimpulanLayout = new javax.swing.GroupLayout(WindowPenunjangKesimpulan.getContentPane());
        WindowPenunjangKesimpulan.getContentPane().setLayout(WindowPenunjangKesimpulanLayout);
        WindowPenunjangKesimpulanLayout.setHorizontalGroup(
            WindowPenunjangKesimpulanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WindowPenunjangKesimpulanLayout.createSequentialGroup()
                .addComponent(internalFrame7, javax.swing.GroupLayout.PREFERRED_SIZE, 631, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        WindowPenunjangKesimpulanLayout.setVerticalGroup(
            WindowPenunjangKesimpulanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(internalFrame7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        Kd2.setName("Kd2"); // NOI18N
        Kd2.setPreferredSize(new java.awt.Dimension(207, 23));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Medical Check Up (MCU) ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 1393));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(330, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(220, 10, 100, 23);

        label14.setText("Dokter :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 1360, 70, 23);

        kddok.setEditable(false);
        kddok.setName("kddok"); // NOI18N
        kddok.setPreferredSize(new java.awt.Dimension(80, 23));
        FormInput.add(kddok);
        kddok.setBounds(80, 1360, 100, 23);

        namadokter.setEditable(false);
        namadokter.setName("namadokter"); // NOI18N
        namadokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(namadokter);
        namadokter.setBounds(180, 1360, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(360, 1360, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(200, 40, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(260, 40, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(390, 40, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(10, 40, 60, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(350, 40, 30, 23);

        Bb.setFocusTraversalPolicyProvider(true);
        Bb.setName("Bb"); // NOI18N
        Bb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BbKeyPressed(evt);
            }
        });
        FormInput.add(Bb);
        Bb.setBounds(420, 330, 50, 23);

        jLabel15.setText("Kesadaran");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(320, 300, 70, 23);

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel16.setText("x/menit");
        jLabel16.setName("jLabel16"); // NOI18N
        FormInput.add(jLabel16);
        jLabel16.setBounds(830, 300, 50, 23);

        Nadi.setFocusTraversalPolicyProvider(true);
        Nadi.setName("Nadi"); // NOI18N
        Nadi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NadiKeyPressed(evt);
            }
        });
        FormInput.add(Nadi);
        Nadi.setBounds(780, 300, 40, 23);

        jLabel17.setText("Frekuensi Nadi :");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(690, 300, 90, 23);

        jLabel18.setText("Suhu :");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(740, 330, 40, 23);

        Suhu.setFocusTraversalPolicyProvider(true);
        Suhu.setName("Suhu"); // NOI18N
        Suhu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SuhuKeyPressed(evt);
            }
        });
        FormInput.add(Suhu);
        Suhu.setBounds(780, 330, 40, 23);

        jLabel22.setText("TD :");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(540, 300, 30, 23);

        Tensi.setFocusTraversalPolicyProvider(true);
        Tensi.setName("Tensi"); // NOI18N
        Tensi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TensiKeyPressed(evt);
            }
        });
        FormInput.add(Tensi);
        Tensi.setBounds(570, 300, 60, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("°C");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(830, 330, 30, 23);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setText("mmHg");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(640, 300, 50, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("x/menit");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(130, 330, 50, 23);

        Rr.setFocusTraversalPolicyProvider(true);
        Rr.setName("Rr"); // NOI18N
        Rr.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RrKeyPressed(evt);
            }
        });
        FormInput.add(Rr);
        Rr.setBounds(80, 330, 40, 23);

        jLabel26.setText("RR :");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(30, 330, 40, 23);

        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel53.setText("A. RIWAYAT PENYAKIT");
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 70, 180, 23);

        Tanggal.setForeground(new java.awt.Color(50, 70, 50));
        Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-12-2021 18:43:32" }));
        Tanggal.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        Tanggal.setName("Tanggal"); // NOI18N
        Tanggal.setOpaque(false);
        Tanggal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalKeyPressed(evt);
            }
        });
        FormInput.add(Tanggal);
        Tanggal.setBounds(70, 40, 130, 23);

        jLabel28.setText("Kg/m²");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(600, 330, 40, 23);

        Tb.setFocusTraversalPolicyProvider(true);
        Tb.setName("Tb"); // NOI18N
        Tb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TbActionPerformed(evt);
            }
        });
        Tb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TbKeyPressed(evt);
            }
        });
        FormInput.add(Tb);
        Tb.setBounds(250, 330, 50, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 70, 880, 1);

        jLabel9.setText("Sklera :");
        jLabel9.setName("jLabel9"); // NOI18N
        FormInput.add(jLabel9);
        jLabel9.setBounds(360, 470, 40, 23);

        jLabel30.setText("Palpebra  :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(70, 470, 100, 23);

        jLabel31.setText("Konjungtiva :");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(90, 500, 80, 23);

        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel54.setText("B. PEMERIKSAAN FISIK");
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(10, 270, 180, 23);

        jLabel29.setText("Submandibula     : ");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(110, 370, 100, 23);

        jLabel32.setText("Leher                 :");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(110, 400, 98, 23);

        jLabel33.setText("Supraklavikula   :");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(510, 370, 90, 23);

        jLabel34.setText("Kelenjar Limfe :");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(10, 370, 91, 23);

        jLabel35.setText("Axiila :");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(320, 370, 60, 23);

        jLabel37.setText("Inguinal :");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(320, 400, 60, 23);

        jLabel38.setText("Oedema :");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(80, 440, 90, 23);

        jLabel39.setText("Nyeri Tekan Sinus Frontalis :");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(270, 440, 140, 23);

        jLabel55.setText("Riwayat Penyakit Darah Tinggi :");
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(10, 90, 170, 23);

        RiwayatDT.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatDT.setName("RiwayatDT"); // NOI18N
        RiwayatDT.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatDTKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatDT);
        RiwayatDT.setBounds(180, 90, 90, 23);

        jLabel50.setText("Riwayat Penyakit Kencing Manis :");
        jLabel50.setName("jLabel50"); // NOI18N
        FormInput.add(jLabel50);
        jLabel50.setBounds(270, 90, 170, 23);

        RiwayatDM.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatDM.setName("RiwayatDM"); // NOI18N
        RiwayatDM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatDMKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatDM);
        RiwayatDM.setBounds(450, 90, 90, 23);

        jLabel56.setText("Riwayat Penyakit Asma/Paru-paru:");
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(10, 120, 170, 23);

        RiwayatParu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatParu.setName("RiwayatParu"); // NOI18N
        RiwayatParu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatParuKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatParu);
        RiwayatParu.setBounds(180, 120, 90, 23);

        jLabel51.setText("Riwayat Penyakit Jantung:");
        jLabel51.setName("jLabel51"); // NOI18N
        FormInput.add(jLabel51);
        jLabel51.setBounds(550, 90, 170, 23);

        RiwayatJantung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatJantung.setName("RiwayatJantung"); // NOI18N
        RiwayatJantung.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatJantungKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatJantung);
        RiwayatJantung.setBounds(730, 90, 90, 23);

        jLabel57.setText("Keadaan Umum");
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(10, 300, 110, 23);

        Ku.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Baik", "Tidak Baik" }));
        Ku.setName("Ku"); // NOI18N
        Ku.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KuKeyPressed(evt);
            }
        });
        FormInput.add(Ku);
        Ku.setBounds(130, 300, 174, 23);

        jLabel52.setText("Nyeri Tekanan Sinus Maxilaris:");
        jLabel52.setName("jLabel52"); // NOI18N
        FormInput.add(jLabel52);
        jLabel52.setBounds(500, 440, 150, 23);

        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel41.setText("Mata     ");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(30, 470, 40, 23);

        scrollPane8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane8.setName("scrollPane8"); // NOI18N

        Audiometri.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Audiometri.setColumns(20);
        Audiometri.setRows(5);
        Audiometri.setName("Audiometri"); // NOI18N
        Audiometri.setPreferredSize(new java.awt.Dimension(182, 92));
        Audiometri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                AudiometriKeyPressed(evt);
            }
        });
        scrollPane8.setViewportView(Audiometri);

        FormInput.add(scrollPane8);
        scrollPane8.setBounds(40, 1220, 810, 50);

        jLabel47.setText("Cornea :");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(100, 530, 70, 20);

        jLabel49.setText("Pupil :");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(370, 530, 30, 23);

        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel58.setText("Tes Buta warna :");
        jLabel58.setName("jLabel58"); // NOI18N
        FormInput.add(jLabel58);
        jLabel58.setBounds(570, 470, 120, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText("Kulit :");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(200, 950, 90, 23);

        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel59.setText("C. PEMERIKSAAN LABORATORIUM (TERLAMPIR)");
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(10, 970, 280, 23);

        jLabel60.setText("Proc. Mastoideus :");
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(70, 590, 100, 23);

        scrollPane10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane10.setName("scrollPane10"); // NOI18N

        PemeriksaanLaboratorium.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PemeriksaanLaboratorium.setColumns(20);
        PemeriksaanLaboratorium.setRows(5);
        PemeriksaanLaboratorium.setName("PemeriksaanLaboratorium"); // NOI18N
        PemeriksaanLaboratorium.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PemeriksaanLaboratoriumKeyPressed(evt);
            }
        });
        scrollPane10.setViewportView(PemeriksaanLaboratorium);

        FormInput.add(scrollPane10);
        scrollPane10.setBounds(40, 990, 810, 60);

        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel61.setText("E. EKG");
        jLabel61.setName("jLabel61"); // NOI18N
        FormInput.add(jLabel61);
        jLabel61.setBounds(10, 1120, 182, 23);

        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel62.setText("D. RONSEN THORAX");
        jLabel62.setName("jLabel62"); // NOI18N
        FormInput.add(jLabel62);
        jLabel62.setBounds(10, 1050, 182, 23);

        scrollPane11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane11.setName("scrollPane11"); // NOI18N

        RongsenThorax.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        RongsenThorax.setColumns(20);
        RongsenThorax.setRows(5);
        RongsenThorax.setText("Dalam Batas Normal");
        RongsenThorax.setName("RongsenThorax"); // NOI18N
        RongsenThorax.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RongsenThoraxKeyPressed(evt);
            }
        });
        scrollPane11.setViewportView(RongsenThorax);

        FormInput.add(scrollPane11);
        scrollPane11.setBounds(40, 1070, 810, 50);

        scrollPane12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane12.setName("scrollPane12"); // NOI18N

        EKG.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EKG.setColumns(20);
        EKG.setRows(5);
        EKG.setText("Dalam Batas Normal");
        EKG.setName("EKG"); // NOI18N
        EKG.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EKGKeyPressed(evt);
            }
        });
        scrollPane12.setViewportView(EKG);

        FormInput.add(scrollPane12);
        scrollPane12.setBounds(40, 1140, 810, 60);

        jSeparator14.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator14.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator14.setName("jSeparator14"); // NOI18N
        FormInput.add(jSeparator14);
        jSeparator14.setBounds(10, 1130, 880, 2);

        scrollPane14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane14.setName("scrollPane14"); // NOI18N

        Kesimpulan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Kesimpulan.setColumns(20);
        Kesimpulan.setRows(5);
        Kesimpulan.setName("Kesimpulan"); // NOI18N
        Kesimpulan.setPreferredSize(new java.awt.Dimension(102, 52));
        Kesimpulan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesimpulanKeyPressed(evt);
            }
        });
        scrollPane14.setViewportView(Kesimpulan);

        FormInput.add(scrollPane14);
        scrollPane14.setBounds(40, 1290, 810, 60);

        jLabel102.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel102.setText("G. KESIMPULAN");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(10, 1270, 190, 23);

        jSeparator16.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator16.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator16.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator16.setName("jSeparator16"); // NOI18N
        FormInput.add(jSeparator16);
        jSeparator16.setBounds(10, 1350, 880, 1);

        jLabel104.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel104.setText("F. AUDIOMETRI");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(10, 1200, 340, 23);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel45.setText("Kepala");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(10, 420, 50, 23);

        Submandibula.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Submandibula.setName("Submandibula"); // NOI18N
        FormInput.add(Submandibula);
        Submandibula.setBounds(210, 370, 110, 22);

        Kesadaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Composmentis", "Apatis", "Somnolen" }));
        Kesadaran.setName("Kesadaran"); // NOI18N
        FormInput.add(Kesadaran);
        Kesadaran.setBounds(400, 300, 130, 22);

        Leher.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Leher.setName("Leher"); // NOI18N
        FormInput.add(Leher);
        Leher.setBounds(210, 400, 110, 22);

        Axila.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Axila.setName("Axila"); // NOI18N
        FormInput.add(Axila);
        Axila.setBounds(390, 370, 110, 23);

        Inguinal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Inguinal.setName("Inguinal"); // NOI18N
        FormInput.add(Inguinal);
        Inguinal.setBounds(390, 400, 110, 22);

        Supraklavikula.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Supraklavikula.setName("Supraklavikula"); // NOI18N
        FormInput.add(Supraklavikula);
        Supraklavikula.setBounds(610, 370, 110, 22);

        Oedema.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        Oedema.setName("Oedema"); // NOI18N
        FormInput.add(Oedema);
        Oedema.setBounds(180, 440, 80, 22);

        NyeriTekanSinusFrontalis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriTekanSinusFrontalis.setName("NyeriTekanSinusFrontalis"); // NOI18N
        FormInput.add(NyeriTekanSinusFrontalis);
        NyeriTekanSinusFrontalis.setBounds(410, 440, 80, 22);

        NyeriTekananSinusMaxilaris.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriTekananSinusMaxilaris.setName("NyeriTekananSinusMaxilaris"); // NOI18N
        FormInput.add(NyeriTekananSinusMaxilaris);
        NyeriTekananSinusMaxilaris.setBounds(660, 440, 130, 22);

        Palpebra.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Oedem", "Ptosis" }));
        Palpebra.setName("Palpebra"); // NOI18N
        FormInput.add(Palpebra);
        Palpebra.setBounds(180, 470, 140, 23);

        Sklera.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Ikterik" }));
        Sklera.setName("Sklera"); // NOI18N
        FormInput.add(Sklera);
        Sklera.setBounds(410, 470, 140, 23);

        TestButaWarna.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Buta Warna Partial", "Buta Warna Total" }));
        TestButaWarna.setName("TestButaWarna"); // NOI18N
        FormInput.add(TestButaWarna);
        TestButaWarna.setBounds(660, 470, 130, 23);

        Konjungtiva.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Anemis", "Hiperemis" }));
        Konjungtiva.setName("Konjungtiva"); // NOI18N
        FormInput.add(Konjungtiva);
        Konjungtiva.setBounds(180, 500, 140, 22);

        Cornea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal" }));
        Cornea.setName("Cornea"); // NOI18N
        FormInput.add(Cornea);
        Cornea.setBounds(180, 530, 140, 22);

        Pupil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Isokor", "Anisokor" }));
        Pupil.setName("Pupil"); // NOI18N
        FormInput.add(Pupil);
        Pupil.setBounds(410, 530, 140, 23);

        LubangTelinga.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lapang", "Sempit", "Serumen Prop" }));
        LubangTelinga.setName("LubangTelinga"); // NOI18N
        FormInput.add(LubangTelinga);
        LubangTelinga.setBounds(180, 560, 140, 23);

        jLabel63.setText("Lubang Hidung  : ");
        jLabel63.setName("jLabel63"); // NOI18N
        FormInput.add(jLabel63);
        jLabel63.setBounds(320, 620, 90, 23);

        LubangTelinga1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lapang", "Sempit", "Serumen Prop" }));
        LubangTelinga1.setName("LubangTelinga1"); // NOI18N
        FormInput.add(LubangTelinga1);
        LubangTelinga1.setBounds(180, 560, 140, 23);

        DaunTelinga.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal" }));
        DaunTelinga.setName("DaunTelinga"); // NOI18N
        FormInput.add(DaunTelinga);
        DaunTelinga.setBounds(410, 560, 140, 23);

        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel64.setText("Daun Telinga :");
        jLabel64.setName("jLabel64"); // NOI18N
        FormInput.add(jLabel64);
        jLabel64.setBounds(330, 560, 80, 23);

        SelaputPendengaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Intak", "Tidak Intak" }));
        SelaputPendengaran.setName("SelaputPendengaran"); // NOI18N
        FormInput.add(SelaputPendengaran);
        SelaputPendengaran.setBounds(680, 560, 140, 22);

        jLabel65.setText("Selaput Pendengaran :");
        jLabel65.setName("jLabel65"); // NOI18N
        FormInput.add(jLabel65);
        jLabel65.setBounds(560, 560, 110, 23);

        NyeriMastoideus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriMastoideus.setName("NyeriMastoideus"); // NOI18N
        FormInput.add(NyeriMastoideus);
        NyeriMastoideus.setBounds(180, 590, 140, 22);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("Telinga");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(30, 560, 40, 23);

        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel66.setText("Lubang Telinga  : ");
        jLabel66.setName("jLabel66"); // NOI18N
        FormInput.add(jLabel66);
        jLabel66.setBounds(90, 560, 90, 23);

        SeptumNasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Deviasi" }));
        SeptumNasi.setName("SeptumNasi"); // NOI18N
        FormInput.add(SeptumNasi);
        SeptumNasi.setBounds(180, 620, 140, 23);

        jLabel67.setText("Tonsil : ");
        jLabel67.setName("jLabel67"); // NOI18N
        FormInput.add(jLabel67);
        jLabel67.setBounds(320, 680, 90, 23);

        LubangHidung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lapang", "Rhinore", "Epistaksis" }));
        LubangHidung.setName("LubangHidung"); // NOI18N
        FormInput.add(LubangHidung);
        LubangHidung.setBounds(410, 620, 140, 22);

        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel68.setText("Hidung");
        jLabel68.setName("jLabel68"); // NOI18N
        FormInput.add(jLabel68);
        jLabel68.setBounds(30, 620, 40, 23);

        jLabel69.setText("Septum Nasi  : ");
        jLabel69.setName("jLabel69"); // NOI18N
        FormInput.add(jLabel69);
        jLabel69.setBounds(80, 620, 90, 23);

        jLabel70.setText("Bibir  : ");
        jLabel70.setName("jLabel70"); // NOI18N
        FormInput.add(jLabel70);
        jLabel70.setBounds(80, 650, 90, 23);

        jLabel71.setText("Caries  : ");
        jLabel71.setName("jLabel71"); // NOI18N
        FormInput.add(jLabel71);
        jLabel71.setBounds(320, 650, 90, 23);

        jLabel72.setText("Lidah  : ");
        jLabel72.setName("jLabel72"); // NOI18N
        FormInput.add(jLabel72);
        jLabel72.setBounds(570, 650, 50, 23);

        jLabel73.setText("Kelenjar Gondok : ");
        jLabel73.setName("jLabel73"); // NOI18N
        FormInput.add(jLabel73);
        jLabel73.setBounds(320, 710, 90, 23);

        Bibir.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lembab", "Kering" }));
        Bibir.setName("Bibir"); // NOI18N
        FormInput.add(Bibir);
        Bibir.setBounds(180, 650, 140, 23);

        Caries.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        Caries.setName("Caries"); // NOI18N
        FormInput.add(Caries);
        Caries.setBounds(410, 650, 140, 23);

        Lidah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Bersih", "Kotor", "Tremor" }));
        Lidah.setName("Lidah"); // NOI18N
        FormInput.add(Lidah);
        Lidah.setBounds(620, 650, 130, 23);

        Faring.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Hiperemis" }));
        Faring.setName("Faring"); // NOI18N
        FormInput.add(Faring);
        Faring.setBounds(180, 680, 140, 23);

        Tonsil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "T0-T0", "T1-T1", "T2-T2", "T3-T3", "T4-T4", " " }));
        Tonsil.setName("Tonsil"); // NOI18N
        FormInput.add(Tonsil);
        Tonsil.setBounds(410, 680, 140, 23);

        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel74.setText("Mulut");
        jLabel74.setName("jLabel74"); // NOI18N
        FormInput.add(jLabel74);
        jLabel74.setBounds(30, 650, 40, 23);

        jLabel75.setText("Faring  : ");
        jLabel75.setName("jLabel75"); // NOI18N
        FormInput.add(jLabel75);
        jLabel75.setBounds(80, 680, 90, 23);

        jLabel76.setText("Bunyi Tambahan :");
        jLabel76.setName("jLabel76"); // NOI18N
        FormInput.add(jLabel76);
        jLabel76.setBounds(320, 770, 90, 23);

        KelenjarLimfe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        KelenjarLimfe.setName("KelenjarLimfe"); // NOI18N
        FormInput.add(KelenjarLimfe);
        KelenjarLimfe.setBounds(180, 710, 130, 23);

        KelenjarGondok.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        KelenjarGondok.setName("KelenjarGondok"); // NOI18N
        FormInput.add(KelenjarGondok);
        KelenjarGondok.setBounds(410, 710, 140, 23);

        jLabel77.setText("Muka :");
        jLabel77.setName("jLabel77"); // NOI18N
        FormInput.add(jLabel77);
        jLabel77.setBounds(90, 420, 50, 23);

        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel78.setText("Leher");
        jLabel78.setName("jLabel78"); // NOI18N
        FormInput.add(jLabel78);
        jLabel78.setBounds(30, 710, 40, 23);

        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel79.setText("Dada");
        jLabel79.setName("jLabel79"); // NOI18N
        FormInput.add(jLabel79);
        jLabel79.setBounds(10, 740, 40, 23);

        jLabel80.setText("Kelenjar Limfe  : ");
        jLabel80.setName("jLabel80"); // NOI18N
        FormInput.add(jLabel80);
        jLabel80.setBounds(80, 710, 90, 23);

        jLabel81.setText("Bunyi Jantung : ");
        jLabel81.setName("jLabel81"); // NOI18N
        FormInput.add(jLabel81);
        jLabel81.setBounds(320, 800, 90, 23);

        jLabel82.setText("Vocal Fremitus :");
        jLabel82.setName("jLabel82"); // NOI18N
        FormInput.add(jLabel82);
        jLabel82.setBounds(320, 740, 90, 23);

        jLabel83.setText("Perkusi :");
        jLabel83.setName("jLabel83"); // NOI18N
        FormInput.add(jLabel83);
        jLabel83.setBounds(560, 740, 50, 23);

        jLabel84.setText("Bunyi Napas :");
        jLabel84.setName("jLabel84"); // NOI18N
        FormInput.add(jLabel84);
        jLabel84.setBounds(80, 770, 90, 23);

        GerakanDada.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Simetris", "Tidak Simetris" }));
        GerakanDada.setName("GerakanDada"); // NOI18N
        FormInput.add(GerakanDada);
        GerakanDada.setBounds(180, 740, 140, 23);

        BunyiNapas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Vesikuler", "Bronkhial", "Trakeal" }));
        BunyiNapas.setName("BunyiNapas"); // NOI18N
        FormInput.add(BunyiNapas);
        BunyiNapas.setBounds(180, 770, 130, 23);

        VocalFremitus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sama", "Tidak Sama" }));
        VocalFremitus.setName("VocalFremitus"); // NOI18N
        FormInput.add(VocalFremitus);
        VocalFremitus.setBounds(420, 740, 130, 23);

        BunyiTambahan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Wheezing", "Tronkhi" }));
        BunyiTambahan.setName("BunyiTambahan"); // NOI18N
        FormInput.add(BunyiTambahan);
        BunyiTambahan.setBounds(420, 770, 130, 23);

        Perkusi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sonor", "Pekak" }));
        Perkusi.setName("Perkusi"); // NOI18N
        FormInput.add(Perkusi);
        Perkusi.setBounds(620, 740, 160, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("Paru");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(50, 740, 40, 23);

        jLabel85.setText("Gerakan Dada  : ");
        jLabel85.setName("jLabel85"); // NOI18N
        FormInput.add(jLabel85);
        jLabel85.setBounds(80, 740, 90, 23);

        jLabel86.setText("Hepar : ");
        jLabel86.setName("jLabel86"); // NOI18N
        FormInput.add(jLabel86);
        jLabel86.setBounds(320, 860, 90, 23);

        jLabel87.setText("Batas : ");
        jLabel87.setName("jLabel87"); // NOI18N
        FormInput.add(jLabel87);
        jLabel87.setBounds(530, 800, 90, 23);

        jLabel88.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel88.setText("Jantung");
        jLabel88.setName("jLabel88"); // NOI18N
        FormInput.add(jLabel88);
        jLabel88.setBounds(50, 800, 40, 23);

        jLabel89.setText("Ictus Cordis  : ");
        jLabel89.setName("jLabel89"); // NOI18N
        FormInput.add(jLabel89);
        jLabel89.setBounds(80, 800, 90, 23);

        jLabel90.setText("Inspeksi  : ");
        jLabel90.setName("jLabel90"); // NOI18N
        FormInput.add(jLabel90);
        jLabel90.setBounds(80, 830, 90, 23);

        jLabel91.setText("Palpasi  : ");
        jLabel91.setName("jLabel91"); // NOI18N
        FormInput.add(jLabel91);
        jLabel91.setBounds(320, 830, 90, 23);

        jLabel92.setText("Limpa : ");
        jLabel92.setName("jLabel92"); // NOI18N
        FormInput.add(jLabel92);
        jLabel92.setBounds(530, 860, 90, 23);

        jLabel93.setText("Ekstremitas Bawah :");
        jLabel93.setName("jLabel93"); // NOI18N
        FormInput.add(jLabel93);
        jLabel93.setBounds(550, 920, 110, 23);

        jLabel94.setText("Perkusi  : ");
        jLabel94.setName("jLabel94"); // NOI18N
        FormInput.add(jLabel94);
        jLabel94.setBounds(530, 830, 90, 23);

        jLabel95.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel95.setText("Abdomen");
        jLabel95.setName("jLabel95"); // NOI18N
        FormInput.add(jLabel95);
        jLabel95.setBounds(50, 830, 50, 23);

        jLabel96.setText("Auskultasi  : ");
        jLabel96.setName("jLabel96"); // NOI18N
        FormInput.add(jLabel96);
        jLabel96.setBounds(80, 860, 90, 23);

        jLabel97.setText("Nyeri Ktok CVA : ");
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(120, 890, 110, 23);

        jLabel98.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel98.setText("Punggung");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(50, 890, 50, 23);

        jLabel99.setText("Ekstremitas Atas :");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(120, 920, 110, 23);

        jLabel100.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel100.setText("Anggota Gerak ");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(50, 920, 90, 23);

        IctusCordis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Terlihat", "Terlihat", "Teraba", "Tidak Teraba" }));
        IctusCordis.setName("IctusCordis"); // NOI18N
        FormInput.add(IctusCordis);
        IctusCordis.setBounds(180, 800, 140, 23);

        BunyiJantung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Reguler", "Irreguler", "Korotkoff I", "II", "Gallop", "Lain-lain", "" }));
        BunyiJantung.setName("BunyiJantung"); // NOI18N
        FormInput.add(BunyiJantung);
        BunyiJantung.setBounds(420, 800, 130, 23);

        BatasJantung.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Melebar" }));
        BatasJantung.setName("BatasJantung"); // NOI18N
        FormInput.add(BatasJantung);
        BatasJantung.setBounds(620, 800, 160, 23);

        Inspeksi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Datar", "Cembung" }));
        Inspeksi.setName("Inspeksi"); // NOI18N
        Inspeksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InspeksiActionPerformed(evt);
            }
        });
        FormInput.add(Inspeksi);
        Inspeksi.setBounds(180, 830, 140, 23);

        Palpasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Supel", "Tegang (Defans Muscular)" }));
        Palpasi.setName("Palpasi"); // NOI18N
        FormInput.add(Palpasi);
        Palpasi.setBounds(420, 830, 130, 23);

        PerkusiAbdomen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Timpani", "Hipertimpani" }));
        PerkusiAbdomen.setName("PerkusiAbdomen"); // NOI18N
        FormInput.add(PerkusiAbdomen);
        PerkusiAbdomen.setBounds(620, 830, 160, 23);

        Auskultasi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Meningkat (>4x/menit)" }));
        Auskultasi.setName("Auskultasi"); // NOI18N
        FormInput.add(Auskultasi);
        Auskultasi.setBounds(180, 860, 140, 23);

        Hepar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Hepar.setName("Hepar"); // NOI18N
        FormInput.add(Hepar);
        Hepar.setBounds(420, 860, 130, 23);

        Limpa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Membesar", "Membesar" }));
        Limpa.setName("Limpa"); // NOI18N
        FormInput.add(Limpa);
        Limpa.setBounds(620, 860, 160, 23);

        NyeriKetok.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak Ada", "Ada" }));
        NyeriKetok.setName("NyeriKetok"); // NOI18N
        FormInput.add(NyeriKetok);
        NyeriKetok.setBounds(240, 890, 170, 23);

        ExtremitasAtas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak Normal; Yaitu" }));
        ExtremitasAtas.setName("ExtremitasAtas"); // NOI18N
        FormInput.add(ExtremitasAtas);
        ExtremitasAtas.setBounds(240, 920, 100, 23);

        ExtremitasBawah.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tidak  Normal; Yaitu" }));
        ExtremitasBawah.setName("ExtremitasBawah"); // NOI18N
        FormInput.add(ExtremitasBawah);
        ExtremitasBawah.setBounds(670, 920, 100, 23);

        KetExtremitasAtas.setName("KetExtremitasAtas"); // NOI18N
        FormInput.add(KetExtremitasAtas);
        KetExtremitasAtas.setBounds(350, 920, 190, 24);

        KetExtremitasBawah.setName("KetExtremitasBawah"); // NOI18N
        FormInput.add(KetExtremitasBawah);
        KetExtremitasBawah.setBounds(780, 920, 190, 24);

        Kulit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Tato", "Penyakit Kulit" }));
        Kulit.setName("Kulit"); // NOI18N
        FormInput.add(Kulit);
        Kulit.setBounds(240, 950, 180, 23);

        jLabel13.setText("Berat Badan:");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(340, 330, 70, 23);

        Imt.setFocusTraversalPolicyProvider(true);
        Imt.setName("Imt"); // NOI18N
        Imt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ImtKeyPressed(evt);
            }
        });
        FormInput.add(Imt);
        Imt.setBounds(550, 330, 50, 23);

        jLabel24.setText("IMT:");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(510, 330, 40, 23);

        MataKanan.setFocusTraversalPolicyProvider(true);
        MataKanan.setName("MataKanan"); // NOI18N
        MataKanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MataKananKeyPressed(evt);
            }
        });
        FormInput.add(MataKanan);
        MataKanan.setBounds(410, 500, 220, 23);

        jLabel27.setText("Mata Kanan:");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(340, 500, 70, 23);

        jLabel40.setText("Mata Kiri:");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(560, 530, 50, 23);

        MataKiri.setFocusTraversalPolicyProvider(true);
        MataKiri.setName("MataKiri"); // NOI18N
        MataKiri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                MataKiriKeyPressed(evt);
            }
        });
        FormInput.add(MataKiri);
        MataKiri.setBounds(620, 530, 200, 23);

        jLabel101.setText("Riwayat Penyakit Hati:");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(280, 120, 110, 23);

        RiwayatHati.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatHati.setName("RiwayatHati"); // NOI18N
        RiwayatHati.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatHatiKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatHati);
        RiwayatHati.setBounds(390, 120, 90, 23);

        jLabel105.setText("Benjolan Pada Tubuh/ Tumor:");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(490, 120, 170, 23);

        RiwayatBenjolan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatBenjolan.setName("RiwayatBenjolan"); // NOI18N
        RiwayatBenjolan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatBenjolanKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatBenjolan);
        RiwayatBenjolan.setBounds(660, 120, 90, 23);

        jLabel103.setText("Riwayat Penyakit Maag:");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(10, 150, 120, 23);

        RiwayatMaag.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatMaag.setName("RiwayatMaag"); // NOI18N
        RiwayatMaag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatMaagKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatMaag);
        RiwayatMaag.setBounds(130, 150, 90, 23);

        jLabel106.setText("Riwayat Penyakit Tertentu:");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(230, 150, 140, 23);

        RiwayatTertentu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak ada", "Ada" }));
        RiwayatTertentu.setName("RiwayatTertentu"); // NOI18N
        RiwayatTertentu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                RiwayatTertentuKeyPressed(evt);
            }
        });
        FormInput.add(RiwayatTertentu);
        RiwayatTertentu.setBounds(380, 150, 90, 23);

        jLabel14.setText("Merokok:");
        jLabel14.setName("jLabel14"); // NOI18N
        FormInput.add(jLabel14);
        jLabel14.setBounds(490, 150, 50, 23);

        Merokok.setHighlighter(null);
        Merokok.setName("Merokok"); // NOI18N
        FormInput.add(Merokok);
        Merokok.setBounds(550, 150, 270, 23);

        jLabel36.setText("Olah raga:");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(10, 180, 60, 23);

        Olahraga.setHighlighter(null);
        Olahraga.setName("Olahraga"); // NOI18N
        FormInput.add(Olahraga);
        Olahraga.setBounds(80, 180, 270, 23);

        Haid.setHighlighter(null);
        Haid.setName("Haid"); // NOI18N
        FormInput.add(Haid);
        Haid.setBounds(450, 180, 270, 23);

        jLabel42.setText("Haid:");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(410, 180, 30, 23);

        Hamil.setHighlighter(null);
        Hamil.setName("Hamil"); // NOI18N
        FormInput.add(Hamil);
        Hamil.setBounds(80, 210, 270, 23);

        jLabel107.setText("Hamil:");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(10, 210, 60, 23);

        Hpht.setHighlighter(null);
        Hpht.setName("Hpht"); // NOI18N
        FormInput.add(Hpht);
        Hpht.setBounds(420, 210, 100, 23);

        jLabel108.setText("HPHT:");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(370, 210, 40, 23);

        Kb.setHighlighter(null);
        Kb.setName("Kb"); // NOI18N
        FormInput.add(Kb);
        Kb.setBounds(590, 210, 270, 23);

        jLabel109.setText("KB:");
        jLabel109.setName("jLabel109"); // NOI18N
        FormInput.add(jLabel109);
        jLabel109.setBounds(550, 210, 30, 23);

        jLabel110.setText("Pakai Kacamata/lensa kontak:");
        jLabel110.setName("jLabel110"); // NOI18N
        FormInput.add(jLabel110);
        jLabel110.setBounds(10, 240, 150, 23);

        PakaiKacamata.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tidak", "Ya" }));
        PakaiKacamata.setName("PakaiKacamata"); // NOI18N
        PakaiKacamata.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PakaiKacamataKeyPressed(evt);
            }
        });
        FormInput.add(PakaiKacamata);
        PakaiKacamata.setBounds(160, 240, 90, 23);

        jLabel111.setText("Ukuran kacamata:");
        jLabel111.setName("jLabel111"); // NOI18N
        FormInput.add(jLabel111);
        jLabel111.setBounds(280, 240, 90, 23);

        UkuranKacamata.setHighlighter(null);
        UkuranKacamata.setName("UkuranKacamata"); // NOI18N
        FormInput.add(UkuranKacamata);
        UkuranKacamata.setBounds(380, 240, 180, 23);

        btnPemeriksaanLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPemeriksaanLab.setMnemonic('2');
        btnPemeriksaanLab.setToolTipText("Alt+2");
        btnPemeriksaanLab.setName("btnPemeriksaanLab"); // NOI18N
        btnPemeriksaanLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPemeriksaanLabActionPerformed(evt);
            }
        });
        FormInput.add(btnPemeriksaanLab);
        btnPemeriksaanLab.setBounds(850, 1000, 36, 26);

        btnPemeriksaanPj.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        btnPemeriksaanPj.setMnemonic('2');
        btnPemeriksaanPj.setToolTipText("Alt+2");
        btnPemeriksaanPj.setName("btnPemeriksaanPj"); // NOI18N
        btnPemeriksaanPj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPemeriksaanPjActionPerformed(evt);
            }
        });
        FormInput.add(btnPemeriksaanPj);
        btnPemeriksaanPj.setBounds(850, 1080, 36, 26);

        jLabel48.setText("Tinggi Badan:");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(170, 330, 70, 23);

        jLabel112.setText("cm");
        jLabel112.setName("jLabel112"); // NOI18N
        FormInput.add(jLabel112);
        jLabel112.setBounds(300, 330, 20, 23);

        jLabel113.setText("kg");
        jLabel113.setName("jLabel113"); // NOI18N
        FormInput.add(jLabel113);
        jLabel113.setBounds(470, 330, 20, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-12-2021" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "24-12-2021" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{            
            Valid.pindah(evt,TCari,BtnDokter);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(Kesimpulan.getText().trim().equals("")){
            Valid.textKosong(Palpebra,"Kesimpulan");
        }else if(namadokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Petugas");
        }else{
           if(Sequel.menyimpantf("penilaian_mcu","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",82,new String[]{
                    TNoRw.getText(),Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Tanggal.getSelectedItem().toString().substring(11,19),
                    RiwayatDT.getSelectedItem().toString(),RiwayatDM.getSelectedItem().toString(),RiwayatJantung.getSelectedItem().toString(),RiwayatParu.getSelectedItem().toString(),
                    RiwayatHati.getSelectedItem().toString(),RiwayatBenjolan.getSelectedItem().toString(),RiwayatMaag.getSelectedItem().toString(),RiwayatTertentu.getSelectedItem().toString(),
                    Merokok.getText(),Olahraga.getText(),Haid.getText(),Hamil.getText(),Hpht.getText(),Kb.getText(),PakaiKacamata.getSelectedItem().toString(),UkuranKacamata.getText(),
                    Ku.getSelectedItem().toString(),Kesadaran.getSelectedItem().toString(),Tensi.getText(),Nadi.getText(),Rr.getText(),Tb.getText(),Bb.getText(),Imt.getText(),
                    Suhu.getText(),Submandibula.getSelectedItem().toString(),Axila.getSelectedItem().toString(),Supraklavikula.getSelectedItem().toString(),
                    Leher.getSelectedItem().toString(),Inguinal.getSelectedItem().toString(),Oedema.getSelectedItem().toString(),NyeriTekanSinusFrontalis.getSelectedItem().toString(),
                    NyeriTekananSinusMaxilaris.getSelectedItem().toString(),Palpebra.getSelectedItem().toString(),Sklera.getSelectedItem().toString(),TestButaWarna.getSelectedItem().toString(),
                    Konjungtiva.getSelectedItem().toString(),MataKanan.getText(),MataKiri.getText(),Cornea.getSelectedItem().toString(),
                    Pupil.getSelectedItem().toString(),LubangTelinga.getSelectedItem().toString(),DaunTelinga.getSelectedItem().toString(),SelaputPendengaran.getSelectedItem().toString(),
                    NyeriMastoideus.getSelectedItem().toString(),SeptumNasi.getSelectedItem().toString(),LubangHidung.getSelectedItem().toString(),Bibir.getSelectedItem().toString(),
                    Caries.getSelectedItem().toString(),Lidah.getSelectedItem().toString(),Faring.getSelectedItem().toString(),Tonsil.getSelectedItem().toString(),
                    KelenjarLimfe.getSelectedItem().toString(),KelenjarGondok.getSelectedItem().toString(),GerakanDada.getSelectedItem().toString(),VocalFremitus.getSelectedItem().toString(),
                    Perkusi.getSelectedItem().toString(),BunyiNapas.getSelectedItem().toString(),BunyiTambahan.getSelectedItem().toString(),IctusCordis.getSelectedItem().toString(),
                    BunyiJantung.getSelectedItem().toString(),BatasJantung.getSelectedItem().toString(),Inspeksi.getSelectedItem().toString(),Palpasi.getSelectedItem().toString(),
                    PerkusiAbdomen.getSelectedItem().toString(),Auskultasi.getSelectedItem().toString(),Hepar.getSelectedItem().toString(),Limpa.getSelectedItem().toString(),
                    NyeriKetok.getSelectedItem().toString(),ExtremitasAtas.getSelectedItem().toString(),KetExtremitasAtas.getText(),ExtremitasBawah.getSelectedItem().toString(),
                    KetExtremitasBawah.getText(),Kulit.getSelectedItem().toString(),PemeriksaanLaboratorium.getText(),RongsenThorax.getText(),
                    EKG.getText(),Audiometri.getText(),Kesimpulan.getText(),
                    kddok.getText()
                })==true){
                    emptTeks();
            }
        }
    
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,Kesimpulan,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(akses.getkode().equals("Admin Utama")){
                hapus();
            }else{
                if(kddok.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString())){
                    hapus();
                }else{
                    JOptionPane.showMessageDialog(null,"Hanya bisa dihapus oleh petugas yang bersangkutan..!!");
                }
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(Tensi.getText().trim().equals("")){
            Valid.textKosong(Tensi,"TD(mmHg)");
        }else if(Nadi.getText().trim().equals("")){
            Valid.textKosong(Nadi,"Nadi(x/menit)");
        }else if(Rr.getText().trim().equals("")){
            Valid.textKosong(Rr,"RR(x/menit)");
        }else if(Suhu.getText().trim().equals("")){
            Valid.textKosong(Suhu,"Suhu(C)");
        }else if(Tb.getText().trim().equals("")){
            Valid.textKosong(Tb,"GCS");
        }else if(Bb.getText().trim().equals("")){
            Valid.textKosong(Bb,"BB(Kg)");       
        }else if(namadokter.getText().trim().equals("")){
            Valid.textKosong(BtnDokter,"Petugas");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(akses.getkode().equals("Admin Utama")){
                    ganti();
                }else{
                    if(kddok.getText().equals(tbObat.getValueAt(tbObat.getSelectedRow(),78).toString())){
                        ganti();
                    }else{
                        JOptionPane.showMessageDialog(null,"Hanya bisa diganti oleh petugas yang bersangkutan..!!");
                    }
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
         this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            try{
                if(TCari.getText().equals("")){
                    ps=koneksi.prepareStatement(
                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_mcu.no_rawat,penilaian_mcu.tanggal,penilaian_mcu.informasi,penilaian_mcu.palpebra,"+
                            "penilaian_mcu.konjungtiva,penilaian_mcu.sklera,penilaian_mcu.td,penilaian_mcu.hr,penilaian_mcu.rr,penilaian_mcu.suhu,penilaian_mcu.tinggi_badan,penilaian_mcu.berat_badan,"+
                            "penilaian_mcu.kesadaran,penilaian_mcu.submandibula,penilaian_mcu.leher,penilaian_mcu.supraklavikula,penilaian_mcu.axila,penilaian_mcu.inguinal,penilaian_mcu.oedema,"+
                            "penilaian_mcu.sinus_frontalis,penilaian_mcu.riwayat_penyakit_sekarang,penilaian_mcu.ket_riwayat_penyakit_sekarang,penilaian_mcu.riwayat_penyakit_keluarga,penilaian_mcu.ket_riwayat_penyakit_keluarga,penilaian_mcu.riwayat_penyakit_dahulu,penilaian_mcu.ket_riwayat_penyakit_dahulu,"+
                            "penilaian_mcu.riwayat_alergi_makanan,penilaian_mcu.ket_riwayat_alergi_makanan,penilaian_mcu.adl,penilaian_mcu.sinus_maxillaris,penilaian_mcu.merokok,penilaian_mcu.cornea,"+
                            "penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.buta_warna,penilaian_mcu.laboratorium,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,penilaian_mcu.selaput_pendengaran,penilaian_mcu.nyeri_mastoideus,"+
                            "penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,"+
                            "penilaian_mcu.gerakan_dada,penilaian_mcu.vocal_fremitus,penilaian_mcu.perkusi,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,"+
                            "penilaian_mcu.ictus_cordis, penilaian_mcu.bunyi_jantung,penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.hepar,penilaian_mcu.limpa,penilaian_mcu.nyeri_ktok,"+
                            "penilaian_mcu.extremitas_atas,penilaian_mcu.ket_extremitas_atas,penilaian_mcu.extremitas_bawah,penilaian_mcu.ket_extremitas_bawah,penilaian_mcu.kulit,"+
                            "penilaian_mcu.hasil_ekg,penilaian_mcu.hasil_thorax,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.nip,dokter.nm_dokter "+
                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                            "inner join petugas on penilaian_mcu.nip=petugas.nip where "+
                            "penilaian_mcu.tanggal between ? and ? order by penilaian_mcu.tanggal");
                }else{
                    ps=koneksi.prepareStatement(
                            "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,penilaian_mcu.no_rawat,penilaian_mcu.tanggal,penilaian_mcu.informasi,penilaian_mcu.palpebra,"+
                            "penilaian_mcu.konjungtiva,penilaian_mcu.sklera,penilaian_mcu.td,penilaian_mcu.hr,penilaian_mcu.rr,penilaian_mcu.suhu,penilaian_mcu.tinggi_badan,penilaian_mcu.berat_badan,"+
                            "penilaian_mcu.kesadaran,penilaian_mcu.submandibula,penilaian_mcu.leher,penilaian_mcu.supraklavikula,penilaian_mcu.axila,penilaian_mcu.inguinal,penilaian_mcu.oedema,"+
                            "penilaian_mcu.sinus_frontalis,penilaian_mcu.riwayat_penyakit_sekarang,penilaian_mcu.ket_riwayat_penyakit_sekarang,penilaian_mcu.riwayat_penyakit_keluarga,penilaian_mcu.ket_riwayat_penyakit_keluarga,penilaian_mcu.riwayat_penyakit_dahulu,penilaian_mcu.ket_riwayat_penyakit_dahulu,"+
                            "penilaian_mcu.riwayat_alergi_makanan,penilaian_mcu.ket_riwayat_alergi_makanan,penilaian_mcu.adl,penilaian_mcu.sinus_maxillaris,penilaian_mcu.merokok,penilaian_mcu.cornea,"+
                            "penilaian_mcu.lensa,penilaian_mcu.pupil,penilaian_mcu.buta_warna,penilaian_mcu.laboratorium,penilaian_mcu.lubang_telinga,penilaian_mcu.daun_telinga,penilaian_mcu.selaput_pendengaran,penilaian_mcu.nyeri_mastoideus,"+
                            "penilaian_mcu.septum_nasi,penilaian_mcu.lubang_hidung,penilaian_mcu.bibir,penilaian_mcu.caries,penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.kelenjar_limfe,penilaian_mcu.kelenjar_gondok,"+
                            "penilaian_mcu.gerakan_dada,penilaian_mcu.vocal_fremitus,penilaian_mcu.perkusi,penilaian_mcu.bunyi_napas,penilaian_mcu.bunyi_tambahan,"+
                            "penilaian_mcu.ictus_cordis, penilaian_mcu.bunyi_jantung,penilaian_mcu.batas,penilaian_mcu.inspeksi,penilaian_mcu.palpasi,penilaian_mcu.perkusi_abdomen,penilaian_mcu.auskultasi,penilaian_mcu.hepar,penilaian_mcu.limpa,penilaian_mcu.nyeri_ktok,"+
                            "penilaian_mcu.extremitas_atas,penilaian_mcu.ket_extremitas_atas,penilaian_mcu.extremitas_bawah,penilaian_mcu.ket_extremitas_bawah,penilaian_mcu.kulit,"+
                            "penilaian_mcu.hasil_ekg,penilaian_mcu.hasil_thorax,penilaian_mcu.alkohol,penilaian_mcu.kesimpulan,penilaian_mcu.anjuran,penilaian_mcu.nip,petugas.nama "+
                            "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                            "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                            "inner join petugas on penilaian_mcu.nip=petugas.nip where "+
                            "penilaian_mcu.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
                            "penilaian_mcu.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
                            "penilaian_mcu.tanggal between ? and ? and pasien.nm_pasien like ? or "+
                            "penilaian_mcu.tanggal between ? and ? and penilaian_mcu.nip like ? or "+
                            "penilaian_mcu.tanggal between ? and ? and dokter.nm_dokter like ? order by penilaian_mcu.tanggal");
                }

                try {
                    if(TCari.getText().equals("")){
                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    }else{
                        ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                        ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                        ps.setString(3,"%"+TCari.getText()+"%");
                        ps.setString(4,"%"+TCari.getText()+"%");
                        ps.setString(5,"%"+TCari.getText()+"%");
                        ps.setString(6,"%"+TCari.getText()+"%");
                        ps.setString(7,"%"+TCari.getText()+"%");
                    }     
                    rs=ps.executeQuery();
                    htmlContent = new StringBuilder();
                    htmlContent.append(                             
                        "<tr class='isi'>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='105px'><b>No.Rawat</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='65px'><b>No.RM</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Nama Pasien</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='50px'><b>J.K.</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='60px'><b>Tgl.Lahir</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Tanggal</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='85px'><b>Informasi</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='270px'><b>Palpebra</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='184px'><b>Konjungtiva</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='184px'><b>Sklera</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='50px'><b>TD</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='30px'><b>HR</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='30px'><b>RR</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='35px'><b>Suhu</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>Nyeri Tekan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>Berat Badan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>Kesadaran</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>submandibula</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>Leher</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>Spraklavikula</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>axila</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>inguinal</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>oedema</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>sinus_frontalis</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='60px'><b>Riwayat Penyakit Sekarang</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='100px'><b>Ket. Riwayat Penyakit Sekarang</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='53px'><b>riwayat_penyakit_keluarga</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='105px'><b>Keteranga riwayat_penyakit_keluarga</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='62px'><b>riwayat_penyakit_dahulu</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Keterangan riwayat_penyakit_dahulu</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='70px'><b>Riwayat Alergi Makanan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='130px'><b>Ket Riwayat Alergi Makanan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='55px'><b>ADL</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='120px'><b>Sinus Maxillaris</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='270px'><b>Pemeriksaan Rokok</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Cornea</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Lensa</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Pupil</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Tes Buta Warna</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Laboratorium</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Lubang Telinga</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Daun Telinga</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Selaput Pendengaran</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Nyeri Mastoideus</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Septum Nasi</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Lubang Hidung</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Bibir</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Caries</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Lidah</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Faring</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Tonsil</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Kelenjar Limfe</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Kelenjar Gondok</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Gerakan Dada</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Vocal Fremitus</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Perkusi</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Bunyi Napas</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Bunyi Tambahan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Ictus Cordis</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Bunyi Jantung</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Batas</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Inspeksi</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Palpasi</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Perkusi Abdomen</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Auskultasi</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Hepar</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Limpa</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Nyeri Ktok</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Extremitas Atas</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Ket Extremitas Atas</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Extremitas Bawah</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Ket Extremitas Bawah</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Kulit</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Hasil EKG</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='160px'><b>Hasil Thorax</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='200px'><b>Pemeriksaan alkohol</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Kesimpulan</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='200px'><b>Anjuran</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='80px'><b>NIP</b></td>"+
                            "<td valign='middle' bgcolor='#FFFAF8' align='center' width='150px'><b>Nama Petugas</b></td>"+
                        "</tr>"
                    );
                    while(rs.next()){
                        htmlContent.append(
                            "<tr class='isi'>"+
                               "<td valign='top'>"+rs.getString("no_rawat")+"</td>"+
                               "<td valign='top'>"+rs.getString("no_rkm_medis")+"</td>"+
                               "<td valign='top'>"+rs.getString("nm_pasien")+"</td>"+
                               "<td valign='top'>"+rs.getString("jk")+"</td>"+
                               "<td valign='top'>"+rs.getString("tgl_lahir")+"</td>"+
                               "<td valign='top'>"+rs.getString("tanggal")+"</td>"+
                               "<td valign='top'>"+rs.getString("informasi")+"</td>"+
                               "<td valign='top'>"+rs.getString("palpebra")+"</td>"+
                               "<td valign='top'>"+rs.getString("konjungtiva")+"</td>"+
                               "<td valign='top'>"+rs.getString("sklera")+"</td>"+
                               "<td valign='top'>"+rs.getString("td")+"</td>"+
                               "<td valign='top'>"+rs.getString("hr")+"</td>"+
                               "<td valign='top'>"+rs.getString("rr")+"</td>"+
                               "<td valign='top'>"+rs.getString("suhu")+"</td>"+
                               "<td valign='top'>"+rs.getString("tinggi_badan")+"</td>"+
                               "<td valign='top'>"+rs.getString("berat_badan")+"</td>"+
                               "<td valign='top'>"+rs.getString("kesadaran")+"</td>"+
                               "<td valign='top'>"+rs.getString("submandibula")+"</td>"+
                               "<td valign='top'>"+rs.getString("leher")+"</td>"+
                               "<td valign='top'>"+rs.getString("supraklavikula")+"</td>"+
                               "<td valign='top'>"+rs.getString("axila")+"</td>"+
                               "<td valign='top'>"+rs.getString("inguinal")+"</td>"+
                               "<td valign='top'>"+rs.getString("oedema")+"</td>"+
                               "<td valign='top'>"+rs.getString("sinus_frontalis")+"</td>"+
                               "<td valign='top'>"+rs.getString("riwayat_penyakit_sekarang")+"</td>"+
                               "<td valign='top'>"+rs.getString("ket_riwayat_penyakit_sekarang")+"</td>"+
                               "<td valign='top'>"+rs.getString("riwayat_penyakit_keluarga")+"</td>"+
                               "<td valign='top'>"+rs.getString("ket_riwayat_penyakit_keluarga")+"</td>"+
                               "<td valign='top'>"+rs.getString("riwayat_penyakit_dahulu")+"</td>"+
                               "<td valign='top'>"+rs.getString("ket_riwayat_penyakit_dahulu")+"</td>"+
                               "<td valign='top'>"+rs.getString("riwayat_alergi_makanan")+"</td>"+
                               "<td valign='top'>"+rs.getString("ket_riwayat_alergi_makanan")+"</td>"+
                               "<td valign='top'>"+rs.getString("adl")+"</td>"+
                               "<td valign='top'>"+rs.getString("sinus_maxillaris")+"</td>"+
                               "<td valign='top'>"+rs.getString("merokok")+"</td>"+
                               "<td valign='top'>"+rs.getString("cornea")+"</td>"+
                               "<td valign='top'>"+rs.getString("lensa")+"</td>"+
                               "<td valign='top'>"+rs.getString("pupil")+"</td>"+
                               "<td valign='top'>"+rs.getString("buta_warna")+"</td>"+
                               "<td valign='top'>"+rs.getString("laboratorium")+"</td>"+
                               "<td valign='top'>"+rs.getString("lubang_telinga")+"</td>"+
                               "<td valign='top'>"+rs.getString("daun_telinga")+"</td>"+
                               "<td valign='top'>"+rs.getString("selaput_pendengaran")+"</td>"+
                               "<td valign='top'>"+rs.getString("nyeri_mastoideus")+"</td>"+
                               "<td valign='top'>"+rs.getString("septum_nasi")+"</td>"+
                               "<td valign='top'>"+rs.getString("lubang_hidung")+"</td>"+
                               "<td valign='top'>"+rs.getString("bibir")+"</td>"+
                               "<td valign='top'>"+rs.getString("caries")+"</td>"+
                               "<td valign='top'>"+rs.getString("lidah")+"</td>"+
                               "<td valign='top'>"+rs.getString("faring")+"</td>"+
                               "<td valign='top'>"+rs.getString("tonsil")+"</td>"+
                               "<td valign='top'>"+rs.getString("kelenjar_limfe")+"</td>"+
                               "<td valign='top'>"+rs.getString("kelenjar_gondok")+"</td>"+
                               "<td valign='top'>"+rs.getString("gerakan_dada")+"</td>"+
                               "<td valign='top'>"+rs.getString("vocal_fremitus")+"</td>"+
                               "<td valign='top'>"+rs.getString("perkusi")+"</td>"+
                               "<td valign='top'>"+rs.getString("bunyi_napas")+"</td>"+
                               "<td valign='top'>"+rs.getString("bunyi_tambahan")+"</td>"+
                               "<td valign='top'>"+rs.getString("ictus_cordis")+"</td>"+
                               "<td valign='top'>"+rs.getString("bunyi_jantung")+"</td>"+
                               "<td valign='top'>"+rs.getString("batas")+"</td>"+
                               "<td valign='top'>"+rs.getString("inspeksi")+"</td>"+
                               "<td valign='top'>"+rs.getString("palpasi")+"</td>"+
                               "<td valign='top'>"+rs.getString("perkusi_abdomen")+"</td>"+
                               "<td valign='top'>"+rs.getString("auskultasi")+"</td>"+
                               "<td valign='top'>"+rs.getString("hepar")+"</td>"+
                               "<td valign='top'>"+rs.getString("limpa")+"</td>"+
                               "<td valign='top'>"+rs.getString("nyeri_ktok")+"</td>"+
                               "<td valign='top'>"+rs.getString("extremitas_atas")+"</td>"+
                               "<td valign='top'>"+rs.getString("ket_extremitas_atas")+"</td>"+
                               "<td valign='top'>"+rs.getString("extremitas_bawah")+"</td>"+
                               "<td valign='top'>"+rs.getString("ket_extremitas_bawah")+"</td>"+
                               "<td valign='top'>"+rs.getString("kulit")+"</td>"+                                       
                               "<td valign='top'>"+rs.getString("hasil_ekg")+"</td>"+
                               "<td valign='top'>"+rs.getString("hasil_thorax")+"</td>"+
                               "<td valign='top'>"+rs.getString("alkohol")+"</td>"+
                               "<td valign='top'>"+rs.getString("kesimpulan")+"</td>"+
                               "<td valign='top'>"+rs.getString("anjuran")+"</td>"+
                               "<td valign='top'>"+rs.getString("nip")+"</td>"+
                               "<td valign='top'>"+rs.getString("nama")+"</td>"+
                            "</tr>");
                    }
                    LoadHTML.setText(
                        "<html>"+
                          "<table width='5500px' border='0' align='center' cellpadding='1px' cellspacing='0' class='tbl_form'>"+
                           htmlContent.toString()+
                          "</table>"+
                        "</html>"
                    );

                    File g = new File("file2.css");            
                    BufferedWriter bg = new BufferedWriter(new FileWriter(g));
                    bg.write(
                        ".isi td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-bottom: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                        ".isi2 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#323232;}"+
                        ".isi3 td{border-right: 1px solid #e2e7dd;font: 8.5px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                        ".isi4 td{font: 11px tahoma;height:12px;border-top: 1px solid #e2e7dd;background: #ffffff;color:#323232;}"+
                        ".isi5 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#AA0000;}"+
                        ".isi6 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#FF0000;}"+
                        ".isi7 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#C8C800;}"+
                        ".isi8 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#00AA00;}"+
                        ".isi9 td{font: 8.5px tahoma;border:none;height:12px;background: #ffffff;color:#969696;}"
                    );
                    bg.close();

                    File f = new File("DataPenilaianAwalMedisRanap.html");            
                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));            
                    bw.write(LoadHTML.getText().replaceAll("<head>","<head>"+
                                "<link href=\"file2.css\" rel=\"stylesheet\" type=\"text/css\" />"+
                                "<table width='5500px' border='0' align='center' cellpadding='3px' cellspacing='0' class='tbl_form'>"+
                                    "<tr class='isi2'>"+
                                        "<td valign='top' align='center'>"+
                                            "<font size='4' face='Tahoma'>"+akses.getnamars()+"</font><br>"+
                                            akses.getalamatrs()+", "+akses.getkabupatenrs()+", "+akses.getpropinsirs()+"<br>"+
                                            akses.getkontakrs()+", E-mail : "+akses.getemailrs()+"<br><br>"+
                                            "<font size='2' face='Tahoma'>DATA PENILAIAN AWAL MEDIS IGD<br><br></font>"+        
                                        "</td>"+
                                   "</tr>"+
                                "</table>")
                    );
                    bw.close();                         
                    Desktop.getDesktop().browse(f.toURI());
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                } finally{
                    if(rs!=null){
                        rs.close();
                    }
                    if(ps!=null){
                        ps.close();
                    }
                }

            }catch(Exception e){
                System.out.println("Notifikasi : "+e);
            }
        }
        this.setCursor(Cursor.getDefaultCursor());
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
            if((evt.getClickCount()==2)&&(tbObat.getSelectedColumn()==0)){
                TabRawat.setSelectedIndex(0);
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }else if(evt.getKeyCode()==KeyEvent.VK_SPACE){
                try {
                    getData();
                    TabRawat.setSelectedIndex(0);
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
}//GEN-LAST:event_tbObatKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.emptTeks();
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        Valid.pindah(evt,Kesimpulan,Kesadaran);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void BbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BbKeyPressed
        Valid.pindah(evt,Tb,Suhu);
    }//GEN-LAST:event_BbKeyPressed

    private void NadiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NadiKeyPressed
        Valid.pindah(evt,Tensi,Rr);
    }//GEN-LAST:event_NadiKeyPressed

    private void SuhuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SuhuKeyPressed
        Valid.pindah(evt,Rr,Tb);
    }//GEN-LAST:event_SuhuKeyPressed

    private void TensiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TensiKeyPressed
        Valid.pindah(evt,Nadi,Nadi);
    }//GEN-LAST:event_TensiKeyPressed

    private void RrKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RrKeyPressed
        Valid.pindah(evt,Nadi,Suhu);
    }//GEN-LAST:event_RrKeyPressed

    private void TanggalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalKeyPressed
        Valid.pindah(evt,RiwayatDT,RiwayatDM);
    }//GEN-LAST:event_TanggalKeyPressed

    private void TbKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TbKeyPressed
        Valid.pindah(evt,Suhu,Bb);
    }//GEN-LAST:event_TbKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        
    }//GEN-LAST:event_formWindowOpened

    private void RiwayatDTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatDTKeyPressed
        Valid.pindah(evt,NyeriTekanSinusFrontalis,RiwayatDM);
    }//GEN-LAST:event_RiwayatDTKeyPressed

    private void RiwayatDMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatDMKeyPressed
        Valid.pindah(evt,RiwayatDT,RiwayatDM);
    }//GEN-LAST:event_RiwayatDMKeyPressed

    private void RiwayatParuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatParuKeyPressed
        Valid.pindah(evt,RiwayatDT,RiwayatDM);
    }//GEN-LAST:event_RiwayatParuKeyPressed

    private void RiwayatJantungKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatJantungKeyPressed
        Valid.pindah(evt,RiwayatDT,RiwayatDM);
    }//GEN-LAST:event_RiwayatJantungKeyPressed

    private void KuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KuKeyPressed
        Valid.pindah(evt,RiwayatDT,RiwayatDM);
    }//GEN-LAST:event_KuKeyPressed

    private void AudiometriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AudiometriKeyPressed
        Valid.pindah(evt,NyeriTekananSinusMaxilaris,Cornea);
    }//GEN-LAST:event_AudiometriKeyPressed

    private void PemeriksaanLaboratoriumKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PemeriksaanLaboratoriumKeyPressed
        Valid.pindah(evt,TestButaWarna,LubangTelinga);
    }//GEN-LAST:event_PemeriksaanLaboratoriumKeyPressed

    private void RongsenThoraxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RongsenThoraxKeyPressed
        Valid.pindah2(evt,EKG,Kesimpulan);
    }//GEN-LAST:event_RongsenThoraxKeyPressed

    private void EKGKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EKGKeyPressed
        Valid.pindah2(evt,LubangTelinga,RongsenThorax);
    }//GEN-LAST:event_EKGKeyPressed

    private void KesimpulanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesimpulanKeyPressed
        Valid.pindah2(evt,LubangTelinga,RongsenThorax);
    }//GEN-LAST:event_KesimpulanKeyPressed

    private void MnPenilaianMCUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenilaianMCUActionPerformed
        if(tbObat.getSelectedRow()>-1){
            Map<String, Object> param = new HashMap<>();
            param.put("namars",akses.getnamars());
            param.put("alamatrs",akses.getalamatrs());
            param.put("kotars",akses.getkabupatenrs());
            param.put("propinsirs",akses.getpropinsirs());
            param.put("kontakrs",akses.getkontakrs());
            param.put("emailrs",akses.getemailrs());
            param.put("logo",Sequel.cariGambar("select logo from setting"));
            finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
            param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()+"\nID "+(finger.equals("")?tbObat.getValueAt(tbObat.getSelectedRow(),5).toString():finger)+"\n"+Valid.SetTgl3(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()));
            Valid.MyReportqry("rptCetakPenilaianAwalMCU.jasper","report","::[ Laporan Penilaian Awal MCU ]::",
                "select dokter.nm_dokter,penilaian_mcu.no_rawat,penilaian_mcu.tanggal,reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.tgl_lahir, "+
                "reg_periksa.tgl_registrasi,pasien.umur,pasien.pekerjaan,pasien.stts_nikah,perusahaan_pasien.nama_perusahaan,penilaian_mcu.darahtinggi,penilaian_mcu.kencingmanis,penilaian_mcu.jantung,penilaian_mcu.paru,penilaian_mcu.hati,"+
                "penilaian_mcu.benjolan,penilaian_mcu.maag,penilaian_mcu.tertentu,penilaian_mcu.merokok,"+
                "penilaian_mcu.olahraga,penilaian_mcu.haid,penilaian_mcu.hamil,penilaian_mcu.hpht,"+
                "penilaian_mcu.kb,penilaian_mcu.kacamata,penilaian_mcu.ukurankacamata,penilaian_mcu.ku,"+
                "penilaian_mcu.kesadaran,penilaian_mcu.tensi,penilaian_mcu.nadi,penilaian_mcu.rr,"+
                "penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.imt,penilaian_mcu.suhu,"+
                "penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,penilaian_mcu.leher,"+
                "penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinusfrontalis,penilaian_mcu.sinusmaxilaris,"+
                "penilaian_mcu.palpebra,penilaian_mcu.sklera,penilaian_mcu.butawarna,penilaian_mcu.konjungtiva,"+
                "penilaian_mcu.matakanan,penilaian_mcu.matakiri,penilaian_mcu.cornea,penilaian_mcu.pupil,"+
                "penilaian_mcu.lubangtelinga,penilaian_mcu.dauntelinga,penilaian_mcu.selaputpendengaran,penilaian_mcu.procmastoideus,"+
                "penilaian_mcu.septumnasi,penilaian_mcu.lubanghidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.limfe,"+
                "penilaian_mcu.gondok,penilaian_mcu.gerakandada,penilaian_mcu.vokalfremitus,penilaian_mcu.perkusidada,"+
                "penilaian_mcu.bunyinafas,penilaian_mcu.bunyitambahan,penilaian_mcu.ictuscordis,penilaian_mcu.bunyijantung,"+
                "penilaian_mcu.batasjantung,penilaian_mcu.inspesiabd,penilaian_mcu.palpasiabd,penilaian_mcu.perkusiabd,"+
                "penilaian_mcu.auskultasiabd,penilaian_mcu.hepar,penilaian_mcu.limpa,penilaian_mcu.nyeriketok,"+
                "penilaian_mcu.ekstremitasatas,penilaian_mcu.ket_ekstremitasatas,penilaian_mcu.ekstremitasbawah,penilaian_mcu.ket_ekstremitasbawah,"+
                "penilaian_mcu.kulit,penilaian_mcu.lab,penilaian_mcu.thorax,penilaian_mcu.ekg,penilaian_mcu.audiometri,penilaian_mcu.kesimpulan,"+
                "penilaian_mcu.nip from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join dokter on penilaian_mcu.nip=dokter.kd_dokter inner join perusahaan_pasien on pasien.perusahaan_pasien=perusahaan_pasien.kode_perusahaan where reg_periksa.no_rawat='"+tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()+"'",param);
        }
    }//GEN-LAST:event_MnPenilaianMCUActionPerformed

    private void TbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TbActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TbActionPerformed

    private void InspeksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InspeksiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InspeksiActionPerformed

    private void ImtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ImtKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ImtKeyPressed

    private void MataKananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MataKananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MataKananKeyPressed

    private void MataKiriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_MataKiriKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_MataKiriKeyPressed

    private void RiwayatHatiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatHatiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RiwayatHatiKeyPressed

    private void RiwayatBenjolanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatBenjolanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RiwayatBenjolanKeyPressed

    private void RiwayatMaagKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatMaagKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RiwayatMaagKeyPressed

    private void RiwayatTertentuKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_RiwayatTertentuKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_RiwayatTertentuKeyPressed

    private void PakaiKacamataKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PakaiKacamataKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PakaiKacamataKeyPressed

    private void btnPemeriksaanLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPemeriksaanLabActionPerformed
        if(TNoRw.getText().equals("")&&TNoRM.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            carilaborat.setNoRawat(TNoRw.getText());
            carilaborat.tampil();
            carilaborat.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            carilaborat.setLocationRelativeTo(internalFrame1);
            carilaborat.setVisible(true);
        }
    }//GEN-LAST:event_btnPemeriksaanLabActionPerformed

    private void btnPemeriksaanPjActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPemeriksaanPjActionPerformed
        if(TNoRw.getText().equals("")&&TNoRM.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Pasien masih kosong...!!!");
        }else{
            cariradiologi.setNoRawat(TNoRw.getText());
            cariradiologi.tampil();
            cariradiologi.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
            cariradiologi.setLocationRelativeTo(internalFrame1);
            cariradiologi.setVisible(true);
        }
    }//GEN-LAST:event_btnPemeriksaanPjActionPerformed

    private void BtnSimpan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpan1ActionPerformed
        if(Kd2.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data yang mau ditampilkan...!!!!");
        }else{
            if(Saran1.getText().equals("")&&Kesan1.getText().equals("")&&Lab.getText().equals("")&&Ekg.getText().equals("")&&Thorax.getText().equals("")){
                Sequel.queryu2("delete from darah_tepi where no_rawat=? and tgl_periksa=? and jam=?",3,new String[]{
                    tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),3).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()
                });
            }else{
                if(Sequel.menyimpantf2("darah_tepi","?,?,?,?,?,?,?,?","Pemeriksaan Darah Tepi", 8,new String[]{
                    tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),3).toString(),
                    tbObat.getValueAt(tbObat.getSelectedRow(),4).toString(),Lab.getText(),Ekg.getText(),Thorax.getText(),Kesan1.getText(),Saran1.getText()
                })==false){
                    Sequel.queryu2("update darah_tepi set eritrosit=?,trombosit=?,leukosit=?, saran=?,kesan=? where no_rawat=? and tgl_periksa=? and jam=?",8,new String[]{
                        Lab.getText(),Ekg.getText(),Thorax.getText(),Saran1.getText(),Kesan1.getText(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString(),
                        tbObat.getValueAt(tbObat.getSelectedRow(),3).toString(),tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()
                    });
                }
            }

            JOptionPane.showMessageDialog(null,"Proses update selesai...!!!!");
        }
    }//GEN-LAST:event_BtnSimpan1ActionPerformed

    private void BtnSimpan1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpan1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnSimpan1KeyPressed

    private void BtnCloseIn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCloseIn6ActionPerformed
        WindowPenunjangKesimpulan.dispose();
    }//GEN-LAST:event_BtnCloseIn6ActionPerformed

    private void MnPenunjangKesimpulanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnPenunjangKesimpulanActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis...!!!!");
            TCari.requestFocus();
        }else if(tbObat.getSelectedRow()<= -1){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan pilih data..!!");
        }else {
            if(Kd2.getText().equals("")){
               JOptionPane.showMessageDialog(null,"Maaf, silahkan pilih data yang mau ditampilkan...!!!!"); 
            }else{
                try {
                    Lab.setText("");
                    Thorax.setText("");
                    ps5=koneksi.prepareStatement(
                        "select saran,kesan from saran_kesan_lab where no_rawat=? and tgl_periksa=? and jam=?");  
                    try {
                        ps5.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                        ps5.setString(2,tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
                        ps5.setString(3,tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
                        rs=ps5.executeQuery();
                        while(rs.next()){      
                            Lab.setText(rs.getString("kesan"));
                            Thorax.setText(rs.getString("saran"));
                        } 
                    } catch (Exception e) {
                        System.out.println("Notif : "+e);
                    } finally{
                        if(rs!=null){
                            rs.close();
                        }
                        if(ps5!=null){
                            ps5.close();
                        }
                    }
                    WindowPenunjangKesimpulan.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
                    WindowPenunjangKesimpulan.setLocationRelativeTo(internalFrame1);
                    WindowPenunjangKesimpulan.setVisible(true);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }         
        }
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_MnPenunjangKesimpulanActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMPenilaianMCU dialog = new RMPenilaianMCU(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.TextArea Audiometri;
    private widget.ComboBox Auskultasi;
    private widget.ComboBox Axila;
    private widget.ComboBox BatasJantung;
    private widget.TextBox Bb;
    private widget.ComboBox Bibir;
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCloseIn6;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Button BtnSimpan1;
    private widget.ComboBox BunyiJantung;
    private widget.ComboBox BunyiNapas;
    private widget.ComboBox BunyiTambahan;
    private widget.ComboBox Caries;
    private widget.ComboBox Cornea;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.ComboBox DaunTelinga;
    private widget.TextArea EKG;
    private widget.TextArea Ekg;
    private widget.ComboBox ExtremitasAtas;
    private widget.ComboBox ExtremitasBawah;
    private widget.ComboBox Faring;
    private widget.PanelBiasa FormInput;
    private widget.ComboBox GerakanDada;
    private widget.TextBox Haid;
    private widget.TextBox Hamil;
    private widget.ComboBox Hepar;
    private widget.TextBox Hpht;
    private widget.ComboBox IctusCordis;
    private widget.TextBox Imt;
    private widget.ComboBox Inguinal;
    private widget.ComboBox Inspeksi;
    private widget.TextBox Jk;
    private widget.TextBox Kb;
    private widget.TextBox Kd2;
    private widget.ComboBox KelenjarGondok;
    private widget.ComboBox KelenjarLimfe;
    private widget.ComboBox Kesadaran;
    private widget.TextArea Kesan1;
    private widget.TextArea Kesimpulan;
    private widget.TextBox KetExtremitasAtas;
    private widget.TextBox KetExtremitasBawah;
    private widget.ComboBox Konjungtiva;
    private widget.ComboBox Ku;
    private widget.ComboBox Kulit;
    private widget.Label LCount;
    private widget.TextArea Lab;
    private widget.ComboBox Leher;
    private widget.ComboBox Lidah;
    private widget.ComboBox Limpa;
    private widget.editorpane LoadHTML;
    private widget.ComboBox LubangHidung;
    private widget.ComboBox LubangTelinga;
    private widget.ComboBox LubangTelinga1;
    private widget.TextBox MataKanan;
    private widget.TextBox MataKiri;
    private widget.TextBox Merokok;
    private javax.swing.JMenuItem MnPenilaianMCU;
    private javax.swing.JMenuItem MnPenunjangKesimpulan;
    private widget.TextBox Nadi;
    private widget.ComboBox NyeriKetok;
    private widget.ComboBox NyeriMastoideus;
    private widget.ComboBox NyeriTekanSinusFrontalis;
    private widget.ComboBox NyeriTekananSinusMaxilaris;
    private widget.ComboBox Oedema;
    private widget.TextBox Olahraga;
    private widget.ComboBox PakaiKacamata;
    private widget.ComboBox Palpasi;
    private widget.ComboBox Palpebra;
    private widget.TextArea PemeriksaanLaboratorium;
    private widget.ComboBox Perkusi;
    private widget.ComboBox PerkusiAbdomen;
    private widget.ComboBox Pupil;
    private widget.ComboBox RiwayatBenjolan;
    private widget.ComboBox RiwayatDM;
    private widget.ComboBox RiwayatDT;
    private widget.ComboBox RiwayatHati;
    private widget.ComboBox RiwayatJantung;
    private widget.ComboBox RiwayatMaag;
    private widget.ComboBox RiwayatParu;
    private widget.ComboBox RiwayatTertentu;
    private widget.TextArea RongsenThorax;
    private widget.TextBox Rr;
    private widget.TextArea Saran1;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll5;
    private widget.ScrollPane Scroll6;
    private widget.ScrollPane Scroll7;
    private widget.ScrollPane Scroll8;
    private widget.ScrollPane Scroll9;
    private widget.ComboBox SelaputPendengaran;
    private widget.ComboBox SeptumNasi;
    private widget.ComboBox Sklera;
    private widget.ComboBox Submandibula;
    private widget.TextBox Suhu;
    private widget.ComboBox Supraklavikula;
    private widget.TextBox TCari;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal Tanggal;
    private widget.TextBox Tb;
    private widget.TextBox Tensi;
    private widget.ComboBox TestButaWarna;
    private widget.TextBox TglLahir;
    private widget.TextArea Thorax;
    private widget.ComboBox Tonsil;
    private widget.TextBox UkuranKacamata;
    private widget.ComboBox VocalFremitus;
    private javax.swing.JDialog WindowPenunjangKesimpulan;
    private widget.Button btnPemeriksaanLab;
    private widget.Button btnPemeriksaanPj;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.InternalFrame internalFrame7;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel109;
    private widget.Label jLabel11;
    private widget.Label jLabel110;
    private widget.Label jLabel111;
    private widget.Label jLabel112;
    private widget.Label jLabel113;
    private widget.Label jLabel13;
    private widget.Label jLabel14;
    private widget.Label jLabel15;
    private widget.Label jLabel16;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel50;
    private widget.Label jLabel51;
    private widget.Label jLabel52;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel58;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel61;
    private widget.Label jLabel62;
    private widget.Label jLabel63;
    private widget.Label jLabel64;
    private widget.Label jLabel65;
    private widget.Label jLabel66;
    private widget.Label jLabel67;
    private widget.Label jLabel68;
    private widget.Label jLabel69;
    private widget.Label jLabel7;
    private widget.Label jLabel70;
    private widget.Label jLabel71;
    private widget.Label jLabel72;
    private widget.Label jLabel73;
    private widget.Label jLabel74;
    private widget.Label jLabel75;
    private widget.Label jLabel76;
    private widget.Label jLabel77;
    private widget.Label jLabel78;
    private widget.Label jLabel79;
    private widget.Label jLabel8;
    private widget.Label jLabel80;
    private widget.Label jLabel81;
    private widget.Label jLabel82;
    private widget.Label jLabel83;
    private widget.Label jLabel84;
    private widget.Label jLabel85;
    private widget.Label jLabel86;
    private widget.Label jLabel87;
    private widget.Label jLabel88;
    private widget.Label jLabel89;
    private widget.Label jLabel9;
    private widget.Label jLabel90;
    private widget.Label jLabel91;
    private widget.Label jLabel92;
    private widget.Label jLabel93;
    private widget.Label jLabel94;
    private widget.Label jLabel95;
    private widget.Label jLabel96;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator16;
    private widget.TextBox kddok;
    private widget.Label label11;
    private widget.Label label14;
    private widget.TextBox namadokter;
    private widget.panelisi panelGlass7;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.ScrollPane scrollPane10;
    private widget.ScrollPane scrollPane11;
    private widget.ScrollPane scrollPane12;
    private widget.ScrollPane scrollPane14;
    private widget.ScrollPane scrollPane8;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                "select dokter.nm_dokter,penilaian_mcu.no_rawat,penilaian_mcu.tanggal,reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.tgl_lahir,penilaian_mcu.darahtinggi,penilaian_mcu.kencingmanis,penilaian_mcu.jantung,penilaian_mcu.paru,penilaian_mcu.hati,"+
                "penilaian_mcu.benjolan,penilaian_mcu.maag,penilaian_mcu.tertentu,penilaian_mcu.merokok,"+
                "penilaian_mcu.olahraga,penilaian_mcu.haid,penilaian_mcu.hamil,penilaian_mcu.hpht,"+
                "penilaian_mcu.kb,penilaian_mcu.kacamata,penilaian_mcu.ukurankacamata,penilaian_mcu.ku,"+
                "penilaian_mcu.kesadaran,penilaian_mcu.tensi,penilaian_mcu.nadi,penilaian_mcu.rr,"+
                "penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.imt,penilaian_mcu.suhu,"+
                "penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,penilaian_mcu.leher,"+
                "penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinusfrontalis,penilaian_mcu.sinusmaxilaris,"+
                "penilaian_mcu.palpebra,penilaian_mcu.sklera,penilaian_mcu.butawarna,penilaian_mcu.konjungtiva,"+
                "penilaian_mcu.matakanan,penilaian_mcu.matakiri,penilaian_mcu.cornea,penilaian_mcu.pupil,"+
                "penilaian_mcu.lubangtelinga,penilaian_mcu.dauntelinga,penilaian_mcu.selaputpendengaran,penilaian_mcu.procmastoideus,"+
                "penilaian_mcu.septumnasi,penilaian_mcu.lubanghidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.limfe,"+
                "penilaian_mcu.gondok,penilaian_mcu.gerakandada,penilaian_mcu.vokalfremitus,penilaian_mcu.perkusidada,"+
                "penilaian_mcu.bunyinafas,penilaian_mcu.bunyitambahan,penilaian_mcu.ictuscordis,penilaian_mcu.bunyijantung,"+
                "penilaian_mcu.batasjantung,penilaian_mcu.inspesiabd,penilaian_mcu.palpasiabd,penilaian_mcu.perkusiabd,"+
                "penilaian_mcu.auskultasiabd,penilaian_mcu.hepar,penilaian_mcu.limpa,penilaian_mcu.nyeriketok,"+
                "penilaian_mcu.ekstremitasatas,penilaian_mcu.ket_ekstremitasatas,penilaian_mcu.ekstremitasbawah,penilaian_mcu.ket_ekstremitasbawah,"+
                "penilaian_mcu.kulit,penilaian_mcu.lab,penilaian_mcu.thorax,penilaian_mcu.ekg,penilaian_mcu.audiometri,penilaian_mcu.kesimpulan,"+
                "penilaian_mcu.nip from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join dokter on penilaian_mcu.nip=dokter.kd_dokter where "+
                "penilaian_mcu.tanggal between ? and ? order by penilaian_mcu.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                "select dokter.nm_dokter,penilaian_mcu.no_rawat,penilaian_mcu.tanggal,reg_periksa.no_rkm_medis,pasien.nm_pasien,pasien.jk,pasien.tgl_lahir,penilaian_mcu.darahtinggi,penilaian_mcu.kencingmanis,penilaian_mcu.jantung,penilaian_mcu.paru,penilaian_mcu.hati,"+
                "penilaian_mcu.benjolan,penilaian_mcu.maag,penilaian_mcu.tertentu,penilaian_mcu.merokok,"+
                "penilaian_mcu.olahraga,penilaian_mcu.haid,penilaian_mcu.hamil,penilaian_mcu.hpht,"+
                "penilaian_mcu.kb,penilaian_mcu.kacamata,penilaian_mcu.ukurankacamata,penilaian_mcu.ku,"+
                "penilaian_mcu.kesadaran,penilaian_mcu.tensi,penilaian_mcu.nadi,penilaian_mcu.rr,"+
                "penilaian_mcu.tb,penilaian_mcu.bb,penilaian_mcu.imt,penilaian_mcu.suhu,"+
                "penilaian_mcu.submandibula,penilaian_mcu.axilla,penilaian_mcu.supraklavikula,penilaian_mcu.leher,"+
                "penilaian_mcu.inguinal,penilaian_mcu.oedema,penilaian_mcu.sinusfrontalis,penilaian_mcu.sinusmaxilaris,"+
                "penilaian_mcu.palpebra,penilaian_mcu.sklera,penilaian_mcu.butawarna,penilaian_mcu.konjungtiva,"+
                "penilaian_mcu.matakanan,penilaian_mcu.matakiri,penilaian_mcu.cornea,penilaian_mcu.pupil,"+
                "penilaian_mcu.lubangtelinga,penilaian_mcu.dauntelinga,penilaian_mcu.selaputpendengaran,penilaian_mcu.procmastoideus,"+
                "penilaian_mcu.septumnasi,penilaian_mcu.lubanghidung,penilaian_mcu.bibir,penilaian_mcu.caries,"+
                "penilaian_mcu.lidah,penilaian_mcu.faring,penilaian_mcu.tonsil,penilaian_mcu.limfe,"+
                "penilaian_mcu.gondok,penilaian_mcu.gerakandada,penilaian_mcu.vokalfremitus,penilaian_mcu.perkusidada,"+
                "penilaian_mcu.bunyinafas,penilaian_mcu.bunyitambahan,penilaian_mcu.ictuscordis,penilaian_mcu.bunyijantung,"+
                "penilaian_mcu.batasjantung,penilaian_mcu.inspesiabd,penilaian_mcu.palpasiabd,penilaian_mcu.perkusiabd,"+
                "penilaian_mcu.auskultasiabd,penilaian_mcu.hepar,penilaian_mcu.limpa,penilaian_mcu.nyeriketok,"+
                "penilaian_mcu.ekstremitasatas,penilaian_mcu.ket_ekstremitasatas,penilaian_mcu.ekstremitasbawah,penilaian_mcu.ket_ekstremitasbawah,"+
                "penilaian_mcu.kulit,penilaian_mcu.lab,penilaian_mcu.thorax,penilaian_mcu.ekg,penilaian_mcu.audiometri,penilaian_mcu.kesimpulan,"+
                "penilaian_mcu.nip from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                "inner join penilaian_mcu on reg_periksa.no_rawat=penilaian_mcu.no_rawat "+
                "inner join dokter on penilaian_mcu.nip=dokter.kd_dokter where penilaian_mcu.tanggal between ? and ? and "+
                        "(reg_periksa.no_rawat like ? or pasien.no_rkm_medis like ? or pasien.nm_pasien like ? or "+
                        "penilaian_mcu.nip like ? or dokter.nm_dokter like ?) order by penilaian_mcu.tanggal");
            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,"%"+TCari.getText()+"%");
                    ps.setString(5,"%"+TCari.getText()+"%");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("jk"),rs.getString("tanggal"),rs.getString("tgl_lahir"),
                        rs.getString("darahtinggi"),rs.getString("kencingmanis"),rs.getString("jantung"),rs.getString("paru"),rs.getString("hati"),
                        rs.getString("benjolan"),rs.getString("maag"),rs.getString("tertentu"),rs.getString("merokok"),
                        rs.getString("olahraga"),rs.getString("haid"),rs.getString("hamil"),rs.getString("hpht"),
                        rs.getString("kb"),rs.getString("kacamata"),rs.getString("ukurankacamata"),rs.getString("ku"),
                        rs.getString("kesadaran"),rs.getString("tensi"),rs.getString("nadi"),rs.getString("rr"),
                        rs.getString("tb"),rs.getString("bb"),rs.getString("imt"),rs.getString("suhu"),
                        rs.getString("submandibula"),rs.getString("axilla"),rs.getString("supraklavikula"),rs.getString("leher"),
                        rs.getString("inguinal"),rs.getString("oedema"),rs.getString("sinusfrontalis"),rs.getString("sinusmaxilaris"),
                        rs.getString("palpebra"),rs.getString("sklera"),rs.getString("butawarna"),rs.getString("konjungtiva"),
                        rs.getString("matakanan"),rs.getString("matakiri"),rs.getString("cornea"),rs.getString("pupil"),
                        rs.getString("lubangtelinga"),rs.getString("dauntelinga"),rs.getString("selaputpendengaran"),rs.getString("procmastoideus"),
                        rs.getString("septumnasi"),rs.getString("lubanghidung"),rs.getString("bibir"),rs.getString("caries"),
                        rs.getString("lidah"),rs.getString("faring"),rs.getString("tonsil"),rs.getString("limfe"),
                        rs.getString("gondok"),rs.getString("gerakandada"),rs.getString("vokalfremitus"),rs.getString("perkusidada"),
                        rs.getString("bunyinafas"),rs.getString("bunyitambahan"),rs.getString("ictuscordis"),rs.getString("bunyijantung"),
                        rs.getString("batasjantung"),rs.getString("inspesiabd"),rs.getString("palpasiabd"),rs.getString("perkusiabd"),
                        rs.getString("auskultasiabd"),rs.getString("hepar"),rs.getString("limpa"),rs.getString("nyeriketok"),
                        rs.getString("ekstremitasatas"),rs.getString("ket_ekstremitasatas"),rs.getString("ekstremitasbawah"),rs.getString("ket_ekstremitasbawah"),
                        rs.getString("kulit"),rs.getString("lab"),rs.getString("thorax"),rs.getString("ekg"),rs.getString("audiometri"),rs.getString("kesimpulan"),
                        rs.getString("nip"),rs.getString("nm_dokter")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        Tanggal.setDate(new Date());
        Palpebra.setSelectedIndex(0);
        Sklera.setSelectedIndex(0);
        Konjungtiva.setSelectedIndex(0);
        Tensi.setText("");
        Nadi.setText("");
        Rr.setText("");
        Suhu.setText("");
        Tb.setText("");
        Bb.setText("");
        Kesadaran.setSelectedIndex(0);
        Submandibula.setSelectedIndex(0);
        Leher.setSelectedIndex(0);
        Supraklavikula.setSelectedIndex(0);
        Axila.setSelectedIndex(0);
        Oedema.setSelectedIndex(0);
        Inguinal.setSelectedIndex(0);
        NyeriTekanSinusFrontalis.setSelectedIndex(0);
        RiwayatDT.setSelectedIndex(0);
        RiwayatParu.setSelectedIndex(0);
        RiwayatDM.setSelectedIndex(0);
        Ku.setSelectedIndex(0);
        NyeriTekananSinusMaxilaris.setSelectedIndex(0);
        Audiometri.setText("");
        Cornea.setSelectedIndex(0);
        Pupil.setSelectedIndex(0);
        TestButaWarna.setSelectedIndex(0);
        PemeriksaanLaboratorium.setText("");
        LubangTelinga.setSelectedIndex(0);
        DaunTelinga.setSelectedIndex(0);
        SelaputPendengaran.setSelectedIndex(0);
        NyeriMastoideus.setSelectedIndex(0);
        SeptumNasi.setSelectedIndex(0);
        LubangHidung.setSelectedIndex(0);
        Bibir.setSelectedIndex(0);
        Caries.setSelectedIndex(0);
        Lidah.setSelectedIndex(0);
        Faring.setSelectedIndex(0);
        Tonsil.setSelectedIndex(0);
        KelenjarLimfe.setSelectedIndex(0);
        KelenjarGondok.setSelectedIndex(0);
        GerakanDada.setSelectedIndex(0);
        VocalFremitus.setSelectedIndex(0);
        Perkusi.setSelectedIndex(0);
        BunyiNapas.setSelectedIndex(0);
        BunyiTambahan.setSelectedIndex(0);
        IctusCordis.setSelectedIndex(0);
        BunyiJantung.setSelectedIndex(0);
        BatasJantung.setSelectedIndex(0);
        Inspeksi.setSelectedIndex(0);
        Palpasi.setSelectedIndex(0);
        PerkusiAbdomen.setSelectedIndex(0);
        Auskultasi.setSelectedIndex(0);
        Hepar.setSelectedIndex(0);
        Limpa.setSelectedIndex(0);
        NyeriKetok.setSelectedIndex(0);
        ExtremitasAtas.setSelectedIndex(0);
        KetExtremitasAtas.setText("");
        ExtremitasBawah.setSelectedIndex(0);
        KetExtremitasBawah.setText("");
        Kulit.setSelectedIndex(0);
        EKG.setText("");
        RongsenThorax.setText("");
        Kesimpulan.setText("");    
        TabRawat.setSelectedIndex(0);

    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            Jk.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString()); 
            TglLahir.setText(tbObat.getValueAt(tbObat.getSelectedRow(),4).toString()); 
            Palpebra.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()); 
            Konjungtiva.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()); 
            Sklera.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString()); 
            Tensi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString()); 
            Nadi.setText(tbObat.getValueAt(tbObat.getSelectedRow(),11).toString()); 
            Rr.setText(tbObat.getValueAt(tbObat.getSelectedRow(),12).toString()); 
            Suhu.setText(tbObat.getValueAt(tbObat.getSelectedRow(),13).toString()); 
            Tb.setText(tbObat.getValueAt(tbObat.getSelectedRow(),14).toString()); 
            Bb.setText(tbObat.getValueAt(tbObat.getSelectedRow(),15).toString()); 
            Kesadaran.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),16).toString()); 
            Submandibula.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),17).toString()); 
            Leher.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),18).toString()); 
            Supraklavikula.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),19).toString());  
            Axila.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString());  
            Inguinal.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),21).toString());  
            Oedema.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());  
            NyeriTekanSinusFrontalis.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString()); 
            RiwayatDT.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),24).toString()); 
            RiwayatDM.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),26).toString()); 
            RiwayatParu.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),28).toString()); 
            RiwayatJantung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),30).toString()); 
            Ku.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),32).toString()); 
            NyeriTekananSinusMaxilaris.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),33).toString()); 
            Audiometri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),34).toString());  
            Cornea.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),35).toString()); 
            Pupil.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),37).toString()); 
            TestButaWarna.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),38).toString()); 
            PemeriksaanLaboratorium.setText(tbObat.getValueAt(tbObat.getSelectedRow(),39).toString()); 
            LubangTelinga.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),40).toString()); 
            DaunTelinga.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),41).toString()); 
            SelaputPendengaran.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),42).toString()); 
            NyeriMastoideus.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),43).toString()); 
            SeptumNasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),44).toString()); 
            LubangHidung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),45).toString()); 
            Bibir.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),46).toString()); 
            Caries.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),47).toString()); 
            Lidah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),48).toString()); 
            Faring.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),49).toString()); 
            Tonsil.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),50).toString()); 
            KelenjarLimfe.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),51).toString()); 
            KelenjarGondok.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),52).toString()); 
            GerakanDada.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),53).toString()); 
            VocalFremitus.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),54).toString()); 
            Perkusi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),55).toString()); 
            BunyiNapas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),56).toString()); 
            BunyiTambahan.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),57).toString()); 
            IctusCordis.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),58).toString()); 
            BunyiJantung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),59).toString()); 
            BatasJantung.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),60).toString()); 
            Inspeksi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),61).toString()); 
            Palpasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),62).toString()); 
            PerkusiAbdomen.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),63).toString()); 
            Auskultasi.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),64).toString()); 
            Hepar.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),65).toString()); 
            Limpa.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),66).toString()); 
            NyeriKetok.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),67).toString()); 
            ExtremitasAtas.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),68).toString()); 
            KetExtremitasAtas.setText(tbObat.getValueAt(tbObat.getSelectedRow(),69).toString()); 
            ExtremitasBawah.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),70).toString()); 
            KetExtremitasBawah.setText(tbObat.getValueAt(tbObat.getSelectedRow(),71).toString()); 
            Kulit.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),72).toString()); 
            EKG.setText(tbObat.getValueAt(tbObat.getSelectedRow(),73).toString()); 
            RongsenThorax.setText(tbObat.getValueAt(tbObat.getSelectedRow(),74).toString()); 
            Kesimpulan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),76).toString()); 
            kddok.setText(tbObat.getValueAt(tbObat.getSelectedRow(),78).toString()); 
            namadokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),79).toString()); 
            Valid.SetTgl2(Tanggal,tbObat.getValueAt(tbObat.getSelectedRow(),5).toString());
        }
    }

    private void isRawat() {
        try {
            ps=koneksi.prepareStatement(
                    "select reg_periksa.no_rkm_medis,pasien.nm_pasien, if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,reg_periksa.tgl_registrasi "+
                    "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis where reg_periksa.no_rawat=?");
            try {
                ps.setString(1,TNoRw.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TNoRM.setText(rs.getString("no_rkm_medis"));
                    TPasien.setText(rs.getString("nm_pasien"));
                    DTPCari1.setDate(rs.getDate("tgl_registrasi"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt, Date tgl2) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        DTPCari2.setDate(tgl2);    
        isRawat(); 
    }
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpasien());
        BtnHapus.setEnabled(akses.getpasien());
        BtnEdit.setEnabled(akses.getpasien());
        BtnEdit.setEnabled(akses.getpasien());
        if(akses.getjml2()>=1){
            kddok.setEditable(false);
            BtnDokter.setEnabled(false);
            kddok.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", namadokter,kddok.getText());
            if(namadokter.getText().equals("")){
                kddok.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void hapus() {
        if(Sequel.queryu2tf("delete from penilaian_mcu where no_rawat=?",1,new String[]{
            tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
        })==true){
            tampil();
        }else{
            JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
        }
    }
    
    private void isBMI(){
        if((!Tb.getText().equals(""))&&(!Bb.getText().equals(""))){
            try {
                Imt.setText(Valid.SetAngka7(Valid.SetAngka(Bb.getText())/((Valid.SetAngka(Tb.getText())/100)*(Valid.SetAngka(Tb.getText())/100)))+"");
            } catch (Exception e) {
                Imt.setText("");
            }
        }
    }

    private void ganti() {
        if(Sequel.mengedittf("penilaian_mcu","no_rawat=?",
                "no_rawat=?,tanggal=?,informasi=?,palpebra=?,konjungtiva=?,sklera=?,td=?,hr=?,rr=?,suhu=?,tinggi_badan=?,berat_badan=?,kesadaran=?,submandibula=?,leher=?,supraklavikula=?,axila=?,inguinal=?,oedema=?,sinus_frontalis=?,riwayat_penyakit_sekarang=?,ket_riwayat_penyakit_sekarang=?,riwayat_penyakit_keluarga=?,ket_riwayat_penyakit_keluarga=?,riwayat_penyakit_dahulu=?,ket_riwayat_penyakit_dahulu=?,riwayat_alergi_makanan=?,ket_riwayat_alergi_makanan=?,adl=?,sinus_maxillaris=?,merokok=?,cornea=?,lensa=?,pupil=?,buta_warna=?,laboratorium=?,lubang_telinga=?,daun_telinga=?,selaput_pendengaran=?,nyeri_mastoideus=?,septum_nasi=?,lubang_hidung=?,bibir=?,caries=?,lidah=?,faring=?,tonsil=?,kelenjar_limfe=?,kelenjar_gondok=?,gerakan_dada=?,vocal_fremitus=?,perkusi=?,bunyi_napas=?,bunyi_tambahan=?,ictus_cordis=?,bunyi_jantung=?,batas=?,inspeksi=?,palpasi=?,perkusi_abdomen=?,auskultasi=?,hepar=?,limpa=?,nyeri_ktok=?,extremitas_atas=?,ket_extremitas_atas=?,extremitas_bawah=?,ket_extremitas_bawah=?,kulit=?,hasil_ekg=?,hasil_thorax=?,alkohol=?,kesimpulan=?,anjuran=?,nip=?",
                76,new String[]{
                TNoRw.getText(),
                Valid.SetTgl(Tanggal.getSelectedItem()+"")+" "+Tanggal.getSelectedItem().toString().substring(11,19),
                Palpebra.getSelectedItem().toString(),
                Konjungtiva.getSelectedItem().toString(),
                Sklera.getSelectedItem().toString(),
                Tensi.getText(),
                Nadi.getText(),
                Rr.getText(),
                Suhu.getText(),
                Tb.getText(),
                Bb.getText(),
                Kesadaran.getSelectedItem().toString(),
                Submandibula.getSelectedItem().toString(),
                Leher.getSelectedItem().toString(),
                Supraklavikula.getSelectedItem().toString(),
                Axila.getSelectedItem().toString(),
                Inguinal.getSelectedItem().toString(),
                Oedema.getSelectedItem().toString(),
                NyeriTekanSinusFrontalis.getSelectedItem().toString(),
                RiwayatDT.getSelectedItem().toString(),
                RiwayatDM.getSelectedItem().toString(),
                RiwayatParu.getSelectedItem().toString(),
                RiwayatJantung.getSelectedItem().toString(),
                Ku.getSelectedItem().toString(),
                NyeriTekananSinusMaxilaris.getSelectedItem().toString(),
                Audiometri.getText(),
                Cornea.getSelectedItem().toString(),
                Pupil.getSelectedItem().toString(),
                TestButaWarna.getSelectedItem().toString(),
                PemeriksaanLaboratorium.getText(),
                LubangTelinga.getSelectedItem().toString(),
                DaunTelinga.getSelectedItem().toString(),
                SelaputPendengaran.getSelectedItem().toString(),
                NyeriMastoideus.getSelectedItem().toString(),
                SeptumNasi.getSelectedItem().toString(),
                LubangHidung.getSelectedItem().toString(),
                Bibir.getSelectedItem().toString(),
                Caries.getSelectedItem().toString(),
                Lidah.getSelectedItem().toString(),
                Faring.getSelectedItem().toString(),
                Tonsil.getSelectedItem().toString(),
                KelenjarLimfe.getSelectedItem().toString(),
                KelenjarGondok.getSelectedItem().toString(),
                GerakanDada.getSelectedItem().toString(),
                VocalFremitus.getSelectedItem().toString(),
                Perkusi.getSelectedItem().toString(),
                BunyiNapas.getSelectedItem().toString(),
                BunyiTambahan.getSelectedItem().toString(),
                IctusCordis.getSelectedItem().toString(),
                BunyiJantung.getSelectedItem().toString(),
                BatasJantung.getSelectedItem().toString(),
                Inspeksi.getSelectedItem().toString(),
                Palpasi.getSelectedItem().toString(),
                PerkusiAbdomen.getSelectedItem().toString(),
                Auskultasi.getSelectedItem().toString(),
                Hepar.getSelectedItem().toString(),
                Limpa.getSelectedItem().toString(),
                NyeriKetok.getSelectedItem().toString(),
                ExtremitasAtas.getSelectedItem().toString(),
                KetExtremitasAtas.getText(),
                ExtremitasBawah.getSelectedItem().toString(),
                KetExtremitasBawah.getText(),
                Kulit.getSelectedItem().toString(),
                EKG.getText(),
                RongsenThorax.getText(),
                Kesimpulan.getText(),
                kddok.getText(),
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
             })==true){
                tampil();
                emptTeks();
                TabRawat.setSelectedIndex(1);
        }
    }
}
