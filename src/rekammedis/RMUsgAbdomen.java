/*
 * Kontribusi dari Abdul Wahid, RSUD Cipayung Jakarta Timur
   Di modifikasi oleh dr. Salim Mulyana
 */


package rekammedis;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;


/**
 *
 * @author perpustakaan
 */
public final class RMUsgAbdomen extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private PreparedStatement ps,ps2;
    private ResultSet rs,rs2;
    private int i=0,jml=0,index=0;
    private DlgCariDokter dokter=new DlgCariDokter(null,false);
    private boolean[] pilih; 
    private String kamar="",namakamar="";
    
    
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public RMUsgAbdomen(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No.Rawat","No.RM","Nama Pasien","Tanggal/ Jam","Keluhan Utama","Riwayat Penyakit Sekarang",
            "Riwayat Penyakit Keluarga","Riwayat Penyakit Dahulu","Keadaan Umum","Kesadaran","GCS",
            "Tensi","Nadi","Suhu","Respirasi","Pemeriksaan Fisik","Pemeriksaan Penunjang Sebelumnya",
            "Obat-obatan yang di konsumsi sebelumnya","Diagnosa Kerja","Diagnosa Banding",
            "Rencana Pemeriksaan Penunjang","Rencana Terapi","NIP","Nama Dokter"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 24; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(65);
            }else if(i==2){
                column.setPreferredWidth(160);
            }else if(i==3){
                column.setPreferredWidth(50);
            }else if(i==4){
                column.setPreferredWidth(60);
            }else if(i==5){
                column.setPreferredWidth(90);
            }else if(i==6){
                column.setPreferredWidth(90);
            }else if(i==7){
                column.setPreferredWidth(65);
            }else if(i==8){
                column.setPreferredWidth(120);
            }else if(i==9){
                column.setPreferredWidth(90);
            }else if(i==10){
                column.setPreferredWidth(35);
            }else if(i==11){
                column.setPreferredWidth(40);
            }else if(i==12){
                column.setPreferredWidth(35);
            }else if(i==13){
                column.setPreferredWidth(40);
            }else if(i==14){
                column.setPreferredWidth(35);
            }else if(i==15){
                column.setPreferredWidth(35);
            }else if(i==16){
                column.setPreferredWidth(35);
            }else if(i==17){
                column.setPreferredWidth(35);
            }else if(i==18){
                column.setPreferredWidth(180);
            }else if(i==19){
                column.setPreferredWidth(150);
            }else if(i==20){
                column.setPreferredWidth(150);
            }else if(i==21){
                column.setPreferredWidth(150);
            }else if(i==22){
                column.setPreferredWidth(100);
            }else if(i==23){
                column.setPreferredWidth(60);
            
            }
        }
         tbObat.setDefaultRenderer(Object.class, new WarnaTable());
                
 
          TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));
          Kesan.setDocument(new batasInput((int)100).getKata(Kesan));
          TCari.setDocument(new batasInput((int)100).getKata(TCari));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
            dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(dokter.getTable().getSelectedRow()!= -1){
                    Kddokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                    NmDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                    Kddokter.requestFocus();                       
                }  
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
    
        
        

        
        

    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LoadHTML = new widget.editorpane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakAsesmenAwalMedis = new javax.swing.JMenuItem();
        internalFrame1 = new widget.InternalFrame();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        TabRawat = new javax.swing.JTabbedPane();
        internalFrame2 = new widget.InternalFrame();
        scrollInput = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        label14 = new widget.Label();
        Kddokter = new widget.TextBox();
        NmDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel8 = new widget.Label();
        TglLahir = new widget.TextBox();
        Jk = new widget.TextBox();
        jLabel10 = new widget.Label();
        label11 = new widget.Label();
        jLabel11 = new widget.Label();
        jLabel53 = new widget.Label();
        cmbKU = new widget.ComboBox();
        cmbKesadaran = new widget.ComboBox();
        TglAsuhan = new widget.Tanggal();
        jLabel93 = new widget.Label();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        label12 = new widget.Label();
        TCariMasalah = new widget.TextBox();
        BtnCariPemeriksaan1 = new widget.Button();
        BtnTambahMasalah = new widget.Button();
        jLabel13 = new widget.Label();
        ddgag = new widget.ScrollPane();
        Kesan = new widget.TextArea();
        jLabel94 = new widget.Label();
        cmbKesadaran1 = new widget.ComboBox();
        jLabel95 = new widget.Label();
        cmbKesadaran2 = new widget.ComboBox();
        jLabel96 = new widget.Label();
        cmbKesadaran3 = new widget.ComboBox();
        jLabel97 = new widget.Label();
        cmbKesadaran4 = new widget.ComboBox();
        jLabel98 = new widget.Label();
        cmbKesadaran5 = new widget.ComboBox();
        cmbKesadaran6 = new widget.ComboBox();
        cmbKesadaran10 = new widget.ComboBox();
        jLabel100 = new widget.Label();
        cmbKesadaran12 = new widget.ComboBox();
        jLabel102 = new widget.Label();
        cmbKU1 = new widget.ComboBox();
        jLabel54 = new widget.Label();
        jLabel104 = new widget.Label();
        cmbKesadaran8 = new widget.ComboBox();
        cmbKesadaran9 = new widget.ComboBox();
        jLabel101 = new widget.Label();
        cmbKesadaran14 = new widget.ComboBox();
        jLabel106 = new widget.Label();
        cmbKU2 = new widget.ComboBox();
        cmbKesadaran15 = new widget.ComboBox();
        jLabel107 = new widget.Label();
        jLabel55 = new widget.Label();
        cmbKesadaran18 = new widget.ComboBox();
        jLabel109 = new widget.Label();
        cmbKesadaran21 = new widget.ComboBox();
        jLabel112 = new widget.Label();
        cmbKU3 = new widget.ComboBox();
        jLabel56 = new widget.Label();
        cmbKesadaran24 = new widget.ComboBox();
        jLabel114 = new widget.Label();
        cmbKesadaran26 = new widget.ComboBox();
        jLabel116 = new widget.Label();
        cmbKesadaran28 = new widget.ComboBox();
        jLabel118 = new widget.Label();
        cmbKU4 = new widget.ComboBox();
        jLabel57 = new widget.Label();
        cmbKesadaran40 = new widget.ComboBox();
        jLabel128 = new widget.Label();
        cmbKesadaran42 = new widget.ComboBox();
        jLabel130 = new widget.Label();
        cmbKU6 = new widget.ComboBox();
        jLabel59 = new widget.Label();
        jLabel132 = new widget.Label();
        jLabel99 = new widget.Label();
        jLabel103 = new widget.Label();
        jLabel105 = new widget.Label();
        cmbKesadaran44 = new widget.ComboBox();
        jLabel133 = new widget.Label();
        cmbKesadaran45 = new widget.ComboBox();
        jLabel134 = new widget.Label();
        cmbKesadaran46 = new widget.ComboBox();
        jLabel135 = new widget.Label();
        cmbKU7 = new widget.ComboBox();
        jLabel60 = new widget.Label();
        jLabel108 = new widget.Label();
        internalFrame3 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();

        LoadHTML.setBorder(null);
        LoadHTML.setName("LoadHTML"); // NOI18N

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakAsesmenAwalMedis.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakAsesmenAwalMedis.setText("Cetak Asesmen Awal Medis");
        MnCetakAsesmenAwalMedis.setName("MnCetakAsesmenAwalMedis"); // NOI18N
        MnCetakAsesmenAwalMedis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakAsesmenAwalMedisActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakAsesmenAwalMedis);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Hasil Pemeriksaan USG Abdomen ]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 54));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        internalFrame1.add(panelGlass8, java.awt.BorderLayout.PAGE_END);

        TabRawat.setBackground(new java.awt.Color(254, 255, 254));
        TabRawat.setForeground(new java.awt.Color(50, 50, 50));
        TabRawat.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        TabRawat.setName("TabRawat"); // NOI18N
        TabRawat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabRawatMouseClicked(evt);
            }
        });

        internalFrame2.setBorder(null);
        internalFrame2.setName("internalFrame2"); // NOI18N
        internalFrame2.setLayout(new java.awt.BorderLayout(1, 1));

        scrollInput.setName("scrollInput"); // NOI18N
        scrollInput.setPreferredSize(new java.awt.Dimension(102, 557));

        FormInput.setBackground(new java.awt.Color(255, 255, 255));
        FormInput.setBorder(null);
        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(870, 400));
        FormInput.setLayout(null);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(74, 10, 131, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        FormInput.add(TPasien);
        TPasien.setBounds(309, 10, 260, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        FormInput.add(TNoRM);
        TNoRM.setBounds(207, 10, 100, 23);

        label14.setText("Dokter :");
        label14.setName("label14"); // NOI18N
        label14.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label14);
        label14.setBounds(0, 40, 70, 23);

        Kddokter.setEditable(false);
        Kddokter.setName("Kddokter"); // NOI18N
        Kddokter.setPreferredSize(new java.awt.Dimension(80, 23));
        Kddokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KddokterKeyPressed(evt);
            }
        });
        FormInput.add(Kddokter);
        Kddokter.setBounds(74, 40, 100, 23);

        NmDokter.setEditable(false);
        NmDokter.setName("NmDokter"); // NOI18N
        NmDokter.setPreferredSize(new java.awt.Dimension(207, 23));
        FormInput.add(NmDokter);
        NmDokter.setBounds(176, 40, 180, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('2');
        BtnDokter.setToolTipText("Alt+2");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        BtnDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnDokterKeyPressed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(358, 40, 28, 23);

        jLabel8.setText("Tgl.Lahir :");
        jLabel8.setName("jLabel8"); // NOI18N
        FormInput.add(jLabel8);
        jLabel8.setBounds(580, 10, 60, 23);

        TglLahir.setEditable(false);
        TglLahir.setHighlighter(null);
        TglLahir.setName("TglLahir"); // NOI18N
        FormInput.add(TglLahir);
        TglLahir.setBounds(644, 10, 80, 23);

        Jk.setEditable(false);
        Jk.setHighlighter(null);
        Jk.setName("Jk"); // NOI18N
        FormInput.add(Jk);
        Jk.setBounds(774, 10, 80, 23);

        jLabel10.setText("No.Rawat :");
        jLabel10.setName("jLabel10"); // NOI18N
        FormInput.add(jLabel10);
        jLabel10.setBounds(0, 10, 70, 23);

        label11.setText("Tanggal :");
        label11.setName("label11"); // NOI18N
        label11.setPreferredSize(new java.awt.Dimension(70, 23));
        FormInput.add(label11);
        label11.setBounds(395, 40, 57, 23);

        jLabel11.setText("J.K. :");
        jLabel11.setName("jLabel11"); // NOI18N
        FormInput.add(jLabel11);
        jLabel11.setBounds(740, 10, 30, 23);

        jLabel53.setText("HEPAR");
        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel53.setName("jLabel53"); // NOI18N
        FormInput.add(jLabel53);
        jLabel53.setBounds(10, 80, 40, 23);

        cmbKU.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tak membesar", "Membesar" }));
        cmbKU.setName("cmbKU"); // NOI18N
        cmbKU.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKUKeyPressed(evt);
            }
        });
        FormInput.add(cmbKU);
        cmbKU.setBounds(70, 110, 120, 23);

        cmbKesadaran.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Rata", "Tidak rata" }));
        cmbKesadaran.setName("cmbKesadaran"); // NOI18N
        cmbKesadaran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaranKeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran);
        cmbKesadaran.setBounds(280, 110, 120, 23);

        TglAsuhan.setForeground(new java.awt.Color(50, 70, 50));
        TglAsuhan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "25-07-2021 16:16:53" }));
        TglAsuhan.setDisplayFormat("dd-MM-yyyy HH:mm:ss");
        TglAsuhan.setName("TglAsuhan"); // NOI18N
        TglAsuhan.setOpaque(false);
        TglAsuhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TglAsuhanKeyPressed(evt);
            }
        });
        FormInput.add(TglAsuhan);
        TglAsuhan.setBounds(456, 40, 130, 23);

        jLabel93.setText("Permukaan:");
        jLabel93.setName("jLabel93"); // NOI18N
        FormInput.add(jLabel93);
        jLabel93.setBounds(200, 110, 70, 23);

        jSeparator1.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator1.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator1.setName("jSeparator1"); // NOI18N
        FormInput.add(jSeparator1);
        jSeparator1.setBounds(0, 69, 880, 2);

        jSeparator7.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator7.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator7.setName("jSeparator7"); // NOI18N
        FormInput.add(jSeparator7);
        jSeparator7.setBounds(0, 690, 880, 1);

        jSeparator8.setBackground(new java.awt.Color(239, 244, 234));
        jSeparator8.setForeground(new java.awt.Color(239, 244, 234));
        jSeparator8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(239, 244, 234)));
        jSeparator8.setName("jSeparator8"); // NOI18N
        FormInput.add(jSeparator8);
        jSeparator8.setBounds(0, 800, 880, 1);

        label12.setText("Key Word :");
        label12.setName("label12"); // NOI18N
        label12.setPreferredSize(new java.awt.Dimension(60, 23));
        FormInput.add(label12);
        label12.setBounds(16, 1150, 60, 23);

        TCariMasalah.setToolTipText("Alt+C");
        TCariMasalah.setName("TCariMasalah"); // NOI18N
        TCariMasalah.setPreferredSize(new java.awt.Dimension(140, 23));
        TCariMasalah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariMasalahKeyPressed(evt);
            }
        });
        FormInput.add(TCariMasalah);
        TCariMasalah.setBounds(80, 1150, 245, 23);

        BtnCariPemeriksaan1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCariPemeriksaan1.setMnemonic('1');
        BtnCariPemeriksaan1.setToolTipText("Alt+1");
        BtnCariPemeriksaan1.setName("BtnCariPemeriksaan1"); // NOI18N
        BtnCariPemeriksaan1.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCariPemeriksaan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariPemeriksaan1ActionPerformed(evt);
            }
        });
        BtnCariPemeriksaan1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariPemeriksaan1KeyPressed(evt);
            }
        });
        FormInput.add(BtnCariPemeriksaan1);
        BtnCariPemeriksaan1.setBounds(330, 1150, 28, 23);

        BtnTambahMasalah.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/plus_16.png"))); // NOI18N
        BtnTambahMasalah.setMnemonic('3');
        BtnTambahMasalah.setToolTipText("Alt+3");
        BtnTambahMasalah.setName("BtnTambahMasalah"); // NOI18N
        BtnTambahMasalah.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnTambahMasalah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahMasalahActionPerformed(evt);
            }
        });
        FormInput.add(BtnTambahMasalah);
        BtnTambahMasalah.setBounds(363, 1150, 28, 23);

        jLabel13.setText("Kesan:");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(10, 580, 40, 40);

        ddgag.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ddgag.setName("ddgag"); // NOI18N

        Kesan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        Kesan.setColumns(20);
        Kesan.setRows(5);
        Kesan.setName("Kesan"); // NOI18N
        Kesan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesanKeyPressed(evt);
            }
        });
        ddgag.setViewportView(Kesan);

        FormInput.add(ddgag);
        ddgag.setBounds(60, 590, 560, 60);

        jLabel94.setText("Struktur:");
        jLabel94.setName("jLabel94"); // NOI18N
        FormInput.add(jLabel94);
        jLabel94.setBounds(410, 110, 70, 23);

        cmbKesadaran1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Kasar" }));
        cmbKesadaran1.setName("cmbKesadaran1"); // NOI18N
        cmbKesadaran1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran1KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran1);
        cmbKesadaran1.setBounds(490, 110, 120, 23);

        jLabel95.setText("Struktur:");
        jLabel95.setName("jLabel95"); // NOI18N
        FormInput.add(jLabel95);
        jLabel95.setBounds(620, 110, 70, 23);

        cmbKesadaran2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Hyperechoic", "Hypoechoic" }));
        cmbKesadaran2.setName("cmbKesadaran2"); // NOI18N
        cmbKesadaran2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran2KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran2);
        cmbKesadaran2.setBounds(700, 110, 120, 23);

        jLabel96.setText("Bentuk:");
        jLabel96.setName("jLabel96"); // NOI18N
        FormInput.add(jLabel96);
        jLabel96.setBounds(30, 420, 40, 23);

        cmbKesadaran3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tajam", "Tumpul" }));
        cmbKesadaran3.setName("cmbKesadaran3"); // NOI18N
        cmbKesadaran3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran3KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran3);
        cmbKesadaran3.setBounds(70, 140, 120, 23);

        jLabel97.setText("Ductus biliaris & system V. porta:");
        jLabel97.setName("jLabel97"); // NOI18N
        FormInput.add(jLabel97);
        jLabel97.setBounds(200, 140, 160, 23);

        cmbKesadaran4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Melebar", "Tidak melebar" }));
        cmbKesadaran4.setName("cmbKesadaran4"); // NOI18N
        cmbKesadaran4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran4KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran4);
        cmbKesadaran4.setBounds(370, 140, 120, 23);

        jLabel98.setText("Nodul:");
        jLabel98.setName("jLabel98"); // NOI18N
        FormInput.add(jLabel98);
        jLabel98.setBounds(510, 140, 50, 23);

        cmbKesadaran5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran5.setName("cmbKesadaran5"); // NOI18N
        cmbKesadaran5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran5KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran5);
        cmbKesadaran5.setBounds(570, 140, 120, 23);

        cmbKesadaran6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Single ", "Multiple" }));
        cmbKesadaran6.setName("cmbKesadaran6"); // NOI18N
        cmbKesadaran6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran6KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran6);
        cmbKesadaran6.setBounds(700, 140, 120, 23);

        cmbKesadaran10.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran10.setName("cmbKesadaran10"); // NOI18N
        cmbKesadaran10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran10KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran10);
        cmbKesadaran10.setBounds(490, 210, 40, 23);

        jLabel100.setText("Batu:");
        jLabel100.setName("jLabel100"); // NOI18N
        FormInput.add(jLabel100);
        jLabel100.setBounds(410, 210, 70, 23);

        cmbKesadaran12.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tak menebal", "Menebal" }));
        cmbKesadaran12.setName("cmbKesadaran12"); // NOI18N
        cmbKesadaran12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran12KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran12);
        cmbKesadaran12.setBounds(280, 210, 120, 23);

        jLabel102.setText("Dinding:");
        jLabel102.setName("jLabel102"); // NOI18N
        FormInput.add(jLabel102);
        jLabel102.setBounds(200, 210, 70, 23);

        cmbKU1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Membesar" }));
        cmbKU1.setName("cmbKU1"); // NOI18N
        cmbKU1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKU1KeyPressed(evt);
            }
        });
        FormInput.add(cmbKU1);
        cmbKU1.setBounds(70, 210, 120, 23);

        jLabel54.setText("VESICA FELLEA");
        jLabel54.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel54.setName("jLabel54"); // NOI18N
        FormInput.add(jLabel54);
        jLabel54.setBounds(10, 180, 90, 23);

        jLabel104.setText("Tepi:");
        jLabel104.setName("jLabel104"); // NOI18N
        FormInput.add(jLabel104);
        jLabel104.setBounds(30, 140, 30, 23);

        cmbKesadaran8.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Hyperechoic", "Hypoechoic" }));
        cmbKesadaran8.setName("cmbKesadaran8"); // NOI18N
        cmbKesadaran8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran8KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran8);
        cmbKesadaran8.setBounds(430, 270, 120, 23);

        cmbKesadaran9.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran9.setName("cmbKesadaran9"); // NOI18N
        cmbKesadaran9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran9KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran9);
        cmbKesadaran9.setBounds(610, 270, 120, 23);

        jLabel101.setText("Nodul:");
        jLabel101.setName("jLabel101"); // NOI18N
        FormInput.add(jLabel101);
        jLabel101.setBounds(550, 270, 50, 23);

        cmbKesadaran14.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Inhomogen" }));
        cmbKesadaran14.setName("cmbKesadaran14"); // NOI18N
        cmbKesadaran14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran14KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran14);
        cmbKesadaran14.setBounds(280, 270, 120, 23);

        jLabel106.setText("Struktur echo:");
        jLabel106.setName("jLabel106"); // NOI18N
        FormInput.add(jLabel106);
        jLabel106.setBounds(200, 270, 70, 23);

        cmbKU2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tak membesar", "Membesar" }));
        cmbKU2.setName("cmbKU2"); // NOI18N
        cmbKU2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKU2KeyPressed(evt);
            }
        });
        FormInput.add(cmbKU2);
        cmbKU2.setBounds(70, 270, 120, 23);

        cmbKesadaran15.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran15.setName("cmbKesadaran15"); // NOI18N
        cmbKesadaran15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran15KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran15);
        cmbKesadaran15.setBounds(70, 300, 50, 23);

        jLabel107.setText("Calsifikasi:");
        jLabel107.setName("jLabel107"); // NOI18N
        FormInput.add(jLabel107);
        jLabel107.setBounds(0, 300, 60, 23);

        jLabel55.setText("PANCREAS");
        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel55.setName("jLabel55"); // NOI18N
        FormInput.add(jLabel55);
        jLabel55.setBounds(20, 240, 60, 23);

        cmbKesadaran18.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran18.setName("cmbKesadaran18"); // NOI18N
        cmbKesadaran18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran18KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran18);
        cmbKesadaran18.setBounds(500, 360, 120, 23);

        jLabel109.setText("Nodul:");
        jLabel109.setName("jLabel109"); // NOI18N
        FormInput.add(jLabel109);
        jLabel109.setBounds(440, 360, 50, 23);

        cmbKesadaran21.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Irregular" }));
        cmbKesadaran21.setName("cmbKesadaran21"); // NOI18N
        cmbKesadaran21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran21KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran21);
        cmbKesadaran21.setBounds(290, 360, 120, 23);

        jLabel112.setText("Struktur echo:");
        jLabel112.setName("jLabel112"); // NOI18N
        FormInput.add(jLabel112);
        jLabel112.setBounds(210, 360, 70, 23);

        cmbKU3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Mengecil", "Membesar" }));
        cmbKU3.setName("cmbKU3"); // NOI18N
        cmbKU3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKU3KeyPressed(evt);
            }
        });
        FormInput.add(cmbKU3);
        cmbKU3.setBounds(80, 360, 120, 23);

        jLabel56.setText("LIEN");
        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel56.setName("jLabel56"); // NOI18N
        FormInput.add(jLabel56);
        jLabel56.setBounds(20, 330, 40, 23);

        cmbKesadaran24.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran24.setName("cmbKesadaran24"); // NOI18N
        cmbKesadaran24.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran24KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran24);
        cmbKesadaran24.setBounds(710, 420, 120, 23);

        jLabel114.setText("Batu:");
        jLabel114.setName("jLabel114"); // NOI18N
        FormInput.add(jLabel114);
        jLabel114.setBounds(630, 420, 70, 23);

        cmbKesadaran26.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Melebar" }));
        cmbKesadaran26.setName("cmbKesadaran26"); // NOI18N
        cmbKesadaran26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran26KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran26);
        cmbKesadaran26.setBounds(500, 420, 120, 23);

        jLabel116.setText("P.C.S");
        jLabel116.setName("jLabel116"); // NOI18N
        FormInput.add(jLabel116);
        jLabel116.setBounds(420, 420, 70, 23);

        cmbKesadaran28.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Hypoechoic", "Hyperechoic" }));
        cmbKesadaran28.setName("cmbKesadaran28"); // NOI18N
        cmbKesadaran28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran28KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran28);
        cmbKesadaran28.setBounds(290, 420, 120, 23);

        jLabel118.setText("Cortex:");
        jLabel118.setName("jLabel118"); // NOI18N
        FormInput.add(jLabel118);
        jLabel118.setBounds(210, 420, 70, 23);

        cmbKU4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Mengecil", "Membesar" }));
        cmbKU4.setName("cmbKU4"); // NOI18N
        cmbKU4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKU4KeyPressed(evt);
            }
        });
        FormInput.add(cmbKU4);
        cmbKU4.setBounds(80, 420, 120, 23);

        jLabel57.setText("REN DEXTRA");
        jLabel57.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel57.setName("jLabel57"); // NOI18N
        FormInput.add(jLabel57);
        jLabel57.setBounds(20, 390, 80, 23);

        cmbKesadaran40.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran40.setName("cmbKesadaran40"); // NOI18N
        cmbKesadaran40.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran40KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran40);
        cmbKesadaran40.setBounds(500, 540, 120, 23);

        jLabel128.setText("Batu:");
        jLabel128.setName("jLabel128"); // NOI18N
        FormInput.add(jLabel128);
        jLabel128.setBounds(420, 540, 70, 23);

        cmbKesadaran42.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Menebal", "Tidak menebal" }));
        cmbKesadaran42.setName("cmbKesadaran42"); // NOI18N
        cmbKesadaran42.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran42KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran42);
        cmbKesadaran42.setBounds(310, 540, 120, 23);

        jLabel130.setText("Dinding mukosa:");
        jLabel130.setName("jLabel130"); // NOI18N
        FormInput.add(jLabel130);
        jLabel130.setBounds(210, 540, 90, 23);

        cmbKU6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Irregular" }));
        cmbKU6.setName("cmbKU6"); // NOI18N
        cmbKU6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKU6KeyPressed(evt);
            }
        });
        FormInput.add(cmbKU6);
        cmbKU6.setBounds(80, 540, 120, 23);

        jLabel59.setText("VESICA URINARIA");
        jLabel59.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel59.setName("jLabel59"); // NOI18N
        FormInput.add(jLabel59);
        jLabel59.setBounds(20, 510, 100, 23);

        jLabel132.setText("Bentuk:");
        jLabel132.setName("jLabel132"); // NOI18N
        FormInput.add(jLabel132);
        jLabel132.setBounds(20, 210, 40, 23);

        jLabel99.setText("Bentuk:");
        jLabel99.setName("jLabel99"); // NOI18N
        FormInput.add(jLabel99);
        jLabel99.setBounds(20, 270, 40, 23);

        jLabel103.setText("Bentuk:");
        jLabel103.setName("jLabel103"); // NOI18N
        FormInput.add(jLabel103);
        jLabel103.setBounds(30, 360, 40, 23);

        jLabel105.setText("Bentuk:");
        jLabel105.setName("jLabel105"); // NOI18N
        FormInput.add(jLabel105);
        jLabel105.setBounds(30, 540, 40, 23);

        cmbKesadaran44.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "+" }));
        cmbKesadaran44.setName("cmbKesadaran44"); // NOI18N
        cmbKesadaran44.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran44KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran44);
        cmbKesadaran44.setBounds(710, 480, 120, 23);

        jLabel133.setText("Batu:");
        jLabel133.setName("jLabel133"); // NOI18N
        FormInput.add(jLabel133);
        jLabel133.setBounds(630, 480, 70, 23);

        cmbKesadaran45.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Melebar" }));
        cmbKesadaran45.setName("cmbKesadaran45"); // NOI18N
        cmbKesadaran45.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran45KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran45);
        cmbKesadaran45.setBounds(500, 480, 120, 23);

        jLabel134.setText("P.C.S");
        jLabel134.setName("jLabel134"); // NOI18N
        FormInput.add(jLabel134);
        jLabel134.setBounds(420, 480, 70, 23);

        cmbKesadaran46.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Hypoechoic", "Hyperechoic" }));
        cmbKesadaran46.setName("cmbKesadaran46"); // NOI18N
        cmbKesadaran46.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesadaran46KeyPressed(evt);
            }
        });
        FormInput.add(cmbKesadaran46);
        cmbKesadaran46.setBounds(290, 480, 120, 23);

        jLabel135.setText("Cortex:");
        jLabel135.setName("jLabel135"); // NOI18N
        FormInput.add(jLabel135);
        jLabel135.setBounds(210, 480, 70, 23);

        cmbKU7.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Normal", "Mengecil", "Membesar" }));
        cmbKU7.setName("cmbKU7"); // NOI18N
        cmbKU7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKU7KeyPressed(evt);
            }
        });
        FormInput.add(cmbKU7);
        cmbKU7.setBounds(80, 480, 120, 23);

        jLabel60.setText("REN SINISTRA");
        jLabel60.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel60.setName("jLabel60"); // NOI18N
        FormInput.add(jLabel60);
        jLabel60.setBounds(20, 450, 90, 23);

        jLabel108.setText("Bentuk:");
        jLabel108.setName("jLabel108"); // NOI18N
        FormInput.add(jLabel108);
        jLabel108.setBounds(30, 480, 40, 23);

        scrollInput.setViewportView(FormInput);

        internalFrame2.add(scrollInput, java.awt.BorderLayout.CENTER);

        TabRawat.addTab("Input Penilaian", internalFrame2);

        internalFrame3.setBorder(null);
        internalFrame3.setName("internalFrame3"); // NOI18N
        internalFrame3.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tbObatKeyPressed(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame3.add(Scroll, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl.Asuhan :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "25-07-2021" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "25-07-2021" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(80, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(195, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(60, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(LCount);

        internalFrame3.add(panelGlass9, java.awt.BorderLayout.PAGE_END);

        TabRawat.addTab("Data Penilaian", internalFrame3);

        internalFrame1.add(TabRawat, java.awt.BorderLayout.CENTER);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(Kesan.getText().trim().equals("")){
            Valid.textKosong(Kesan,"Rencana Penunjang");
        
        }else{
               if(Sequel.menyimpantf("asesmen_awal_medis","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Rawat",21,new String[]{
                    TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),
                    Kesan.getText(),Kddokter.getText()
                 })==true){
                 emptTeks();
            }
        }
    
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
         //   Valid.pindah(evt,,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        if(tbObat.getSelectedRow()>-1){
            if(Sequel.queryu2tf("delete from asesmen_awal_medis where no_rawat=?",1,new String[]{
                tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
            })==true){
                tampil();
                emptTeks();
            }else{
                JOptionPane.showMessageDialog(null,"Gagal menghapus..!!");
            }
        }else{
            JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
        }            
            
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(TNoRM.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"Nama Pasien");
        }else if(Kesan.getText().trim().equals("")){
            Valid.textKosong(Kesan,"Rencana Penunjang");
        }else{
            if(tbObat.getSelectedRow()>-1){
                if(Sequel.mengedittf("asesmen_awal_medis","no_rawat=?","no_rawat=?,tanggal=?,keluhan_utama=?,rps=?,rpk=?,rpd=?,keadaanumum=?,kesadaran=?,"+
                    "gcs=?,td=?,nadi=?,suhu=?,rr=?,pemeriksaanfisik=?,pemeriksaanpenunjang=?,obat=?,diagnosakerja=?,diagnosabanding=?,rencanapenunjang=?,"+
                    "rencanaterapi=?,nip=?",21,new String[]{
                    TNoRw.getText(),Valid.SetTgl(TglAsuhan.getSelectedItem()+"")+" "+TglAsuhan.getSelectedItem().toString().substring(11,19),
                    Kesan.getText(),Kddokter.getText()
                    })==true){
                        tampil();
                        emptTeks();
                        TabRawat.setSelectedIndex(1);
                }
            }else{
                JOptionPane.showMessageDialog(rootPane,"Silahkan anda pilih data terlebih dahulu..!!");
            }   
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnKeluarActionPerformed(null);
        }else{Valid.pindah(evt,BtnEdit,TCari);}
    
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed

}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            TCari.setText("");
            tampil();
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked

}//GEN-LAST:event_tbObatMouseClicked

    private void tbObatKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyPressed
 
}//GEN-LAST:event_tbObatKeyPressed

    private void TabRawatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabRawatMouseClicked
        if(TabRawat.getSelectedIndex()==1){
            tampil();
        }
    }//GEN-LAST:event_TabRawatMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        tampilMasalah();
    }//GEN-LAST:event_formWindowOpened

    private void BtnTambahMasalahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahMasalahActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        MasterMasalahKeperawatan form=new MasterMasalahKeperawatan(null,false);
        form.isCek();
        form.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        form.setLocationRelativeTo(internalFrame1);
        form.setVisible(true);
        this.setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_BtnTambahMasalahActionPerformed

    private void BtnCariPemeriksaan1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariPemeriksaan1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnCariPemeriksaan1KeyPressed

    private void BtnCariPemeriksaan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariPemeriksaan1ActionPerformed
        tampilMasalah();
    }//GEN-LAST:event_BtnCariPemeriksaan1ActionPerformed

    private void TCariMasalahKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariMasalahKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            tampilMasalah();
            //   }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            //     Rencana.requestFocus();
            //  }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            //    KetDokter.requestFocus();
        }
    }//GEN-LAST:event_TCariMasalahKeyPressed

    private void TglAsuhanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TglAsuhanKeyPressed
        //    Valid.pindah(evt,Rencana,Informasi);
    }//GEN-LAST:event_TglAsuhanKeyPressed

    private void cmbKesadaranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaranKeyPressed
        //    Valid.pindah(evt,KetProthesa,StatusPsiko);
    }//GEN-LAST:event_cmbKesadaranKeyPressed

    private void cmbKUKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKUKeyPressed
        //     Valid.pindah(evt,KetBantu,KetProthesa);
    }//GEN-LAST:event_cmbKUKeyPressed

    private void BtnDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnDokterKeyPressed
        //Valid.pindah(evt,Monitoring,BtnSimpan);
    }//GEN-LAST:event_BtnDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        dokter.isCek();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setAlwaysOnTop(false);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void KddokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KddokterKeyPressed

    }//GEN-LAST:event_KddokterKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
        }else{
            Valid.pindah(evt,TCari,BtnDokter);
        }
    }//GEN-LAST:event_TNoRwKeyPressed

    private void MnCetakAsesmenAwalMedisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakAsesmenAwalMedisActionPerformed
       if(TPasien.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan anda pilih dulu pasien...!!!");
        }else{
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                Map<String, Object> param = new HashMap<>();
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());  
                param.put("logo",Sequel.cariGambar("select logo from setting")); 
                kamar=Sequel.cariIsi("select ifnull(kd_kamar,'') from kamar_inap where no_rawat=? order by tgl_masuk desc limit 1",TNoRw.getText());
                    namakamar=kamar+", "+Sequel.cariIsi("select nm_bangsal from bangsal inner join kamar on bangsal.kd_bangsal=kamar.kd_bangsal "+
                            " where kamar.kd_kamar=? ",kamar);            
                    kamar="Kamar";  
                param.put("kamar",kamar);
                Valid.MyReportqry("AsesmenAwalMedis.jasper","report","::[ Asesmen Awal Medis (Dokter) ]::",
                              "select reg_periksa.no_rawat,DATE_FORMAT(reg_periksa.tgl_registrasi,'%d-%m-%Y')as tgl_registrasi,dokter.nm_dokter,DATE_FORMAT(pasien.tgl_lahir,'%d-%m-%Y')as tgl_lahir,pasien.jk," +
                              "pasien.no_rkm_medis,pasien.nm_pasien,concat(reg_periksa.umurdaftar,' ',reg_periksa.sttsumur)as umur,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat," +
                              "asesmen_awal_medis.tanggal,asesmen_awal_medis.keluhan_utama,asesmen_awal_medis.rps,"+
                              "asesmen_awal_medis.rpk,asesmen_awal_medis.rpd,asesmen_awal_medis.keadaanumum,asesmen_awal_medis.kesadaran,"+
                              "asesmen_awal_medis.gcs,asesmen_awal_medis.td,asesmen_awal_medis.nadi,asesmen_awal_medis.suhu,"+
                              "asesmen_awal_medis.rr,asesmen_awal_medis.pemeriksaanfisik,asesmen_awal_medis.pemeriksaanpenunjang,"+
                              "asesmen_awal_medis.obat,asesmen_awal_medis.diagnosakerja,asesmen_awal_medis.diagnosabanding,"+
                              "asesmen_awal_medis.rencanapenunjang,asesmen_awal_medis.rencanaterapi,asesmen_awal_medis.nip " +
                              "from asesmen_awal_medis inner join reg_periksa inner join pasien inner join dokter inner join kelurahan inner join kecamatan inner join kabupaten" +
                              "on reg_periksa.no_rkm_medis=pasien.no_rkm_medis and asesmen_awal_medis.nip=dokter.kd_dokter and pasien.kd_kel=kelurahan.kd_kel "+
                              "and asesmen_awal_medis.no_rawat=reg_periksa.no_rawat and pasien.kd_kec=kecamatan.kd_kec and pasien.kd_kab=kabupaten.kd_kab "+
                              "and asesmen_awal_medis.nip=dokter.kd_dokter and reg_periksa.no_rkm_medis=pasien.no_rkm_medis where asesmen_awal_medis.no_rawat='"+TNoRw.getText()+"' ",param);
                this.setCursor(Cursor.getDefaultCursor());  
       }
    }//GEN-LAST:event_MnCetakAsesmenAwalMedisActionPerformed

    private void cmbKesadaran1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran1KeyPressed

    private void cmbKesadaran2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran2KeyPressed

    private void cmbKesadaran3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran3KeyPressed

    private void cmbKesadaran4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran4KeyPressed

    private void cmbKesadaran5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran5KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran5KeyPressed

    private void cmbKesadaran6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran6KeyPressed

    private void KesanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KesanKeyPressed

    private void cmbKesadaran10KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran10KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran10KeyPressed

    private void cmbKesadaran12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran12KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran12KeyPressed

    private void cmbKU1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKU1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKU1KeyPressed

    private void cmbKesadaran8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran8KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran8KeyPressed

    private void cmbKesadaran9KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran9KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran9KeyPressed

    private void cmbKesadaran14KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran14KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran14KeyPressed

    private void cmbKU2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKU2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKU2KeyPressed

    private void cmbKesadaran15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran15KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran15KeyPressed

    private void cmbKesadaran18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran18KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran18KeyPressed

    private void cmbKesadaran21KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran21KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran21KeyPressed

    private void cmbKU3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKU3KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKU3KeyPressed

    private void cmbKesadaran24KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran24KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran24KeyPressed

    private void cmbKesadaran26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran26KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran26KeyPressed

    private void cmbKesadaran28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran28KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran28KeyPressed

    private void cmbKU4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKU4KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKU4KeyPressed

    private void cmbKesadaran40KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran40KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran40KeyPressed

    private void cmbKesadaran42KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran42KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran42KeyPressed

    private void cmbKU6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKU6KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKU6KeyPressed

    private void cmbKesadaran44KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran44KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran44KeyPressed

    private void cmbKesadaran45KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran45KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran45KeyPressed

    private void cmbKesadaran46KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesadaran46KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKesadaran46KeyPressed

    private void cmbKU7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKU7KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbKU7KeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            RMUsgAbdomen dialog = new RMUsgAbdomen(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnCariPemeriksaan1;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.Button BtnTambahMasalah;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.TextBox Jk;
    private widget.TextBox Kddokter;
    private widget.TextArea Kesan;
    private widget.Label LCount;
    private widget.editorpane LoadHTML;
    private javax.swing.JMenuItem MnCetakAsesmenAwalMedis;
    private widget.TextBox NmDokter;
    private widget.ScrollPane Scroll;
    private widget.TextBox TCari;
    private widget.TextBox TCariMasalah;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private javax.swing.JTabbedPane TabRawat;
    private widget.Tanggal TglAsuhan;
    private widget.TextBox TglLahir;
    private widget.ComboBox cmbKU;
    private widget.ComboBox cmbKU1;
    private widget.ComboBox cmbKU2;
    private widget.ComboBox cmbKU3;
    private widget.ComboBox cmbKU4;
    private widget.ComboBox cmbKU6;
    private widget.ComboBox cmbKU7;
    private widget.ComboBox cmbKesadaran;
    private widget.ComboBox cmbKesadaran1;
    private widget.ComboBox cmbKesadaran10;
    private widget.ComboBox cmbKesadaran12;
    private widget.ComboBox cmbKesadaran14;
    private widget.ComboBox cmbKesadaran15;
    private widget.ComboBox cmbKesadaran18;
    private widget.ComboBox cmbKesadaran2;
    private widget.ComboBox cmbKesadaran21;
    private widget.ComboBox cmbKesadaran24;
    private widget.ComboBox cmbKesadaran26;
    private widget.ComboBox cmbKesadaran28;
    private widget.ComboBox cmbKesadaran3;
    private widget.ComboBox cmbKesadaran4;
    private widget.ComboBox cmbKesadaran40;
    private widget.ComboBox cmbKesadaran42;
    private widget.ComboBox cmbKesadaran44;
    private widget.ComboBox cmbKesadaran45;
    private widget.ComboBox cmbKesadaran46;
    private widget.ComboBox cmbKesadaran5;
    private widget.ComboBox cmbKesadaran6;
    private widget.ComboBox cmbKesadaran8;
    private widget.ComboBox cmbKesadaran9;
    private widget.ScrollPane ddgag;
    private widget.InternalFrame internalFrame1;
    private widget.InternalFrame internalFrame2;
    private widget.InternalFrame internalFrame3;
    private widget.Label jLabel10;
    private widget.Label jLabel100;
    private widget.Label jLabel101;
    private widget.Label jLabel102;
    private widget.Label jLabel103;
    private widget.Label jLabel104;
    private widget.Label jLabel105;
    private widget.Label jLabel106;
    private widget.Label jLabel107;
    private widget.Label jLabel108;
    private widget.Label jLabel109;
    private widget.Label jLabel11;
    private widget.Label jLabel112;
    private widget.Label jLabel114;
    private widget.Label jLabel116;
    private widget.Label jLabel118;
    private widget.Label jLabel128;
    private widget.Label jLabel13;
    private widget.Label jLabel130;
    private widget.Label jLabel132;
    private widget.Label jLabel133;
    private widget.Label jLabel134;
    private widget.Label jLabel135;
    private widget.Label jLabel19;
    private widget.Label jLabel21;
    private widget.Label jLabel53;
    private widget.Label jLabel54;
    private widget.Label jLabel55;
    private widget.Label jLabel56;
    private widget.Label jLabel57;
    private widget.Label jLabel59;
    private widget.Label jLabel6;
    private widget.Label jLabel60;
    private widget.Label jLabel7;
    private widget.Label jLabel8;
    private widget.Label jLabel93;
    private widget.Label jLabel94;
    private widget.Label jLabel95;
    private widget.Label jLabel96;
    private widget.Label jLabel97;
    private widget.Label jLabel98;
    private widget.Label jLabel99;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private widget.Label label11;
    private widget.Label label12;
    private widget.Label label14;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollInput;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables
 
    private void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            if(TCari.getText().equals("")){
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,"+
                        "asesmen_awal_medis.no_rawat,asesmen_awal_medis.tanggal,asesmen_awal_medis.keluhan_utama,asesmen_awal_medis.rps,asesmen_awal_medis.rpk,asesmen_awal_medis.rpd,"+
                        "asesmen_awal_medis.keadaanumum,asesmen_awal_medis.kesadaran,asesmen_awal_medis.gcs,asesmen_awal_medis.td,asesmen_awal_medis.nadi,asesmen_awal_medis.suhu,"+
                        "asesmen_awal_medis.rpd,asesmen_awal_medis.rpk,asesmen_awal_medis.rr,asesmen_awal_medis.pemeriksaanfisik,asesmen_awal_medis.pemeriksaanpenunjang,asesmen_awal_medis.obat,asesmen_awal_medis.diagnosakerja,"+
                        "asesmen_awal_medis.diagnosabanding,asesmen_awal_medis.rencanapenunjang,asesmen_awal_medis.rencanaterapi,asesmen_awal_medis.nip,dokter.nm_dokter "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join asesmen_awal_medis on reg_periksa.no_rawat=asesmen_awal_medis.no_rawat "+
                        "inner join dokter on asesmen_awal_medis.nip=dokter.kd_dokter where "+
                        "asesmen_awal_medis.tanggal between ? and ? order by asesmen_awal_medis.tanggal");
            }else{
                ps=koneksi.prepareStatement(
                        "select reg_periksa.no_rawat,pasien.no_rkm_medis,pasien.nm_pasien,if(pasien.jk='L','Laki-Laki','Perempuan') as jk,pasien.tgl_lahir,"+
                        "asesmen_awal_medis.no_rawat,asesmen_awal_medis.tanggal,asesmen_awal_medis.keluhan_utama,asesmen_awal_medis.rps,asesmen_awal_medis.rpk,asesmen_awal_medis.rpd,"+
                        "asesmen_awal_medis.keadaanumum,asesmen_awal_medis.kesadaran,asesmen_awal_medis.gcs,asesmen_awal_medis.td,asesmen_awal_medis.nadi,asesmen_awal_medis.suhu,"+
                        "asesmen_awal_medis.rpd,asesmen_awal_medis.rpk,asesmen_awal_medis.rr,asesmen_awal_medis.pemeriksaanfisik,asesmen_awal_medis.pemeriksaanpenunjang,asesmen_awal_medis.obat,asesmen_awal_medis.diagnosakerja,"+
                        "asesmen_awal_medis.diagnosabanding,asesmen_awal_medis.rencanapenunjang,asesmen_awal_medis.rencanaterapi,asesmen_awal_medis.nip,dokter.nm_dokter "+
                        "from reg_periksa inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                        "inner join asesmen_awal_medis on reg_periksa.no_rawat=asesmen_awal_medis.no_rawat "+
                        "inner join dokter on asesmen_awal_medis.nip=dokter.kd_dokter where "+
                        "asesmen_awal_medis.tanggal between ? and ? and reg_periksa.no_rawat like ? or "+
                        "asesmen_awal_medis.tanggal between ? and ? and pasien.no_rkm_medis like ? or "+
                        "asesmen_awal_medis.tanggal between ? and ? and pasien.nm_pasien like ? or "+
                        "asesmen_awal_medis.tanggal between ? and ? and asesmen_awal_medis.nip like ? or "+
                        "asesmen_awal_medis.tanggal between ? and ? and dokter.nm_dokter like ? order by asesmen_awal_medis.tanggal");
            }
                
            try {
                if(TCari.getText().equals("")){
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                }else{
                    ps.setString(1,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(2,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(3,"%"+TCari.getText()+"%");
                    ps.setString(4,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(5,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(6,"%"+TCari.getText()+"%");
                    ps.setString(7,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(8,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(9,"%"+TCari.getText()+"%");
                    ps.setString(10,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(11,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(12,"%"+TCari.getText()+"%");
                    ps.setString(13,Valid.SetTgl(DTPCari1.getSelectedItem()+"")+" 00:00:00");
                    ps.setString(14,Valid.SetTgl(DTPCari2.getSelectedItem()+"")+" 23:59:59");
                    ps.setString(15,"%"+TCari.getText()+"%");
                }   
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString("no_rawat"),rs.getString("no_rkm_medis"),rs.getString("nm_pasien"),rs.getString("tanggal"),rs.getString("keluhan_utama"),rs.getString("rps"),rs.getString("rpk"),rs.getString("rpd"),
                        rs.getString("keadaanumum"),rs.getString("kesadaran"),rs.getString("gcs"),rs.getString("td"),rs.getString("nadi"),rs.getString("suhu"),rs.getString("rr"),
                        rs.getString("pemeriksaanfisik"),rs.getString("pemeriksaanpenunjang"),rs.getString("obat"),rs.getString("diagnosakerja"),rs.getString("diagnosabanding"),rs.getString("rencanapenunjang"),rs.getString("rencanaterapi"),
                        rs.getString("nip"),rs.getString("nm_dokter")
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
            
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TglAsuhan.setDate(new Date());
        TNoRw.setText("");
        TNoRM.setText("");
        TPasien.setText("");   
        Jk.setText("");
        TglLahir.setText("");
        Kesan.setText("");
        cmbKU.setSelectedIndex(0);
        cmbKesadaran.setSelectedIndex(0);
        TabRawat.setSelectedIndex(0);
      //  Informasi.requestFocus();
    } 

    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()); 
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString()); 
            Valid.SetTgl2(TglAsuhan,tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            cmbKU.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString());
            cmbKesadaran.setSelectedItem(tbObat.getValueAt(tbObat.getSelectedRow(),9).toString());
            Kesan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),20).toString()); 
            Kddokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),22).toString());
            NmDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),23).toString());
            
            try {
                    ps.setString(1,tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
                    rs=ps.executeQuery();
                } catch (Exception e) {
                    System.out.println("Notif : "+e);
                }
        }
    }
    private void isRawat() {
        Sequel.cariIsi("select no_rkm_medis from reg_periksa where no_rawat=? ",TNoRM,TNoRw.getText());
        try {
            ps=koneksi.prepareStatement(
                    "select nm_pasien, if(jk='L','Laki-Laki','Perempuan') as jk,tgl_lahir,agama,bahasa_pasien.nama_bahasa,cacat_fisik.nama_cacat "+
                    "from pasien inner join bahasa_pasien on bahasa_pasien.id=pasien.bahasa_pasien "+
                    "inner join cacat_fisik on cacat_fisik.id=pasien.cacat_fisik "+
                    "where no_rkm_medis=?");
            try {
                ps.setString(1,TNoRM.getText());
                rs=ps.executeQuery();
                if(rs.next()){
                    TPasien.setText(rs.getString("nm_pasien"));
                    Jk.setText(rs.getString("jk"));
                    TglLahir.setText(rs.getString("tgl_lahir"));
                //  CacatFisik.setText(rs.getString("nama_cacat"));
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Notif : "+e);
        }
    }
    
    public void setNoRm(String norwt) {
        TNoRw.setText(norwt);
        TCari.setText(norwt);
        Sequel.cariIsi("select tgl_registrasi from reg_periksa where no_rawat='"+norwt+"'", DTPCari1);
       // DTPCari2.setDate(tgl2);    
        isRawat(); 
    }
    
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnHapus.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnEdit.setEnabled(akses.getpenilaian_awal_keperawatan_ralan());
        BtnTambahMasalah.setEnabled(akses.getmaster_masalah_keperawatan());  
        if(akses.getjml2()>=1){
            Kddokter.setEditable(false);
            BtnDokter.setEnabled(false);
            Kddokter.setText(akses.getkode());
            Sequel.cariIsi("select nama from petugas where nip=?", NmDokter,Kddokter.getText());
            if(NmDokter.getText().equals("")){
                Kddokter.setText("");
                JOptionPane.showMessageDialog(null,"User login bukan petugas...!!");
            }
        }            
    }

    public void setTampil(){
       TabRawat.setSelectedIndex(1);
       tampil();
    }
    
    private void tampilMasalah() {
     /*   try{
            jml=0;
            for(i=0;i<tbMasalahKeperawatan.getRowCount();i++){
                if(tbMasalahKeperawatan.getValueAt(i,0).toString().equals("true")){
                    jml++;
                }
            }

            pilih=null;
            pilih=new boolean[jml]; 
            kode=null;
            kode=new String[jml];
            masalah=null;
            masalah=new String[jml];

            index=0;        
            for(i=0;i<tbMasalahKeperawatan.getRowCount();i++){
                if(tbMasalahKeperawatan.getValueAt(i,0).toString().equals("true")){
                    pilih[index]=true;
                    kode[index]=tbMasalahKeperawatan.getValueAt(i,1).toString();
                    masalah[index]=tbMasalahKeperawatan.getValueAt(i,2).toString();
                    index++;
                }
            } 

            Valid.tabelKosong(tabModeMasalah);

            for(i=0;i<jml;i++){
                tabModeMasalah.addRow(new Object[] {
                    pilih[i],kode[i],masalah[i]
                });
            }
            ps=koneksi.prepareStatement("select * from master_masalah_keperawatan where kode_masalah like ? or nama_masalah like ? order by kode_masalah");
            try {
                ps.setString(1,"%"+TCariMasalah.getText().trim()+"%");
                ps.setString(2,"%"+TCariMasalah.getText().trim()+"%");
                rs=ps.executeQuery();
                while(rs.next()){
                    tabModeMasalah.addRow(new Object[]{false,rs.getString(1),rs.getString(2)});
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        } */
    } 
    


    
    
   
}
