/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * kontribusi dari dokter Salim Mulyana
 */

package surat;

import fungsi.WarnaTable;
import fungsi.batasInput;
import fungsi.koneksiDB;
import fungsi.sekuel;
import fungsi.validasi;
import fungsi.akses;
import fungsi.validasi1;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import kepegawaian.DlgCariDokter;
import kepegawaian.DlgCariDokter2;
import laporan.DlgDiagnosaPenyakit;
import simrskhanza.DlgPerusahaan;


/**
 * 
 * @author salimmulyana
 */
public final class SuratKeteranganKesehatanJiwa extends javax.swing.JDialog {
    private final DefaultTableModel tabMode;
    private Connection koneksi=koneksiDB.condb();
    public  DlgCariDokter dokter=new DlgCariDokter(null,false);
    public  DlgCariDokter2 dokter2=new DlgCariDokter2(null,false);
    public  DlgDiagnosaPenyakit diagnosa=new DlgDiagnosaPenyakit(null,false);
    public  DlgPerusahaan perusahaan=new DlgPerusahaan(null,false);
    private sekuel Sequel=new sekuel();
    private validasi Valid=new validasi();
    private validasi1 Valid1=new validasi1();
    private PreparedStatement ps;
    private ResultSet rs;
    private int i=0;
    private String tgl,finger,bln_angka = "", bln_romawi = "";
    /** Creates new form DlgRujuk
     * @param parent
     * @param modal */
    public SuratKeteranganKesehatanJiwa(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocation(8,1);
        setSize(628,674);
        
        tabMode=new DefaultTableModel(null,new Object[]{
            "No Rawat","No Surat","Tanggal Periksa","Kode Dokter","Nama Dokter", "Wawancara Psikiatri",
            "Sikap Terhadap Test","Potensi Kerja","Kemampuan Adaptasi","Kendala Psikologis", "Perilaku berisiko",
            "Integritas Moral","Index Kapasitas Mental","Profil Klinis","Keterbukaan Pikiran","Keterbukaan Hari",
            "Keterbukaan Terhadap Orang Lain","Keterbukaan terhadap Kesepakatan","Keterbukaan Terhadap Tekanan",
            "Index OCEAN","Kesimpulan","Keperluan"
        }){
              @Override public boolean isCellEditable(int rowIndex, int colIndex){return false;}
        };
        tbObat.setModel(tabMode);

        //tbObat.setDefaultRenderer(Object.class, new WarnaTable(panelJudul.getBackground(),tbObat.getBackground()));
        tbObat.setPreferredScrollableViewportSize(new Dimension(500,500));
        tbObat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        for (i = 0; i < 20; i++) {
            TableColumn column = tbObat.getColumnModel().getColumn(i);
            if(i==0){
                column.setPreferredWidth(105);
            }else if(i==1){
                column.setPreferredWidth(105);
            }else if(i==2){
                column.setPreferredWidth(70);
            }else if(i==3){
                column.setPreferredWidth(170);
            }else if(i==4){
                column.setPreferredWidth(90);
            }else if(i==5){
                column.setPreferredWidth(90);
            }else if(i==6){
                column.setPreferredWidth(100);
            }else if(i==7){
                column.setPreferredWidth(90);
            }else if(i==8){
                column.setPreferredWidth(100);
            }else if(i==9){
                column.setPreferredWidth(100); 
            }else if(i==10){
                column.setPreferredWidth(50);  
            }else if(i==11){
                column.setPreferredWidth(105);
            }else if(i==12){
                column.setPreferredWidth(70);
            }else if(i==13){
                column.setPreferredWidth(170);
            }else if(i==14){
                column.setPreferredWidth(90);
            }else if(i==15){
                column.setPreferredWidth(90);
            }else if(i==16){
                column.setPreferredWidth(100);
            }else if(i==17){
                column.setPreferredWidth(90);
            }else if(i==18){
                column.setPreferredWidth(100);
            }else if(i==19){
                column.setPreferredWidth(100); 
            }else if(i==20){
                column.setPreferredWidth(50); 
            }
        }
        tbObat.setDefaultRenderer(Object.class, new WarnaTable());
        
        NoSurat.setDocument(new batasInput((byte)17).getKata(NoSurat));
        TNoRw.setDocument(new batasInput((byte)17).getKata(TNoRw));  
        WawancaraPrikiatri.setDocument(new batasInput((byte)20).getKata(WawancaraPrikiatri));         
        TCari.setDocument(new batasInput((byte)100).getKata(TCari));  
        KdDokter.setDocument(new batasInput((byte)50).getKata(KdDokter));
        TDokter.setDocument(new batasInput((byte)100).getKata(TDokter));
        PotensiKerja.setDocument(new batasInput((byte)1).getOnlyAngka(PotensiKerja));
        KemampuanAdaptasi.setDocument(new batasInput((byte)1).getOnlyAngka(KemampuanAdaptasi));
        KendalaPsikologis.setDocument(new batasInput((byte)1).getOnlyAngka(KendalaPsikologis));
        PerilakuBeresiko.setDocument(new batasInput((byte)1).getOnlyAngka(PerilakuBeresiko));
        IntegrasiMoral.setDocument(new batasInput((byte)1).getOnlyAngka(IntegrasiMoral));
        IndexKapasitasMental.setDocument(new batasInput((byte)1).getOnlyAngka(IndexKapasitasMental));
        KeterbukaanPikiran.setDocument(new batasInput((byte)1).getOnlyAngka(KeterbukaanPikiran));
        PotensiKerja.setDocument(new batasInput((byte)1).getOnlyAngka(PotensiKerja));
        PotensiKerja.setDocument(new batasInput((byte)1).getOnlyAngka(PotensiKerja));
        PotensiKerja.setDocument(new batasInput((byte)1).getOnlyAngka(PotensiKerja));
        PotensiKerja.setDocument(new batasInput((byte)1).getOnlyAngka(PotensiKerja));
        PotensiKerja.setDocument(new batasInput((byte)1).getOnlyAngka(PotensiKerja));
        
        if(koneksiDB.CARICEPAT().equals("aktif")){
            TCari.getDocument().addDocumentListener(new javax.swing.event.DocumentListener(){
                @Override
                public void insertUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void removeUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
                @Override
                public void changedUpdate(DocumentEvent e) {
                    if(TCari.getText().length()>2){
                        tampil();
                    }
                }
            });
        }
        ChkInput.setSelected(false);
        isForm();
        
        perusahaan.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(akses.getform().equals("SuratSakit")){
                    if(perusahaan.getTable().getSelectedRow()!= -1){
                        kdperusahaan.setText(perusahaan.getTable().getValueAt(perusahaan.getTable().getSelectedRow(),0).toString());
                        ;
                    }  
                    kdperusahaan.requestFocus();
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
        
        perusahaan.getTable().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}
            @Override
            public void keyPressed(KeyEvent e) {
                if(akses.getform().equals("SuratSakit")){
                    if(e.getKeyCode()==KeyEvent.VK_SPACE){
                        perusahaan.dispose();
                    }                
                }
            }
            @Override
            public void keyReleased(KeyEvent e) {}
        });
    
        
        dokter.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {;}
            @Override
            public void windowClosing(WindowEvent e) {}
            @Override
            public void windowClosed(WindowEvent e) {
                if(akses.getform().equals("SuratSakit")){
                    if(dokter.getTable().getSelectedRow()!= -1){    
                            KdDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),0).toString());
                            TDokter.setText(dokter.getTable().getValueAt(dokter.getTable().getSelectedRow(),1).toString());
                            KdDokter.requestFocus();
                    }                
                }
            }
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
              
    }
       
        

    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MnCetakKesehatanJiwa = new javax.swing.JMenuItem();
        kdperusahaan = new widget.TextBox();
        internalFrame1 = new widget.InternalFrame();
        Scroll = new widget.ScrollPane();
        tbObat = new widget.Table();
        jPanel3 = new javax.swing.JPanel();
        panelGlass8 = new widget.panelisi();
        BtnSimpan = new widget.Button();
        BtnBatal = new widget.Button();
        BtnHapus = new widget.Button();
        BtnEdit = new widget.Button();
        BtnPrint = new widget.Button();
        BtnAll = new widget.Button();
        BtnKeluar = new widget.Button();
        panelGlass9 = new widget.panelisi();
        jLabel19 = new widget.Label();
        DTPCari1 = new widget.Tanggal();
        jLabel21 = new widget.Label();
        DTPCari2 = new widget.Tanggal();
        jLabel6 = new widget.Label();
        TCari = new widget.TextBox();
        BtnCari = new widget.Button();
        jLabel7 = new widget.Label();
        LCount = new widget.Label();
        PanelInput = new javax.swing.JPanel();
        ChkInput = new widget.CekBox();
        Scroll1 = new widget.ScrollPane();
        FormInput = new widget.PanelBiasa();
        jLabel3 = new widget.Label();
        NoSurat = new widget.TextBox();
        jLabel4 = new widget.Label();
        TNoRw = new widget.TextBox();
        TPasien = new widget.TextBox();
        TNoRM = new widget.TextBox();
        WawancaraPrikiatri = new widget.TextBox();
        jLabel18 = new widget.Label();
        TanggalPeriksa = new widget.Tanggal();
        jLabel13 = new widget.Label();
        NoSurat1 = new widget.TextBox();
        KdDokter = new widget.TextBox();
        TDokter = new widget.TextBox();
        BtnDokter = new widget.Button();
        jLabel15 = new widget.Label();
        jLabel17 = new widget.Label();
        Pekerjaan = new widget.TextBox();
        jLabel22 = new widget.Label();
        jLabel23 = new widget.Label();
        scrollPane1 = new widget.ScrollPane();
        SikapTes = new widget.TextArea();
        jLabel24 = new widget.Label();
        PotensiKerja = new widget.TextBox();
        jLabel25 = new widget.Label();
        jLabel26 = new widget.Label();
        KemampuanAdaptasi = new widget.TextBox();
        jLabel27 = new widget.Label();
        KendalaPsikologis = new widget.TextBox();
        jLabel28 = new widget.Label();
        PerilakuBeresiko = new widget.TextBox();
        jLabel29 = new widget.Label();
        IntegrasiMoral = new widget.TextBox();
        jLabel30 = new widget.Label();
        jLabel31 = new widget.Label();
        jLabel32 = new widget.Label();
        jLabel33 = new widget.Label();
        jLabel34 = new widget.Label();
        jLabel35 = new widget.Label();
        IndexKapasitasMental = new widget.TextBox();
        ProfilKlinis = new widget.TextBox();
        jLabel36 = new widget.Label();
        jLabel20 = new widget.Label();
        KeterbukaanPikiran = new widget.TextBox();
        jLabel37 = new widget.Label();
        jLabel38 = new widget.Label();
        KeterbukaanHati = new widget.TextBox();
        jLabel39 = new widget.Label();
        KeterbukaanOrang = new widget.TextBox();
        jLabel40 = new widget.Label();
        KeterbukaanKesepakatan = new widget.TextBox();
        jLabel41 = new widget.Label();
        KeterbukaanTekanan = new widget.TextBox();
        jLabel42 = new widget.Label();
        jLabel43 = new widget.Label();
        jLabel44 = new widget.Label();
        jLabel45 = new widget.Label();
        jLabel46 = new widget.Label();
        jLabel47 = new widget.Label();
        IndeksOcean = new widget.TextBox();
        jLabel48 = new widget.Label();
        Kesimpulan = new widget.TextBox();
        jLabel49 = new widget.Label();
        Keperluan = new widget.TextBox();

        jPopupMenu1.setName("jPopupMenu1"); // NOI18N

        MnCetakKesehatanJiwa.setBackground(new java.awt.Color(250, 250, 250));
        MnCetakKesehatanJiwa.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        MnCetakKesehatanJiwa.setForeground(new java.awt.Color(50, 50, 50));
        MnCetakKesehatanJiwa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/category.png"))); // NOI18N
        MnCetakKesehatanJiwa.setText("Cetak Surat Kesehatan Jiwa");
        MnCetakKesehatanJiwa.setName("MnCetakKesehatanJiwa"); // NOI18N
        MnCetakKesehatanJiwa.setPreferredSize(new java.awt.Dimension(200, 26));
        MnCetakKesehatanJiwa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MnCetakKesehatanJiwaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MnCetakKesehatanJiwa);

        kdperusahaan.setEditable(false);
        kdperusahaan.setHighlighter(null);
        kdperusahaan.setName("kdperusahaan"); // NOI18N
        kdperusahaan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kdperusahaanKeyPressed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        internalFrame1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(240, 245, 235)), "::[ Data Surat Keterangan Kesehatan Jiwa]::", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(50, 50, 50))); // NOI18N
        internalFrame1.setFont(new java.awt.Font("Tahoma", 2, 12)); // NOI18N
        internalFrame1.setName("internalFrame1"); // NOI18N
        internalFrame1.setLayout(new java.awt.BorderLayout(1, 1));

        Scroll.setName("Scroll"); // NOI18N
        Scroll.setOpaque(true);
        Scroll.setPreferredSize(new java.awt.Dimension(452, 200));

        tbObat.setAutoCreateRowSorter(true);
        tbObat.setToolTipText("Silahkan klik untuk memilih data yang mau diedit ataupun dihapus");
        tbObat.setComponentPopupMenu(jPopupMenu1);
        tbObat.setName("tbObat"); // NOI18N
        tbObat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbObatMouseClicked(evt);
            }
        });
        tbObat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tbObatKeyReleased(evt);
            }
        });
        Scroll.setViewportView(tbObat);

        internalFrame1.add(Scroll, java.awt.BorderLayout.CENTER);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(44, 100));
        jPanel3.setLayout(new java.awt.BorderLayout(1, 1));

        panelGlass8.setName("panelGlass8"); // NOI18N
        panelGlass8.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass8.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        BtnSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/save-16x16.png"))); // NOI18N
        BtnSimpan.setMnemonic('S');
        BtnSimpan.setText("Simpan");
        BtnSimpan.setToolTipText("Alt+S");
        BtnSimpan.setName("BtnSimpan"); // NOI18N
        BtnSimpan.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpanActionPerformed(evt);
            }
        });
        BtnSimpan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnSimpanKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnSimpan);

        BtnBatal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Cancel-2-16x16.png"))); // NOI18N
        BtnBatal.setMnemonic('B');
        BtnBatal.setText("Baru");
        BtnBatal.setToolTipText("Alt+B");
        BtnBatal.setName("BtnBatal"); // NOI18N
        BtnBatal.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBatalActionPerformed(evt);
            }
        });
        BtnBatal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnBatalKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnBatal);

        BtnHapus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/stop_f2.png"))); // NOI18N
        BtnHapus.setMnemonic('H');
        BtnHapus.setText("Hapus");
        BtnHapus.setToolTipText("Alt+H");
        BtnHapus.setName("BtnHapus"); // NOI18N
        BtnHapus.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusActionPerformed(evt);
            }
        });
        BtnHapus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnHapusKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnHapus);

        BtnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/inventaris.png"))); // NOI18N
        BtnEdit.setMnemonic('G');
        BtnEdit.setText("Ganti");
        BtnEdit.setToolTipText("Alt+G");
        BtnEdit.setName("BtnEdit"); // NOI18N
        BtnEdit.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditActionPerformed(evt);
            }
        });
        BtnEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnEditKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnEdit);

        BtnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/b_print.png"))); // NOI18N
        BtnPrint.setMnemonic('T');
        BtnPrint.setText("Cetak");
        BtnPrint.setToolTipText("Alt+T");
        BtnPrint.setName("BtnPrint"); // NOI18N
        BtnPrint.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        BtnPrint.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnPrintKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnPrint);

        BtnAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/Search-16x16.png"))); // NOI18N
        BtnAll.setMnemonic('M');
        BtnAll.setText("Semua");
        BtnAll.setToolTipText("Alt+M");
        BtnAll.setName("BtnAll"); // NOI18N
        BtnAll.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAllActionPerformed(evt);
            }
        });
        BtnAll.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnAllKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnAll);

        BtnKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/exit.png"))); // NOI18N
        BtnKeluar.setMnemonic('K');
        BtnKeluar.setText("Keluar");
        BtnKeluar.setToolTipText("Alt+K");
        BtnKeluar.setName("BtnKeluar"); // NOI18N
        BtnKeluar.setPreferredSize(new java.awt.Dimension(100, 30));
        BtnKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKeluarActionPerformed(evt);
            }
        });
        BtnKeluar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnKeluarKeyPressed(evt);
            }
        });
        panelGlass8.add(BtnKeluar);

        jPanel3.add(panelGlass8, java.awt.BorderLayout.CENTER);

        panelGlass9.setName("panelGlass9"); // NOI18N
        panelGlass9.setPreferredSize(new java.awt.Dimension(44, 44));
        panelGlass9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 9));

        jLabel19.setText("Tgl. Surat :");
        jLabel19.setName("jLabel19"); // NOI18N
        jLabel19.setPreferredSize(new java.awt.Dimension(67, 23));
        panelGlass9.add(jLabel19);

        DTPCari1.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-12-2022" }));
        DTPCari1.setDisplayFormat("dd-MM-yyyy");
        DTPCari1.setName("DTPCari1"); // NOI18N
        DTPCari1.setOpaque(false);
        DTPCari1.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari1);

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("s.d.");
        jLabel21.setName("jLabel21"); // NOI18N
        jLabel21.setPreferredSize(new java.awt.Dimension(23, 23));
        panelGlass9.add(jLabel21);

        DTPCari2.setForeground(new java.awt.Color(50, 70, 50));
        DTPCari2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-12-2022" }));
        DTPCari2.setDisplayFormat("dd-MM-yyyy");
        DTPCari2.setName("DTPCari2"); // NOI18N
        DTPCari2.setOpaque(false);
        DTPCari2.setPreferredSize(new java.awt.Dimension(90, 23));
        panelGlass9.add(DTPCari2);

        jLabel6.setText("Key Word :");
        jLabel6.setName("jLabel6"); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(70, 23));
        panelGlass9.add(jLabel6);

        TCari.setName("TCari"); // NOI18N
        TCari.setPreferredSize(new java.awt.Dimension(205, 23));
        TCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TCariKeyPressed(evt);
            }
        });
        panelGlass9.add(TCari);

        BtnCari.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/accept.png"))); // NOI18N
        BtnCari.setMnemonic('3');
        BtnCari.setToolTipText("Alt+3");
        BtnCari.setName("BtnCari"); // NOI18N
        BtnCari.setPreferredSize(new java.awt.Dimension(28, 23));
        BtnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariActionPerformed(evt);
            }
        });
        BtnCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                BtnCariKeyPressed(evt);
            }
        });
        panelGlass9.add(BtnCari);

        jLabel7.setText("Record :");
        jLabel7.setName("jLabel7"); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(65, 23));
        panelGlass9.add(jLabel7);

        LCount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LCount.setText("0");
        LCount.setName("LCount"); // NOI18N
        LCount.setPreferredSize(new java.awt.Dimension(50, 23));
        panelGlass9.add(LCount);

        jPanel3.add(panelGlass9, java.awt.BorderLayout.PAGE_START);

        internalFrame1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        PanelInput.setName("PanelInput"); // NOI18N
        PanelInput.setOpaque(false);
        PanelInput.setPreferredSize(new java.awt.Dimension(192, 500));
        PanelInput.setRequestFocusEnabled(false);
        PanelInput.setLayout(new java.awt.BorderLayout(1, 1));

        ChkInput.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setMnemonic('I');
        ChkInput.setText(".: Input Data");
        ChkInput.setToolTipText("Alt+I");
        ChkInput.setBorderPainted(true);
        ChkInput.setBorderPaintedFlat(true);
        ChkInput.setFocusable(false);
        ChkInput.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ChkInput.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ChkInput.setName("ChkInput"); // NOI18N
        ChkInput.setPreferredSize(new java.awt.Dimension(192, 20));
        ChkInput.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/143.png"))); // NOI18N
        ChkInput.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/145.png"))); // NOI18N
        ChkInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkInputActionPerformed(evt);
            }
        });
        PanelInput.add(ChkInput, java.awt.BorderLayout.PAGE_END);

        Scroll1.setName("Scroll1"); // NOI18N
        Scroll1.setOpaque(true);
        Scroll1.setPreferredSize(new java.awt.Dimension(452, 200));

        FormInput.setName("FormInput"); // NOI18N
        FormInput.setPreferredSize(new java.awt.Dimension(100, 800));
        FormInput.setLayout(null);

        jLabel3.setText("No. Surat:");
        jLabel3.setName("jLabel3"); // NOI18N
        FormInput.add(jLabel3);
        jLabel3.setBounds(0, 40, 95, 23);

        NoSurat.setHighlighter(null);
        NoSurat.setName("NoSurat"); // NOI18N
        NoSurat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NoSuratKeyPressed(evt);
            }
        });
        FormInput.add(NoSurat);
        NoSurat.setBounds(99, 40, 141, 23);

        jLabel4.setText("No.Rawat :");
        jLabel4.setName("jLabel4"); // NOI18N
        FormInput.add(jLabel4);
        jLabel4.setBounds(0, 10, 95, 23);

        TNoRw.setHighlighter(null);
        TNoRw.setName("TNoRw"); // NOI18N
        TNoRw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRwKeyPressed(evt);
            }
        });
        FormInput.add(TNoRw);
        TNoRw.setBounds(99, 10, 141, 23);

        TPasien.setEditable(false);
        TPasien.setHighlighter(null);
        TPasien.setName("TPasien"); // NOI18N
        TPasien.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TPasienKeyPressed(evt);
            }
        });
        FormInput.add(TPasien);
        TPasien.setBounds(355, 10, 365, 23);

        TNoRM.setEditable(false);
        TNoRM.setHighlighter(null);
        TNoRM.setName("TNoRM"); // NOI18N
        TNoRM.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TNoRMKeyPressed(evt);
            }
        });
        FormInput.add(TNoRM);
        TNoRM.setBounds(242, 10, 111, 23);

        WawancaraPrikiatri.setHighlighter(null);
        WawancaraPrikiatri.setName("WawancaraPrikiatri"); // NOI18N
        WawancaraPrikiatri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                WawancaraPrikiatriActionPerformed(evt);
            }
        });
        WawancaraPrikiatri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                WawancaraPrikiatriKeyPressed(evt);
            }
        });
        FormInput.add(WawancaraPrikiatri);
        WawancaraPrikiatri.setBounds(120, 100, 590, 23);

        jLabel18.setText("b. Indeks Kapasitas Mental");
        jLabel18.setName("jLabel18"); // NOI18N
        FormInput.add(jLabel18);
        jLabel18.setBounds(10, 240, 130, 23);

        TanggalPeriksa.setForeground(new java.awt.Color(50, 70, 50));
        TanggalPeriksa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "22-12-2022" }));
        TanggalPeriksa.setDisplayFormat("dd-MM-yyyy");
        TanggalPeriksa.setName("TanggalPeriksa"); // NOI18N
        TanggalPeriksa.setOpaque(false);
        TanggalPeriksa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TanggalPeriksaActionPerformed(evt);
            }
        });
        TanggalPeriksa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TanggalPeriksaKeyPressed(evt);
            }
        });
        FormInput.add(TanggalPeriksa);
        TanggalPeriksa.setBounds(650, 40, 90, 23);

        jLabel13.setText("Tanggal Periksa:");
        jLabel13.setName("jLabel13"); // NOI18N
        FormInput.add(jLabel13);
        jLabel13.setBounds(540, 40, 100, 23);

        NoSurat1.setHighlighter(null);
        NoSurat1.setName("NoSurat1"); // NOI18N
        NoSurat1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NoSurat1KeyPressed(evt);
            }
        });
        FormInput.add(NoSurat1);
        NoSurat1.setBounds(99, 40, 141, 23);

        KdDokter.setHighlighter(null);
        KdDokter.setName("KdDokter"); // NOI18N
        KdDokter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KdDokterKeyPressed(evt);
            }
        });
        FormInput.add(KdDokter);
        KdDokter.setBounds(100, 70, 107, 23);

        TDokter.setEditable(false);
        TDokter.setName("TDokter"); // NOI18N
        FormInput.add(TDokter);
        TDokter.setBounds(210, 70, 209, 23);

        BtnDokter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/picture/190.png"))); // NOI18N
        BtnDokter.setMnemonic('3');
        BtnDokter.setToolTipText("ALt+3");
        BtnDokter.setName("BtnDokter"); // NOI18N
        BtnDokter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDokterActionPerformed(evt);
            }
        });
        FormInput.add(BtnDokter);
        BtnDokter.setBounds(420, 70, 28, 23);

        jLabel15.setText("Dokter:");
        jLabel15.setName("jLabel15"); // NOI18N
        FormInput.add(jLabel15);
        jLabel15.setBounds(50, 70, 40, 23);

        jLabel17.setText("Pekerjaan:");
        jLabel17.setName("jLabel17"); // NOI18N
        FormInput.add(jLabel17);
        jLabel17.setBounds(250, 40, 60, 23);

        Pekerjaan.setName("Pekerjaan"); // NOI18N
        FormInput.add(Pekerjaan);
        Pekerjaan.setBounds(320, 40, 209, 23);

        jLabel22.setText("Wawancara Psikiatri :");
        jLabel22.setName("jLabel22"); // NOI18N
        FormInput.add(jLabel22);
        jLabel22.setBounds(0, 100, 110, 23);

        jLabel23.setText("Hasil Tes MMPI (Minnesota Multiphasic Personality Inventory)");
        jLabel23.setName("jLabel23"); // NOI18N
        FormInput.add(jLabel23);
        jLabel23.setBounds(0, 130, 300, 23);

        scrollPane1.setName("scrollPane1"); // NOI18N

        SikapTes.setColumns(20);
        SikapTes.setRows(5);
        SikapTes.setName("SikapTes"); // NOI18N
        scrollPane1.setViewportView(SikapTes);

        FormInput.add(scrollPane1);
        scrollPane1.setBounds(140, 150, 580, 80);

        jLabel24.setText("a. Sikap Terhadap Tes");
        jLabel24.setName("jLabel24"); // NOI18N
        FormInput.add(jLabel24);
        jLabel24.setBounds(10, 150, 110, 23);

        PotensiKerja.setHighlighter(null);
        PotensiKerja.setName("PotensiKerja"); // NOI18N
        PotensiKerja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PotensiKerjaActionPerformed(evt);
            }
        });
        PotensiKerja.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PotensiKerjaKeyPressed(evt);
            }
        });
        FormInput.add(PotensiKerja);
        PotensiKerja.setBounds(140, 260, 60, 23);

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("(Rentang skor: 0 = rendah, 1 = sedang, 2 =tinggil)");
        jLabel25.setName("jLabel25"); // NOI18N
        FormInput.add(jLabel25);
        jLabel25.setBounds(290, 380, 260, 23);

        jLabel26.setText("Kemampuan Adaptasi:");
        jLabel26.setName("jLabel26"); // NOI18N
        FormInput.add(jLabel26);
        jLabel26.setBounds(10, 290, 110, 23);

        KemampuanAdaptasi.setHighlighter(null);
        KemampuanAdaptasi.setName("KemampuanAdaptasi"); // NOI18N
        KemampuanAdaptasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KemampuanAdaptasiActionPerformed(evt);
            }
        });
        KemampuanAdaptasi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KemampuanAdaptasiKeyPressed(evt);
            }
        });
        FormInput.add(KemampuanAdaptasi);
        KemampuanAdaptasi.setBounds(140, 290, 60, 23);

        jLabel27.setText("Kendala Psikologis:");
        jLabel27.setName("jLabel27"); // NOI18N
        FormInput.add(jLabel27);
        jLabel27.setBounds(10, 320, 110, 23);

        KendalaPsikologis.setHighlighter(null);
        KendalaPsikologis.setName("KendalaPsikologis"); // NOI18N
        KendalaPsikologis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KendalaPsikologisActionPerformed(evt);
            }
        });
        KendalaPsikologis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KendalaPsikologisKeyPressed(evt);
            }
        });
        FormInput.add(KendalaPsikologis);
        KendalaPsikologis.setBounds(140, 320, 60, 23);

        jLabel28.setText("Perilaku Beresiko:");
        jLabel28.setName("jLabel28"); // NOI18N
        FormInput.add(jLabel28);
        jLabel28.setBounds(10, 350, 110, 23);

        PerilakuBeresiko.setHighlighter(null);
        PerilakuBeresiko.setName("PerilakuBeresiko"); // NOI18N
        PerilakuBeresiko.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PerilakuBeresikoActionPerformed(evt);
            }
        });
        PerilakuBeresiko.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PerilakuBeresikoKeyPressed(evt);
            }
        });
        FormInput.add(PerilakuBeresiko);
        PerilakuBeresiko.setBounds(140, 350, 60, 23);

        jLabel29.setText("Integritas Moral:");
        jLabel29.setName("jLabel29"); // NOI18N
        FormInput.add(jLabel29);
        jLabel29.setBounds(10, 380, 110, 23);

        IntegrasiMoral.setHighlighter(null);
        IntegrasiMoral.setName("IntegrasiMoral"); // NOI18N
        IntegrasiMoral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IntegrasiMoralActionPerformed(evt);
            }
        });
        IntegrasiMoral.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IntegrasiMoralKeyPressed(evt);
            }
        });
        FormInput.add(IntegrasiMoral);
        IntegrasiMoral.setBounds(140, 380, 60, 23);

        jLabel30.setText("Potensi Kerja :");
        jLabel30.setName("jLabel30"); // NOI18N
        FormInput.add(jLabel30);
        jLabel30.setBounds(20, 260, 100, 23);

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("(Rentang skor: 0 = kurang, 1 = sedang, 2 = besar)");
        jLabel31.setName("jLabel31"); // NOI18N
        FormInput.add(jLabel31);
        jLabel31.setBounds(290, 260, 260, 23);

        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("(Rentang skor: 0 = kurang, 1 = sedang, 2 = besar)");
        jLabel32.setName("jLabel32"); // NOI18N
        FormInput.add(jLabel32);
        jLabel32.setBounds(290, 290, 260, 23);

        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("(Rentang skor: 0 = berat, 1 = sedang, 2 = ringan)");
        jLabel33.setName("jLabel33"); // NOI18N
        FormInput.add(jLabel33);
        jLabel33.setBounds(290, 320, 260, 23);

        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setText("(Rentang skor: 0 = besar, 1 = sedang, 2 = kecil)");
        jLabel34.setName("jLabel34"); // NOI18N
        FormInput.add(jLabel34);
        jLabel34.setBounds(290, 350, 260, 23);

        jLabel35.setText("Index Kapasitas Mental:");
        jLabel35.setName("jLabel35"); // NOI18N
        FormInput.add(jLabel35);
        jLabel35.setBounds(10, 410, 130, 23);

        IndexKapasitasMental.setHighlighter(null);
        IndexKapasitasMental.setName("IndexKapasitasMental"); // NOI18N
        IndexKapasitasMental.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IndexKapasitasMentalActionPerformed(evt);
            }
        });
        IndexKapasitasMental.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IndexKapasitasMentalKeyPressed(evt);
            }
        });
        FormInput.add(IndexKapasitasMental);
        IndexKapasitasMental.setBounds(160, 410, 90, 23);

        ProfilKlinis.setHighlighter(null);
        ProfilKlinis.setName("ProfilKlinis"); // NOI18N
        ProfilKlinis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProfilKlinisActionPerformed(evt);
            }
        });
        ProfilKlinis.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ProfilKlinisKeyPressed(evt);
            }
        });
        FormInput.add(ProfilKlinis);
        ProfilKlinis.setBounds(130, 440, 590, 23);

        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setText("c. Profil Klinis :");
        jLabel36.setName("jLabel36"); // NOI18N
        FormInput.add(jLabel36);
        jLabel36.setBounds(10, 440, 110, 23);

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("d. Indeks Kepribadian Dasar");
        jLabel20.setName("jLabel20"); // NOI18N
        FormInput.add(jLabel20);
        jLabel20.setBounds(10, 480, 150, 23);

        KeterbukaanPikiran.setHighlighter(null);
        KeterbukaanPikiran.setName("KeterbukaanPikiran"); // NOI18N
        KeterbukaanPikiran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KeterbukaanPikiranActionPerformed(evt);
            }
        });
        KeterbukaanPikiran.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeterbukaanPikiranKeyPressed(evt);
            }
        });
        FormInput.add(KeterbukaanPikiran);
        KeterbukaanPikiran.setBounds(300, 500, 60, 23);

        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel37.setText("(Rentang skor: 0=kurang, 1=sedang, 2=besar)");
        jLabel37.setName("jLabel37"); // NOI18N
        FormInput.add(jLabel37);
        jLabel37.setBounds(380, 620, 260, 23);

        jLabel38.setText("Keterbukaan hari (Conscientiuesness):");
        jLabel38.setName("jLabel38"); // NOI18N
        FormInput.add(jLabel38);
        jLabel38.setBounds(80, 530, 190, 23);

        KeterbukaanHati.setHighlighter(null);
        KeterbukaanHati.setName("KeterbukaanHati"); // NOI18N
        KeterbukaanHati.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KeterbukaanHatiActionPerformed(evt);
            }
        });
        KeterbukaanHati.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeterbukaanHatiKeyPressed(evt);
            }
        });
        FormInput.add(KeterbukaanHati);
        KeterbukaanHati.setBounds(300, 530, 60, 23);

        jLabel39.setText("Keterbukaan terhadap orang lain (Extraversion):");
        jLabel39.setName("jLabel39"); // NOI18N
        FormInput.add(jLabel39);
        jLabel39.setBounds(30, 560, 240, 23);

        KeterbukaanOrang.setHighlighter(null);
        KeterbukaanOrang.setName("KeterbukaanOrang"); // NOI18N
        KeterbukaanOrang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KeterbukaanOrangActionPerformed(evt);
            }
        });
        KeterbukaanOrang.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeterbukaanOrangKeyPressed(evt);
            }
        });
        FormInput.add(KeterbukaanOrang);
        KeterbukaanOrang.setBounds(300, 560, 60, 23);

        jLabel40.setText("Keterbukaan terhadap Kesepakatan (Agreebleness):");
        jLabel40.setName("jLabel40"); // NOI18N
        FormInput.add(jLabel40);
        jLabel40.setBounds(10, 590, 260, 23);

        KeterbukaanKesepakatan.setHighlighter(null);
        KeterbukaanKesepakatan.setName("KeterbukaanKesepakatan"); // NOI18N
        KeterbukaanKesepakatan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KeterbukaanKesepakatanActionPerformed(evt);
            }
        });
        KeterbukaanKesepakatan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeterbukaanKesepakatanKeyPressed(evt);
            }
        });
        FormInput.add(KeterbukaanKesepakatan);
        KeterbukaanKesepakatan.setBounds(300, 590, 60, 23);

        jLabel41.setText("Keterbukaan terhadap Tekanan (Neuroticism):");
        jLabel41.setName("jLabel41"); // NOI18N
        FormInput.add(jLabel41);
        jLabel41.setBounds(40, 620, 230, 23);

        KeterbukaanTekanan.setHighlighter(null);
        KeterbukaanTekanan.setName("KeterbukaanTekanan"); // NOI18N
        KeterbukaanTekanan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KeterbukaanTekananActionPerformed(evt);
            }
        });
        KeterbukaanTekanan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeterbukaanTekananKeyPressed(evt);
            }
        });
        FormInput.add(KeterbukaanTekanan);
        KeterbukaanTekanan.setBounds(300, 620, 60, 23);

        jLabel42.setText("Keterbukaan Pikiran (openness):");
        jLabel42.setName("jLabel42"); // NOI18N
        FormInput.add(jLabel42);
        jLabel42.setBounds(110, 500, 160, 23);

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel43.setText("(Rentang skor: 0=kurang, 1=sedang, 2=besar)");
        jLabel43.setName("jLabel43"); // NOI18N
        FormInput.add(jLabel43);
        jLabel43.setBounds(380, 500, 260, 23);

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("(Rentang skor: 0=kurang, 1=sedang, 2=besar)");
        jLabel44.setName("jLabel44"); // NOI18N
        FormInput.add(jLabel44);
        jLabel44.setBounds(380, 530, 260, 23);

        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel45.setText("(Rentang skor: 0=kurang, 1=sedang, 2=besar)");
        jLabel45.setName("jLabel45"); // NOI18N
        FormInput.add(jLabel45);
        jLabel45.setBounds(380, 560, 260, 23);

        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("(Rentang skor: 0=kurang, 1=sedang, 2=besar)");
        jLabel46.setName("jLabel46"); // NOI18N
        FormInput.add(jLabel46);
        jLabel46.setBounds(380, 590, 260, 23);

        jLabel47.setText("INDEKS “OCEAN” (Change DNA):");
        jLabel47.setName("jLabel47"); // NOI18N
        FormInput.add(jLabel47);
        jLabel47.setBounds(100, 650, 170, 23);

        IndeksOcean.setHighlighter(null);
        IndeksOcean.setName("IndeksOcean"); // NOI18N
        IndeksOcean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IndeksOceanActionPerformed(evt);
            }
        });
        IndeksOcean.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                IndeksOceanKeyPressed(evt);
            }
        });
        FormInput.add(IndeksOcean);
        IndeksOcean.setBounds(300, 650, 60, 23);

        jLabel48.setText("Kesimpulan:");
        jLabel48.setName("jLabel48"); // NOI18N
        FormInput.add(jLabel48);
        jLabel48.setBounds(10, 680, 70, 23);

        Kesimpulan.setHighlighter(null);
        Kesimpulan.setName("Kesimpulan"); // NOI18N
        Kesimpulan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KesimpulanActionPerformed(evt);
            }
        });
        Kesimpulan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KesimpulanKeyPressed(evt);
            }
        });
        FormInput.add(Kesimpulan);
        Kesimpulan.setBounds(90, 680, 660, 23);

        jLabel49.setText("Keperluan:");
        jLabel49.setName("jLabel49"); // NOI18N
        FormInput.add(jLabel49);
        jLabel49.setBounds(10, 710, 70, 23);

        Keperluan.setHighlighter(null);
        Keperluan.setName("Keperluan"); // NOI18N
        Keperluan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                KeperluanActionPerformed(evt);
            }
        });
        Keperluan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                KeperluanKeyPressed(evt);
            }
        });
        FormInput.add(Keperluan);
        Keperluan.setBounds(90, 710, 640, 23);

        Scroll1.setViewportView(FormInput);

        PanelInput.add(Scroll1, java.awt.BorderLayout.CENTER);

        internalFrame1.add(PanelInput, java.awt.BorderLayout.PAGE_START);

        getContentPane().add(internalFrame1, java.awt.BorderLayout.CENTER);
        internalFrame1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void NoSuratKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NoSuratKeyPressed
       Valid.pindah(evt,TCari,TanggalPeriksa);
}//GEN-LAST:event_NoSuratKeyPressed

    private void TNoRwKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRwKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isRawat();
            isPsien();
        }else{            
            Valid.pindah(evt,TCari,WawancaraPrikiatri);
        }
}//GEN-LAST:event_TNoRwKeyPressed

    private void TPasienKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TPasienKeyPressed
        Valid.pindah(evt,TCari,BtnSimpan);
}//GEN-LAST:event_TPasienKeyPressed

    private void BtnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpanActionPerformed
        if(NoSurat.getText().trim().equals("")){
            Valid.textKosong(NoSurat,"No.Surat Sakit");
        }else if(WawancaraPrikiatri.getText().trim().equals("")){
            Valid.textKosong(WawancaraPrikiatri,"lama sakit");
        }else if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");
        }else if(KdDokter.getText().trim().equals("")||TDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"dokter");
        }else{
            if(Sequel.menyimpantf("suratjiwa","?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?","No.Surat Sakit",22,new String[]{
                    TNoRw.getText(),NoSurat.getText(),Pekerjaan.getText(),Valid.SetTgl(TanggalPeriksa.getSelectedItem()+""),
                    KdDokter.getText(),WawancaraPrikiatri.getText(),SikapTes.getText(),PotensiKerja.getText(),
                    KemampuanAdaptasi.getText(),KendalaPsikologis.getText(),PerilakuBeresiko.getText(),IntegrasiMoral.getText(),
                    IndexKapasitasMental.getText(),ProfilKlinis.getText(),KeterbukaanPikiran.getText(),KeterbukaanHati.getText(),
                    KeterbukaanOrang.getText(),KeterbukaanKesepakatan.getText(),KeterbukaanTekanan.getText(),IndeksOcean.getText(),Kesimpulan.getText(),
                    Keperluan.getText()
                })==true){
                tampil();
                emptTeks();
            }
        }
}//GEN-LAST:event_BtnSimpanActionPerformed

    private void BtnSimpanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnSimpanKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnSimpanActionPerformed(null);
        }else{
            Valid.pindah(evt,WawancaraPrikiatri,BtnBatal);
        }
}//GEN-LAST:event_BtnSimpanKeyPressed

    private void BtnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBatalActionPerformed
        emptTeks();
        ChkInput.setSelected(true);
        isForm(); 
        
}//GEN-LAST:event_BtnBatalActionPerformed

    private void BtnBatalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnBatalKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            emptTeks();
        }else{Valid.pindah(evt, BtnSimpan, BtnHapus);}
}//GEN-LAST:event_BtnBatalKeyPressed

    private void BtnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusActionPerformed
        Valid.hapusTable(tabMode,NoSurat,"suratjiwa","no_surat");
        tampil();
        emptTeks();
}//GEN-LAST:event_BtnHapusActionPerformed

    private void BtnHapusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnHapusKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnHapusActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnBatal, BtnEdit);
        }
}//GEN-LAST:event_BtnHapusKeyPressed

    private void BtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditActionPerformed
        if(NoSurat.getText().trim().equals("")){
            Valid.textKosong(NoSurat,"No.Surat Sakit");      
        }else if(TNoRw.getText().trim().equals("")||TPasien.getText().trim().equals("")){
            Valid.textKosong(TNoRw,"pasien");  
        }else if(KdDokter.getText().trim().equals("")||TDokter.getText().trim().equals("")){
            Valid.textKosong(KdDokter,"dokter");
        }else if(Pekerjaan.getText().trim().equals("")){
            Valid.textKosong(Pekerjaan,"Pekerjaan");
        }else{    
            if(tbObat.getSelectedRow()!= -1){
                if(Sequel.mengedittf("suratjiwa","no_surat=?","no_surat=?,no_rawat=?,tanggalawal=?,tanggalakhir=?,lamasakit=?,nip=?,instansi=?,pekerjaan=?",9,new String[]{
                    NoSurat.getText(),TNoRw.getText(),Valid.SetTgl(TanggalPeriksa.getSelectedItem()+""),WawancaraPrikiatri.getText(),
                    KdDokter.getText(),Pekerjaan.getText(),tbObat.getValueAt(tbObat.getSelectedRow(),0).toString()
                })==true){
                    tampil();
                    emptTeks();
                }
            }
        }
}//GEN-LAST:event_BtnEditActionPerformed

    private void BtnEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnEditKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnEditActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnHapus, BtnPrint);
        }
}//GEN-LAST:event_BtnEditKeyPressed

    private void BtnKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKeluarActionPerformed
        dispose();
}//GEN-LAST:event_BtnKeluarActionPerformed

    private void BtnKeluarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnKeluarKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            dispose();
        }else{Valid.pindah(evt,BtnEdit,TCari);}
}//GEN-LAST:event_BtnKeluarKeyPressed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if(tabMode.getRowCount()==0){
            JOptionPane.showMessageDialog(null,"Maaf, data sudah habis. Tidak ada data yang bisa anda print...!!!!");
            BtnBatal.requestFocus();
        }else if(tabMode.getRowCount()!=0){
            Map<String, Object> param = new HashMap<>(); 
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());   
                param.put("logo",Sequel.cariGambar("select logo from setting")); 
            tgl=" suratjiwa.tanggalawal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' ";
            if(TCari.getText().trim().equals("")){
                Valid.MyReportqry("rptDataSuratSakit.jasper","report","::[ Data Surat Sakit Pasien ]::",
                     "select suratjiwa.no_surat,suratjiwa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                     "suratjiwa.tanggalawal,suratjiwa.tanggalakhir,suratjiwa.lamasakit "+                  
                     "from suratjiwa inner join reg_periksa on suratjiwa.no_rawat=reg_periksa.no_rawat "+
                     "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                     "where "+tgl+"order by suratjiwa.no_surat",param);
            }else{
                Valid.MyReportqry("rptDataSuratSakit.jasper","report","::[ Data Surat Sakit Pasien ]::",
                     "select suratjiwa.no_surat,suratjiwa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                     "suratjiwa.tanggalawal,suratjiwa.tanggalakhir,suratjiwa.lamasakit "+                  
                     "from suratjiwa inner join reg_periksa on suratjiwa.no_rawat=reg_periksa.no_rawat "+
                     "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                     "where "+tgl+"and no_surat like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and suratjiwa.no_rawat like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and reg_periksa.no_rkm_medis like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and pasien.nm_pasien like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and suratjiwa.tanggalawal like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and suratjiwa.tanggalakhir like '%"+TCari.getText().trim()+"%' "+
                     "order by suratjiwa.no_surat",param);
            }
            
        }
        this.setCursor(Cursor.getDefaultCursor());        
}//GEN-LAST:event_BtnPrintActionPerformed

    private void BtnPrintKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnPrintKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnPrintActionPerformed(null);
        }else{
            Valid.pindah(evt, BtnEdit, BtnKeluar);
        }
}//GEN-LAST:event_BtnPrintKeyPressed

    private void TCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            BtnCariActionPerformed(null);
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            BtnCari.requestFocus();
        }else if(evt.getKeyCode()==KeyEvent.VK_PAGE_UP){
            BtnKeluar.requestFocus();
        }
}//GEN-LAST:event_TCariKeyPressed

    private void BtnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariActionPerformed
        tampil();
}//GEN-LAST:event_BtnCariActionPerformed

    private void BtnCariKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnCariKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            BtnCariActionPerformed(null);
        }else{
            Valid.pindah(evt, TCari, BtnAll);
        }
}//GEN-LAST:event_BtnCariKeyPressed

    private void BtnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAllActionPerformed
        TCari.setText("");
        tampil();
}//GEN-LAST:event_BtnAllActionPerformed

    private void BtnAllKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BtnAllKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_SPACE){
            tampil();
            TCari.setText("");
        }else{
            Valid.pindah(evt, BtnCari, TPasien);
        }
}//GEN-LAST:event_BtnAllKeyPressed
   
                                  
    private void TNoRMKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TNoRMKeyPressed
       
}//GEN-LAST:event_TNoRMKeyPressed

    private void tbObatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbObatMouseClicked
        if(tabMode.getRowCount()!=0){
            try {
                getData();
            } catch (java.lang.NullPointerException e) {
            }
        }
}//GEN-LAST:event_tbObatMouseClicked

    private void WawancaraPrikiatriKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_WawancaraPrikiatriKeyPressed
//        Valid.pindah(evt,TanggalAkhir,BtnSimpan);
    }//GEN-LAST:event_WawancaraPrikiatriKeyPressed

    private void ChkInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkInputActionPerformed
       isForm();
    }//GEN-LAST:event_ChkInputActionPerformed

    private void tbObatKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tbObatKeyReleased
        if(tabMode.getRowCount()!=0){
            if((evt.getKeyCode()==KeyEvent.VK_ENTER)||(evt.getKeyCode()==KeyEvent.VK_UP)||(evt.getKeyCode()==KeyEvent.VK_DOWN)){
                try {
                    getData();
                } catch (java.lang.NullPointerException e) {
                }
            }
        }
    }//GEN-LAST:event_tbObatKeyReleased

    private void WawancaraPrikiatriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_WawancaraPrikiatriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_WawancaraPrikiatriActionPerformed

    private void TanggalPeriksaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TanggalPeriksaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TanggalPeriksaActionPerformed

    private void TanggalPeriksaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TanggalPeriksaKeyPressed
//        Valid.pindah(evt,NoSurat,TanggalAkhir);
    }//GEN-LAST:event_TanggalPeriksaKeyPressed

    private void MnCetakKesehatanJiwaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MnCetakKesehatanJiwaActionPerformed
       if(TPasien.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Maaf, Silahkan anda pilih dulu pasien...!!!");
        }else{
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                Map<String, Object> param = new HashMap<>();
                param.put("hari",WawancaraPrikiatri.getText());
                param.put("TanggalAwal",TanggalPeriksa.getSelectedItem().toString());
//                param.put("TanggalAkhir",TanggalAkhir.getSelectedItem().toString());
                param.put("nosakit",NoSurat.getText());
                param.put("namars",akses.getnamars());
                param.put("alamatrs",akses.getalamatrs());
                param.put("kotars",akses.getkabupatenrs());
                param.put("propinsirs",akses.getpropinsirs());
                param.put("kontakrs",akses.getkontakrs());
                param.put("emailrs",akses.getemailrs());  
                param.put("pekerjaan",Pekerjaan.getText());
                param.put("dokterpj",TDokter.getText());
                param.put("penyakit",Sequel.cariIsi("select concat(diagnosa_pasien.kd_penyakit,' ',penyakit.nm_penyakit) from diagnosa_pasien inner join reg_periksa inner join penyakit "+
                          "on diagnosa_pasien.no_rawat=reg_periksa.no_rawat and diagnosa_pasien.kd_penyakit=penyakit.kd_penyakit "+
                          "where diagnosa_pasien.no_rawat=? and diagnosa_pasien.prioritas='1'",TNoRw.getText()));
                param.put("logo",Sequel.cariGambar("select logo from setting")); 
                finger=Sequel.cariIsi("select sha1(sidikjari) from sidikjari inner join pegawai on pegawai.id=sidikjari.id where pegawai.nik=?",KdDokter.getText());
                param.put("finger","Dikeluarkan di "+akses.getnamars()+", Kabupaten/Kota "+akses.getkabupatenrs()+"\nDitandatangani secara elektronik oleh "+TDokter.getText()+"\nID "+(finger.equals("")?KdDokter.getText():finger));
                Valid.MyReportqry("rptSuratSehatJiwa.jasper","report","::[ Surat Sakit ]::",
                              "select DATE_FORMAT(reg_periksa.tgl_registrasi,'%d-%m-%Y')as tgl_registrasi,perusahaan_pasien.nama_perusahaan,reg_periksa.no_rawat,dokter.nm_dokter,pasien.keluarga,pasien.namakeluarga,pasien.tgl_lahir,pasien.jk," +
                              " pasien.nm_pasien,pasien.jk,concat(reg_periksa.umurdaftar,' ',reg_periksa.sttsumur)as umur,pasien.pekerjaan,concat(pasien.alamat,', ',kelurahan.nm_kel,', ',kecamatan.nm_kec,', ',kabupaten.nm_kab) as alamat" +
                              " from reg_periksa inner join pasien inner join dokter inner join kelurahan inner join perusahaan_pasien inner join kecamatan inner join kabupaten" +
                              " on reg_periksa.no_rkm_medis=pasien.no_rkm_medis and reg_periksa.kd_dokter=dokter.kd_dokter and pasien.kd_kel=kelurahan.kd_kel "+
                              "and pasien.perusahaan_pasien=perusahaan_pasien.kode_perusahaan and pasien.kd_kec=kecamatan.kd_kec and pasien.kd_kab=kabupaten.kd_kab "+
                              "where reg_periksa.no_rawat='"+TNoRw.getText()+"' ",param);
                this.setCursor(Cursor.getDefaultCursor());  
       }
    }//GEN-LAST:event_MnCetakKesehatanJiwaActionPerformed

    private void NoSurat1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NoSurat1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_NoSurat1KeyPressed

    private void KdDokterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KdDokterKeyPressed
    /*    if(evt.getKeyCode()==KeyEvent.VK_PAGE_DOWN){
            isNumber();
            Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?",TDokter,KdDokter.getText());
        }else if(evt.getKeyCode()==KeyEvent.VK_UP){
            BtnDokterActionPerformed(null);
        }else{
            Valid.pindah(evt,CmbDetik,kdpoli);
        } */
    }//GEN-LAST:event_KdDokterKeyPressed

    private void BtnDokterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDokterActionPerformed
        akses.setform("SuratSakit");
        dokter.isCek();        
        dokter.TCari.requestFocus();
        dokter.setSize(internalFrame1.getWidth()-20,internalFrame1.getHeight()-20);
        dokter.setLocationRelativeTo(internalFrame1);
        dokter.setVisible(true);
    }//GEN-LAST:event_BtnDokterActionPerformed

    private void kdperusahaanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kdperusahaanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_kdperusahaanKeyPressed

    private void PotensiKerjaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PotensiKerjaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PotensiKerjaActionPerformed

    private void PotensiKerjaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PotensiKerjaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PotensiKerjaKeyPressed

    private void KemampuanAdaptasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KemampuanAdaptasiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KemampuanAdaptasiActionPerformed

    private void KemampuanAdaptasiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KemampuanAdaptasiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KemampuanAdaptasiKeyPressed

    private void KendalaPsikologisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KendalaPsikologisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KendalaPsikologisActionPerformed

    private void KendalaPsikologisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KendalaPsikologisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KendalaPsikologisKeyPressed

    private void PerilakuBeresikoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PerilakuBeresikoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PerilakuBeresikoActionPerformed

    private void PerilakuBeresikoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PerilakuBeresikoKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_PerilakuBeresikoKeyPressed

    private void IntegrasiMoralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IntegrasiMoralActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IntegrasiMoralActionPerformed

    private void IntegrasiMoralKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IntegrasiMoralKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_IntegrasiMoralKeyPressed

    private void IndexKapasitasMentalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IndexKapasitasMentalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IndexKapasitasMentalActionPerformed

    private void IndexKapasitasMentalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IndexKapasitasMentalKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_IndexKapasitasMentalKeyPressed

    private void ProfilKlinisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProfilKlinisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ProfilKlinisActionPerformed

    private void ProfilKlinisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ProfilKlinisKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_ProfilKlinisKeyPressed

    private void KeterbukaanPikiranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KeterbukaanPikiranActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanPikiranActionPerformed

    private void KeterbukaanPikiranKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeterbukaanPikiranKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanPikiranKeyPressed

    private void KeterbukaanHatiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KeterbukaanHatiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanHatiActionPerformed

    private void KeterbukaanHatiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeterbukaanHatiKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanHatiKeyPressed

    private void KeterbukaanOrangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KeterbukaanOrangActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanOrangActionPerformed

    private void KeterbukaanOrangKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeterbukaanOrangKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanOrangKeyPressed

    private void KeterbukaanKesepakatanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KeterbukaanKesepakatanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanKesepakatanActionPerformed

    private void KeterbukaanKesepakatanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeterbukaanKesepakatanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanKesepakatanKeyPressed

    private void KeterbukaanTekananActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KeterbukaanTekananActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanTekananActionPerformed

    private void KeterbukaanTekananKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeterbukaanTekananKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeterbukaanTekananKeyPressed

    private void IndeksOceanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IndeksOceanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IndeksOceanActionPerformed

    private void IndeksOceanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IndeksOceanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_IndeksOceanKeyPressed

    private void KesimpulanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KesimpulanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KesimpulanActionPerformed

    private void KesimpulanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KesimpulanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KesimpulanKeyPressed

    private void KeperluanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_KeperluanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeperluanActionPerformed

    private void KeperluanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_KeperluanKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_KeperluanKeyPressed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            SuratKeteranganKesehatanJiwa dialog = new SuratKeteranganKesehatanJiwa(new javax.swing.JFrame(), true);
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private widget.Button BtnAll;
    private widget.Button BtnBatal;
    private widget.Button BtnCari;
    private widget.Button BtnDokter;
    private widget.Button BtnEdit;
    private widget.Button BtnHapus;
    private widget.Button BtnKeluar;
    private widget.Button BtnPrint;
    private widget.Button BtnSimpan;
    private widget.CekBox ChkInput;
    private widget.Tanggal DTPCari1;
    private widget.Tanggal DTPCari2;
    private widget.PanelBiasa FormInput;
    private widget.TextBox IndeksOcean;
    private widget.TextBox IndexKapasitasMental;
    private widget.TextBox IntegrasiMoral;
    private widget.TextBox KdDokter;
    private widget.TextBox KemampuanAdaptasi;
    private widget.TextBox KendalaPsikologis;
    private widget.TextBox Keperluan;
    private widget.TextBox Kesimpulan;
    private widget.TextBox KeterbukaanHati;
    private widget.TextBox KeterbukaanKesepakatan;
    private widget.TextBox KeterbukaanOrang;
    private widget.TextBox KeterbukaanPikiran;
    private widget.TextBox KeterbukaanTekanan;
    private widget.Label LCount;
    private javax.swing.JMenuItem MnCetakKesehatanJiwa;
    private widget.TextBox NoSurat;
    private widget.TextBox NoSurat1;
    private javax.swing.JPanel PanelInput;
    private widget.TextBox Pekerjaan;
    private widget.TextBox PerilakuBeresiko;
    private widget.TextBox PotensiKerja;
    private widget.TextBox ProfilKlinis;
    private widget.ScrollPane Scroll;
    private widget.ScrollPane Scroll1;
    private widget.TextArea SikapTes;
    private widget.TextBox TCari;
    private widget.TextBox TDokter;
    private widget.TextBox TNoRM;
    private widget.TextBox TNoRw;
    private widget.TextBox TPasien;
    private widget.Tanggal TanggalPeriksa;
    private widget.TextBox WawancaraPrikiatri;
    private widget.InternalFrame internalFrame1;
    private widget.Label jLabel13;
    private widget.Label jLabel15;
    private widget.Label jLabel17;
    private widget.Label jLabel18;
    private widget.Label jLabel19;
    private widget.Label jLabel20;
    private widget.Label jLabel21;
    private widget.Label jLabel22;
    private widget.Label jLabel23;
    private widget.Label jLabel24;
    private widget.Label jLabel25;
    private widget.Label jLabel26;
    private widget.Label jLabel27;
    private widget.Label jLabel28;
    private widget.Label jLabel29;
    private widget.Label jLabel3;
    private widget.Label jLabel30;
    private widget.Label jLabel31;
    private widget.Label jLabel32;
    private widget.Label jLabel33;
    private widget.Label jLabel34;
    private widget.Label jLabel35;
    private widget.Label jLabel36;
    private widget.Label jLabel37;
    private widget.Label jLabel38;
    private widget.Label jLabel39;
    private widget.Label jLabel4;
    private widget.Label jLabel40;
    private widget.Label jLabel41;
    private widget.Label jLabel42;
    private widget.Label jLabel43;
    private widget.Label jLabel44;
    private widget.Label jLabel45;
    private widget.Label jLabel46;
    private widget.Label jLabel47;
    private widget.Label jLabel48;
    private widget.Label jLabel49;
    private widget.Label jLabel6;
    private widget.Label jLabel7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu jPopupMenu1;
    private widget.TextBox kdperusahaan;
    private widget.panelisi panelGlass8;
    private widget.panelisi panelGlass9;
    private widget.ScrollPane scrollPane1;
    private widget.Table tbObat;
    // End of variables declaration//GEN-END:variables

    public void tampil() {
        Valid.tabelKosong(tabMode);
        try{
            tgl=" suratjiwa.tanggalawal between '"+Valid.SetTgl(DTPCari1.getSelectedItem()+"")+"' and '"+Valid.SetTgl(DTPCari2.getSelectedItem()+"")+"' ";
            if(TCari.getText().trim().equals("")){
                ps=koneksi.prepareStatement(
                     "select suratjiwa.no_surat,suratjiwa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                     "suratjiwa.tanggalawal,suratjiwa.tanggalakhir,suratjiwa.lamasakit,suratjiwa.nip,dokter.nm_dokter,suratjiwa.instansi,suratjiwa.pekerjaan "+                  
                     "from suratjiwa inner join reg_periksa on suratjiwa.no_rawat=reg_periksa.no_rawat "+
                     "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis inner join dokter on suratjiwa.nip=dokter.kd_dokter "+
                     "where "+tgl+"order by suratjiwa.no_surat");
            }else{
                ps=koneksi.prepareStatement(
                     "select suratjiwa.no_surat,suratjiwa.no_rawat,reg_periksa.no_rkm_medis,pasien.nm_pasien,"+
                     "suratjiwa.tanggalawal,suratjiwa.tanggalakhir,suratjiwa.lamasakit,suratjiwa.nip,dokter.nm_dokter,suratjiwa.instansi,suratjiwa.pekerjaan "+                  
                     "from suratjiwa inner join reg_periksa on suratjiwa.no_rawat=reg_periksa.no_rawat inner join dokter on suratjiwa.nip=dokter.kd_dokter "+
                     "inner join pasien on reg_periksa.no_rkm_medis=pasien.no_rkm_medis "+
                     "where "+tgl+"and no_surat like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and suratjiwa.no_rawat like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and reg_periksa.no_rkm_medis like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and pasien.nm_pasien like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and suratjiwa.instansi like '%"+TCari.getText().trim()+"%' or "+        
                     tgl+"and suratjiwa.tanggalawal like '%"+TCari.getText().trim()+"%' or "+
                     tgl+"and suratjiwa.tanggalakhir like '%"+TCari.getText().trim()+"%' "+
                     "order by suratjiwa.no_surat");
            }
                
            try {
                rs=ps.executeQuery();
                while(rs.next()){
                    tabMode.addRow(new String[]{
                        rs.getString(1),rs.getString(2),rs.getString(3),
                        rs.getString(4),rs.getString(5),rs.getString(6),
                        rs.getString(7),rs.getString(8),rs.getString(9),
                        rs.getString(10),rs.getString(11)
                    });
                }
            } catch (Exception e) {
                System.out.println("Notif : "+e);
            } finally{
                if(rs!=null){
                    rs.close();
                }
                if(ps!=null){
                    ps.close();
                }
            }
        }catch(Exception e){
            System.out.println("Notifikasi : "+e);
        }
        LCount.setText(""+tabMode.getRowCount());
    }

    public void emptTeks() {
        TNoRw.setText("");
        TNoRM.setText("");
        TPasien.setText("");
        NoSurat.setText("");
        WawancaraPrikiatri.setText("1 (Satu)");
        KdDokter.setText("");
        TDokter.setText("");
        Pekerjaan.setText("");
      
        TanggalPeriksa.setDate(new Date());
        nomorSurat();
//      Valid.autoNomer3("select ifnull(MAX(CONVERT(RIGHT(no_surat,3),signed)),0) from suratjiwa where tanggalawal='"+Valid.SetTgl(TanggalAwal.getSelectedItem()+"")+"' ",
//                "SKS"+TanggalAwal.getSelectedItem().toString().substring(6,10)+TanggalAwal.getSelectedItem().toString().substring(3,5)+TanggalAwal.getSelectedItem().toString().substring(0,2),3,NoSurat); 
        NoSurat.requestFocus();
    }

 
    private void getData() {
        if(tbObat.getSelectedRow()!= -1){
            NoSurat.setText(tbObat.getValueAt(tbObat.getSelectedRow(),0).toString());
            TNoRw.setText(tbObat.getValueAt(tbObat.getSelectedRow(),1).toString());
            TNoRM.setText(tbObat.getValueAt(tbObat.getSelectedRow(),2).toString());
            TPasien.setText(tbObat.getValueAt(tbObat.getSelectedRow(),3).toString());
            Valid.SetTgl(TanggalPeriksa,tbObat.getValueAt(tbObat.getSelectedRow(),4).toString());
            WawancaraPrikiatri.setText(tbObat.getValueAt(tbObat.getSelectedRow(),6).toString()); 
            KdDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),7).toString()); 
            TDokter.setText(tbObat.getValueAt(tbObat.getSelectedRow(),8).toString()); 
            Pekerjaan.setText(tbObat.getValueAt(tbObat.getSelectedRow(),10).toString());
            isDiagnosa();
            
        }
    }

    private void isRawat() {
         Sequel.cariIsi("select no_rkm_medis from reg_periksa where no_rawat='"+TNoRw.getText()+"' ",TNoRM);
    }

    private void isPsien() {
        Sequel.cariIsi("select nm_pasien from pasien where no_rkm_medis='"+TNoRM.getText()+"' ",TPasien);
    }
    
    private void isPerusahaan() {
//         Sequel.cariIsi("select nama_perusahaan from pasien inner join perusahaan_pasien on perusahaan_pasien.kode_perusahaan=pasien.perusahaan_pasien where no_rkm_medis='"+TNoRM.getText()+"' ",TInstansi);
    }
    
    private void isPekerjaan() {
         Sequel.cariIsi("select pekerjaan from pasien where no_rkm_medis='"+TNoRM.getText()+"' ",Pekerjaan);
    }
    
    private void isDiagnosa() {
////        Sequel.cariIsi("select kd_penyakit from diagnosa_pasien where diagnosa_pasien.prioritas=1 and no_rawat='"+TNoRw.getText()+"' ",KdDiag); 
//        Sequel.cariIsi("select nm_penyakit from penyakit inner join diagnosa_pasien on penyakit.kd_penyakit=diagnosa_pasien.kd_penyakit where diagnosa_pasien.prioritas=1 and no_rawat='"+TNoRw.getText()+"' ",Diagnosa);
    }
    
    public void setNoRm(String norwt, Date tgl1, Date tgl2,String kodedokter) {
        TNoRw.setText(norwt);
        KdDokter.setText(kodedokter);
     // KdDokter.setText(Sequel.cariIsi("select kd_dokter from reg_periksa where no_rawat=?",norwt));
     // TDokter.setText(namadokter);
        TDokter.setText(Sequel.cariIsi("select nm_dokter from dokter where kd_dokter=?",kodedokter));        
        TCari.setText(norwt);
        DTPCari1.setDate(tgl1);
        DTPCari2.setDate(tgl2);
        isRawat();
        isPsien(); 
        isPerusahaan();
        isPekerjaan();
        isDiagnosa();
        ChkInput.setSelected(true);
        isForm();
    }
    
    
    
    private void isForm(){
        if(ChkInput.isSelected()==true){
            ChkInput.setVisible(false);
            PanelInput.setPreferredSize(new Dimension(WIDTH,400));
            FormInput.setVisible(true);      
            ChkInput.setVisible(true);
        }else if(ChkInput.isSelected()==false){           
            ChkInput.setVisible(false);            
            PanelInput.setPreferredSize(new Dimension(WIDTH,20));
            FormInput.setVisible(false);      
            ChkInput.setVisible(true);
        }
    }
       
    
    public void isCek(){
        BtnSimpan.setEnabled(akses.getsurat_sakit());
        BtnHapus.setEnabled(akses.getsurat_sakit());
        BtnEdit.setEnabled(akses.getsurat_sakit());
    }
    
    private void nomorSurat() {
        bln_angka = "";
        bln_romawi = "";
        
        bln_angka = TanggalPeriksa.getSelectedItem().toString().substring(3,5);
        
        if (bln_angka.equals("01")) {
            //bln_angka  = "01";
            bln_romawi = "I";
        } else if (bln_angka.equals("02")) {
            //bln_angka  = "02";
            bln_romawi = "II";
        } else if (bln_angka.equals("03")) {
            //bln_angka  = "03";
            bln_romawi = "III";
        } else if (bln_angka.equals("04")) {
            //bln_angka  = "04";
            bln_romawi = "IV";
        } else if (bln_angka.equals("05")) {
            //bln_angka  = "05";
            bln_romawi = "V";
        } else if (bln_angka.equals("06")) {
            //bln_angka  = "06";
            bln_romawi = "VI";
        } else if (bln_angka.equals("07")) {
            //bln_angka  = "07";
            bln_romawi = "VII";
        } else if (bln_angka.equals("08")) {
            //bln_angka  = "08";
            bln_romawi = "VIII";
        } else if (bln_angka.equals("09")) {
            //bln_angka  = "09";
            bln_romawi = "IX";
        } else if (bln_angka.equals("10")) {
            //bln_angka  = "10";
            bln_romawi = "X";
        } else if (bln_angka.equals("11")) {
            //bln_angka  = "11";
            bln_romawi = "XI";
        } else if (bln_angka.equals("12")) {
            //bln_angka  = "12";
            bln_romawi = "XII";
        }

        Valid1.autoNomerSuratKhusus1("select ifnull(MAX(CONVERT(LEFT(no_surat,3),signed)),0), MONTH(tanggalawal) bln from suratjiwa where "
                + "tanggalawal like '%" + Valid.SetTgl(TanggalPeriksa.getSelectedItem() + "").substring(0, 7) + "%' ", "/SCS/" + bln_romawi + "/" + TanggalPeriksa.getSelectedItem().toString().substring(6,10), 3, NoSurat);
        bln_angka = Sequel.cariIsi("SELECT MONTH(tanggalawal) bln FROM suratjiwa WHERE tanggalawal like '%" + Valid.SetTgl(TanggalPeriksa.getSelectedItem() + "").substring(0, 7) + "%'");
    }
}





